package com.nna88.voz.parserhtml;

import android.util.Log;

import com.nna88.voz.listview.WebkitCookieManagerProxy;
import com.nna88.voz.main.Global;
import com.nna88.voz.util.UserInfo;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Map;

public class parser {
	String m_url;
	public static final int TIMEOUT = 20000;
	public String sNotif = "";

	public parser(String url){
		m_url = url;
	}
	public String getUrl(){
		return m_url;
	}
	public void setUrl(String url){
		m_url = url;
	}
	public Document login(UserInfo mUser){
        String mCaptcha="", mSalt="", mUserid="";
    	try {
            Response res = Jsoup.connect("https://vozforums.com/vbdev/login_api.php")
                    .timeout(TIMEOUT)
                    .data("do","login")
                    .data("api_cookieuser", "1")
                    .data("securitytoken", "guest")
                    .data("api_vb_login_md5password", mUser.md5Pass())
                    .data("api_vb_login_md5password_utf", mUser.md5Pass())
                    .data("api_vb_login_password", mUser.Pass())
                    .data("api_vb_login_username", mUser.User())
                    .data("api_2factor", "")
                    .data("api_captcha", "")
                    .data("api_salt", "")
                    .method(Method.POST)
                    .execute();
            Document doctemp = res.parse();
            try {
                JSONObject jObject = new JSONObject(doctemp.text());
                mCaptcha = (String) jObject.get("captcha");
                res = Jsoup.connect("https://vozforums.com/vbdev/login_api.php")
                        .timeout(TIMEOUT)
                        .data("do", "login")
                        .data("api_cookieuser", "1")
                        .data("securitytoken", "guest")
                        .data("api_vb_login_md5password", mUser.md5Pass())
                        .data("api_vb_login_md5password_utf", mUser.md5Pass())
                        .data("api_vb_login_password", mUser.Pass())
                        .data("api_vb_login_username", mUser.User())
                        .data("api_2factor", "")
                        .data("api_captcha", mCaptcha)
                        .data("api_salt", "")
                        .method(Method.POST)
                        .execute();
                doctemp = res.parse();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jObject = new JSONObject(doctemp.text());
                mSalt = (String) jObject.get("salt");
                mUserid = (String) ((JSONObject)jObject.get("userinfo")).get("userid");
                res = Jsoup.connect("https://vozforums.com/vbdev/login_api.php")
                        .timeout(TIMEOUT)
                        .data("do","login")
                        .data("api_cookieuser", "1")
                        .data("securitytoken", "guest")
                        .data("api_vb_login_md5password", mUser.md5Pass())
                        .data("api_vb_login_md5password_utf", mUser.md5Pass())
                        .data("api_vb_login_password", mUser.Pass())
                        .data("api_vb_login_username", mUser.User())
                        .data("api_2factor", "")
                        .data("api_captcha", mCaptcha)
                        .data("api_salt", mSalt)
                        .method(Method.POST)
                        .execute();
                doctemp = res.parse();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(doctemp.select("div:contains(You have entered an invalid username or password)").first() != null){
                sNotif = null;
                return null;
            }else{
                Map<String,String> mCookies = res.cookies();
                mCookies.put("vfuserid",mUserid);
                mUser.cookies(mCookies);
                mUser.setLogin(true);
                return getDoc(mUser);
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = null;
			e.printStackTrace();
			return null;
		}
	}
	public Document Logout(UserInfo mUser){
    	String urlLogout=null;
    	urlLogout = "https://vozforums.com/login.php?do=logout&"+
    			"logouthash=" + mUser.Token();
    	try {
    		Response  res = Jsoup.connect(urlLogout)
						.timeout(TIMEOUT)
						.method(Method.POST)
						.execute();
			mUser.cookies(res.cookies());
			return getDoc(mUser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
		
    }
	
	public Document Search(UserInfo mUser, String sectoken,String query, String showpost){
		Map<String, String> cookies = mUser.cookies();
		if(cookies==null || sectoken==null) return null;
		String url = "https://vozforums.com/search.php?do=process";
		Response res;
		try {
			res = Jsoup.connect(url)
					.timeout(TIMEOUT)
					.cookies(cookies)
					.data("s"," ")
					.data("securitytoken",sectoken)
					.data("do","process")
					.data("query",query)
					.data("showposts",showpost)
					.data("quicksearch","1")
					.data("childforums","1")
					.data("exactname","1")
					.execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
            return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
	}

	public Document AdvanceSearch(UserInfo mUser, String sectoken,String query, String forumchoice, String showpost){
		Map<String, String> cookies = mUser.cookies();
		if(cookies==null || sectoken==null) return null;
		String url = "https://vozforums.com/search.php?do=process";
		Response res;
		try {
			res = Jsoup.connect(url)
					.timeout(TIMEOUT)
					.cookies(cookies)
					.data("s"," ")
					.data("securitytoken",sectoken)
					.data("do","process")
					.data("searchthreadid"," ")
					.data("query",query)
					.data("titleonly","0")
					.data("searchuser"," ")
					.data("starteronly"," ")
					.data("exactname","1")
					.data("prefixchoice[]"," ")
					.data("replyless","0")
					.data("replylimit","0")
					.data("searchdate","0")
					.data("beforeafter","after")
					.data("sortby","lastpost")
					.data("order","descending")
					.data("showposts",showpost)
					.data("forumchoice[]",forumchoice)
					.data("childforums","1")
					.data("dosearch","Search Now")
					.data("saveprefs","1")
					.execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
            return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
		
	}
    private String mUserIdRecentPost=null;
    public void setUserRecentPost(String userid){
        mUserIdRecentPost = userid;
    }
	public Document myRecentPost(UserInfo mUser)  {
		Map<String, String> cookies = mUser.cookies();
		String sUserId = null;
        if (mUserIdRecentPost!=null)
            sUserId = mUserIdRecentPost;
        else
            sUserId = mUser.UserId();
        mUserIdRecentPost=null;
		if(cookies==null || sUserId==null) return null;
		Response res;
		try {
			String url = "https://vozforums.com/search.php?do=finduser&u=";
			url += sUserId;
			res = Jsoup.connect(url)
					.timeout(TIMEOUT)
					.cookies(cookies)
					.execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
			return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
	}
	public Document myRecentThread(UserInfo mUser)  {
		Map<String, String> cookies = mUser.cookies();
		String sUserId = mUser.UserId();
		if(cookies==null || sUserId==null) return null;
		Response res;
		try {
			//https://vozforums.com/search.php?do=finduser&u=953521&starteronly=1
			String url = "https://vozforums.com/search.php?do=finduser&u=";
			url += sUserId;
			url += "&starteronly=1";
			res = Jsoup.connect(url)
					.timeout(TIMEOUT)
					.cookies(cookies)
					.execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
            return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Get text quote
	 * @param type : 0: reply, 1: edit
	 * @param id: id post
	 * @return
	 */
	public Document quoteGet(UserInfo mUser, int type, int id){
    	try {
    		String temp = null;
    		if(type == Global.REQUEST_QUOTE)//new reply
    			temp = "https://vozforums.com/newreply.php?do=newreply&p=";
    		else if ((type == Global.REQUEST_EDIT) || (type == Global.REQUEST_DELETE) )//edit post
    			temp = "https://vozforums.com/editpost.php?do=editpost&p=";
    		else if (type == Global.REQUEST_REPLY) // quickreply
    			return null;
    		m_url = temp + String.valueOf(id);
			Response res = Jsoup.connect(m_url)
					  .timeout(TIMEOUT)  
					  .cookies(mUser.cookies())
					  .method(Method.GET)
				      .execute();
			Log.d("nna","quoteget ok");
			Document doc2 = res.parse();
			return doc2;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
            e.printStackTrace();
			return null;
		}
    }

    public void testHtml(){
//        try {
//            log("http1");
//            String result;
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet(m_url);
//            HttpResponse response = httpClient.execute(httpGet);
//            HttpEntity httpEntity = response.getEntity();
//            if(httpEntity != null){
//               result = EntityUtils.toString(httpEntity);
//            }
//            if(httpEntity != null){
//                httpEntity.consumeContent();
//            }
//            httpClient.getConnectionManager().shutdown();
//            log("http2");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }
    public Document getDoc(UserInfo mUser){
    	Response res ;
        Document doc = null;
//    	try {
//            if (mUser.cookies() == null) {
//                res = Jsoup.connect(m_url)
//                        .timeout(TIMEOUT)
//                        .execute();
//                if (res != null) doc = res.parse();
//                mUser.setLogin(false);
//            } else {
//                res = Jsoup.connect(m_url)
//                        .timeout(TIMEOUT)
//                        .cookies(mUser.cookies())
//                        .execute();
//
//                res.cookies()
//                mUser.setLogin(true);
//                if (res != null) doc = res.parse();
//            }
//
//            if (doc != null) m_url = doc.baseUri();
//            else {
//                sNotif = "Cannot access vozForums\n";
//                sNotif += res.statusMessage();
//                sNotif += res.statusCode();
//
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        try{
            try {
                String result=null;
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(m_url);
                // Create local HTTP context
                if(mUser.getCookiStore() != null) httpGet.addHeader("Cookie", mUser.getCookiStore());
                //Set HANDLE_REDIRECTS to false ,
                //defines redirects should be handled manually
                HttpParams params = httpClient.getParams();
                HttpClientParams.setRedirecting(params, false);
                HttpResponse response = httpClient.execute(httpGet);
                int responseCode = response.getStatusLine().getStatusCode();
                if (responseCode != HttpStatus.SC_OK) {
                    Header[] headers = response.getHeaders("Location");
                    if (headers != null && headers.length != 0) {
                        //Need to redirect
                        m_url = headers[headers.length - 1].getValue();
                        return getDoc(mUser);
                    }
                }

                HttpEntity httpEntity = response.getEntity();
                if(httpEntity != null){
                    result = EntityUtils.toString(httpEntity);
                }
                if(httpEntity != null){
                    httpEntity.consumeContent();
                }

                List<Cookie> cookies = httpClient.getCookieStore().getCookies();
                if (cookies.isEmpty()) {
                    mUser.setLogin(false);
                } else {
                    for (int i = 0; i < cookies.size(); i++) {
                        if(cookies.get(i).getName().equals("vfsessionhash")){
                            mUser.setLogin(true);
                            break;
                        }
                    }
                }

                httpClient.getConnectionManager().shutdown();
                doc = Jsoup.parse(result);
//                log("time http,parser"+ (System.currentTimeMillis() - time));
            } catch (IOException e) {
                e.printStackTrace();
            }
			return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Log.d("nna:","cookiesnull,status:"+res.statusCode());
			sNotif = "Cannot access vozForums\n";
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }else if(e instanceof SocketTimeoutException){
                sNotif += "\nTimeout" ;
            }
            e.printStackTrace();
			return null;
		}
    }
    public Document getPM(UserInfo mUser){
    	Map<String, String> cookies = mUser.cookies();
		String url = "https://vozforums.com/private.php";
		Response res;
		
		try {
			res = Jsoup.connect(url)
					.timeout(TIMEOUT)
					.cookies(cookies)
					.execute();
			return res.parse();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
    public Document SearchQuote(UserInfo mUser,Map<String, String> _cookies){
    	String sectoken=null;
    	String user = null;
    	Map<String, String> cookies;
    	if(!mUser.isLogin()){
			cookies = _cookies;
    	}else{
    		cookies = mUser.cookies();
    	}
    	try {
    		Document docGetToken = Jsoup.connect("https://vozforums.com/usercp.php")
    									.cookies(cookies)
    									.timeout(TIMEOUT)
    									.get();
    		
    		if(docGetToken!= null){
    			Element eleToken = docGetToken.select("form[action*=profile.php]").first();
    			if(eleToken != null){
    				sectoken = eleToken.select("input[name=securitytoken]").attr("value");
    			}else{
    				Log.d("nna","Sectoken null");
    				return null;
    			}
    			if(mUser.User()!=null){
    				user = mUser.User();
    			}else{
    				user = docGetToken.select("a[href*=member.php]").first().text();
        			
    			}
    			
    				
    		}
			Response res = Jsoup.connect("https://vozforums.com/search.php?do=process")
					  .timeout(TIMEOUT)
					  .cookies(cookies)
					  .data("do","process")
	    			  .data("quicksearch","1")
	    			  .data("childforums","1")
	    			  .data("exactname","1")
	    			  .data("s"," ")
	    			  .data("securitytoken",sectoken)
	    			  .data("query",user)
	    			  .data("showposts","1")
					  .method(Method.POST)
				      .execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
            return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
    public Document SearchQuote(UserInfo mUser){
    	String sectoken=null;
//    	if(!mUser.isLogin()) 		return null;
    	try {
    		Document docGetToken = Jsoup.connect("https://vozforums.com/usercp.php")
                    .cookies(mUser.cookies())
                    .get();
            if(docGetToken!= null){
                Element eleToken = docGetToken.select("form[action*=profile.php]").first();
                if(eleToken != null){
                    sectoken = eleToken.select("input[name=securitytoken]").attr("value");
                }else{
                    Log.d("nna","Sectoken null");
                    return null;
                }
            }
			Response res = Jsoup.connect("https://vozforums.com/search.php?do=process")
					  .timeout(TIMEOUT)
					  .cookies(mUser.cookies())
					  .data("do","process")
	    			  .data("quicksearch","1")
	    			  .data("childforums","1")
	    			  .data("exactname","1")
	    			  .data("s"," ")
	    			  .data("securitytoken",sectoken)
	    			  .data("query",mUser.User())
	    			  .data("showposts","1")
					  .method(Method.POST)
				      .execute();
            Document doc = res.parse();
            m_url = doc.baseUri();
            return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
//            sNotif = "Cannot access vozForums\n";
//            sNotif += e.getLocalizedMessage();
//            if(e instanceof  HttpStatusException){
//                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
//            }else if(e instanceof  SocketTimeoutException){
//                sNotif += "\nStatus: " + ((SocketTimeoutException) e).getLocalizedMessage();
//                sNotif += "\nStatus: " + ((SocketTimeoutException) e).getMessage();
//            }
//            log(sNotif);
			e.printStackTrace();
			return null;
		}
    }
    private void log(String ss){
        Log.d("nna",ss);
    }
    public Document UnSubscribe(UserInfo mUser, String id ){
    	try {
    		
    		Response res = Jsoup.connect("https://vozforums.com/subscription.php?do=removesubscription&t=" + id)
					  .timeout(TIMEOUT)
					  .cookies(mUser.cookies())
					  .method(Method.GET)
				      .execute();
			return getDoc(mUser);
			//return parser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}

    }
    public Document Subscribe(UserInfo mUser, String id,String secToken){
    	try {
    		if(mUser.cookies() == null) return null;
            if(secToken == null) return null;
            if(id == null) return null;
			Response res = Jsoup.connect("https://vozforums.com/subscription.php?do=doaddsubscription&threadid="+id)
					  .timeout(TIMEOUT)
					  .cookies(mUser.cookies())
					  .data("do","doaddsubscription")
	    			  .data("threadid",id)
	    			  .data("s"," ")
	    			  .data("url","index.php")
	    			  .data("emailupdate", "0")
	    			  .data("folderid", "0")
	    			  .data("securitytoken", secToken)
					  .method(Method.POST)
				      .execute();
			return getDoc(mUser);
			//return parser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}

    }
 
    /**
     * 
     * @param message
     * @param url
     * @param title
     * @param secToken
     * @param p
     * @param postHash
     * @param postStartTime
     * @return
     */
    
    public Document PostEditReply(UserInfo mUser, int type, String message,String url,String title, String secToken,
    		String p,String postHash, String postStartTime ){
    	try {
    		Response res;
    		if(type == 0){
    		 res = Jsoup.connect("https://vozforums.com/" + url)
    				  .timeout(TIMEOUT)
    				  .cookies(mUser.cookies())
    				  .data("title",title)
    				  .data("message",message)
					  .data("wysiwyg","0")
					  .data("s", " ")
					  .data("securitytoken",secToken)
					  .data("do", "updatepost")
					  .data("p", p)
					  .data("posthash", postHash)
					  .data("poststarttime", postStartTime)
					  .data("sbutton", "Save Changes")
					  .data("parseurl", "1")
//					  .data("emailupdate", "0")
					  .data("signature","1")
					  .method(Method.POST)
				      .execute();
    		}else{
    			 res = Jsoup.connect("https://vozforums.com/" + url)
      				  .timeout(TIMEOUT)
      				  .cookies(mUser.cookies())
      				  .data("s","")
      				  .data("securitytoken",secToken)
      				  .data("p", p)
      				  .data("url","https://vozforums.com/showthread.php?p="+p+"&highlight=")
      				  .data("do", "deletepost")
      				  .data("deletepost", "delete")
      				  .data("reason", "")		  
  					  .method(Method.POST)
  				      .execute();
    		}
    		m_url = res.url().toString();
    		return res.parse();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
    public Document PostPMNew(UserInfo mUser, String recipients,String title,String message, String secToken){
    	String url = "https://vozforums.com/private.php?do=insertpm&pmid=";
     	if(mUser.cookies()==null) return null;	
    	try {
    		Response res = Jsoup.connect(url)
    				  .timeout(TIMEOUT)
    				  .cookies(mUser.cookies())
    				  .data("message", message)
					  .data("wysiwyg","0")
					  .data("styleid", "0")
					  .data("fromquickreply", "1")
					  .data("s", " ")
					  .data("securitytoken",secToken)
					  .data("do", "insertpm")
					  .data("iconid","0")
					  .data("parseurl", "1")
					  .data("title",title)
					  .data("recipients",recipients)
					  .data("forward","0")
					  .data("savecopy","1")
					  .data("sbutton", "Submit Message")
					  .method(Method.POST)
				      .execute();
    		m_url = res.url().toString();
    		return res.parse();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
    public Document PostPMReply(UserInfo mUser, String userid,String recipients,String title,String message,String mId, String secToken){
    	String url = "https://vozforums.com/private.php?do=insertpm&pmid=";
    	url += mId;
    	if(mUser.cookies()==null) return null;
    	if(secToken==null) return null;
    	try {
    		Response res = Jsoup.connect(url)
    				  .timeout(TIMEOUT)
    				  .cookies(mUser.cookies())
    				  .data("message", message)
					  .data("wysiwyg", "0")
					  .data("styleid", "0")
					  .data("fromquickreply", "1")
					  .data("s", " ")
					  .data("securitytoken",secToken)
					  .data("do", "insertpm")
					  .data("pmid",mId)
					  .data("loggedinuser",userid)
					  .data("parseurl", "1")
					  .data("title",title)
					  .data("recipients",recipients)
					  .data("forward","0")
					  .data("savecopy","1")
					  .data("sbutton", "Post Message")
					  .method(Method.POST)
				      .execute();
    		m_url = res.url().toString();
    		return res.parse();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
    public Document PostReplyQuote(Document doc, UserInfo mUser, String message, String specifiedpost) throws Exception{
        if(mUser.cookies() ==null ) return null;
        String mUrl;
        Element eleUrl = doc.select("form[action*=newreply.php?do=postreply]").first();
        Element eleSecurity = doc.select("input[name*=securitytoken]").first();
        Element elePostHash = doc.select("input[name*=posthash]").first();
        Element elePostStartTime = doc.select("input[name*=poststarttime]").first();
        Element eleTitle = doc.select("input[name=title]").first();
        Element eleP = doc.select("input[name=p]").first();
        mUser.setToken(eleSecurity.attr("value"));
        mUrl = eleUrl.attr("action");
        mUrl = mUrl.replace("&amp;", "&");
        mUrl = "https://vozforums.com/" + mUrl;
        String thread = mUrl.split("=")[2];
        String postHash = elePostHash.attr("value");
        String postStartTime = elePostStartTime.attr("value");
        String p = eleP.attr("value");
        String title = eleTitle.attr("value");
        try {
            Response res = Jsoup.connect(mUrl)
                    .timeout(TIMEOUT)
                    .cookies(mUser.cookies())
                    .data("title:Re: ",title)
                    .data("message",message)
                    .data("wysiwyg","0")
                    .data("s", " ")
                    .data("securitytoken",mUser.Token())
                    .data("do", "postreply")
                    .data("t", thread)
                    .data("p", p)
                    .data("specifiedpost", specifiedpost)
                    .data("posthash", postHash)
                    .data("poststarttime", postStartTime)
                    .data("loggedinuser", mUser.UserId())
                    .data("multiquoteempty:", " ")
                    .data("sbutton", "Submit Reply")
                    .data("signature","1")
                    .data("parseurl", "1")
//                    .data("emailupdate", "0")
                    .method(Method.POST)
                    .execute();
            m_url = res.url().toString();
            return res.parse();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();
            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
            e.printStackTrace();
            return null;
        }
    }

    public Document NewThread(UserInfo mUser, String title,String message,String forum,String secToken,
    		String postHash,String startTime){
    	try {
    		Response res = Jsoup.connect("https://vozforums.com/newthread.php?do=postthread&f="+forum)
    				  .timeout(TIMEOUT)
    				  .cookies(mUser.cookies())
    				  .data("subject", title)
    				  .data("message",message)
    				  .data("wysiwyg","0")
					  .data("s", " ")
    				  .data("securitytoken",secToken)
    				  .data("f",forum)
    				  .data("do", "postthread")
    				  .data("posthash",postHash)
    				  .data("poststarttime",startTime)
    				  .data("loggedinuser",mUser.UserId())
    				  .data("sbutton", "Submit New Thread")
    				  .data("signature", "1")
    				  .data("parseurl", "1")
    				  .data("emailupdate", "9999")
					  .method(Method.POST)
				      .execute();
    		m_url = res.url().toString();
    		return res.parse();
		} catch (Exception e) {
			// TODO Auto-generated catch block
            sNotif = "Cannot access vozForums\n";
            sNotif= e.getLocalizedMessage();

            if(e instanceof  HttpStatusException){
                sNotif += "\nStatus: " + ((HttpStatusException) e).getStatusCode();
            }
			e.printStackTrace();
			return null;
		}
    }
}
