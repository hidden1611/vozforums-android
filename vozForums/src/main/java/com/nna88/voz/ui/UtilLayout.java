package com.nna88.voz.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


/**
 * Created by nna on 11/3/13.
 */
public class UtilLayout {
    public static void setMargin(Context context, View view, int left, int top, int right, int bottom){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        lp.setMargins(Util.convertDpToPx(context,left),
                Util.convertDpToPx(context,top),
                Util.convertDpToPx(context,right),
                Util.convertDpToPx(context,bottom));
        view.setLayoutParams(lp);
        view.requestLayout();
        view.invalidate();
    }
    public static void addView(ViewGroup viewParent, View viewChild, int width, int height){
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width,height);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
        viewParent.addView(viewChild,lp);
//        RelativeLayout relative = new RelativeLayout(this);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
//        params.addRule(RelativeLayout.ALIGN_WITH_PARENT_BOTTOM);
//
//        relativeLayout.addView(textView, params);
//
//        linearLayout.addView(relativeLayout, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }
}
