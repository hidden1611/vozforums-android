package com.nna88.voz.ui;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;

import com.nna88.voz.main.R;


/**
 * Created by nna on 10/23/13.
 */
public class NavigationDrawerActivity extends ActionBarActivity {
    protected DrawerLayout mDrawerLayout;
    protected ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerLeftList;
    private CharSequence mDrawerTitle;
    private Toolbar toolbar;
    private CharSequence mTitle;

    protected void setLeftDrawer(SidebarAdapter mAdapterLeftDrawer){
        mDrawerLeftList.setAdapter(mAdapterLeftDrawer);
    }
//    protected void setRightDrawer(SidebarAdapter mAdapterRightDrawer){
//        mDrawerRightList.setAdapter(mAdapterRightDrawer);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigationdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = getTitle();
        mDrawerTitle = "Open navigation";
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerLeftList = (ListView) findViewById(R.id.left_drawer);
//        mDrawerRightList = (ListView) findViewById(R.id.right_drawer);
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
         mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                if(Build.VERSION.SDK_INT > 11){
                    getSupportActionBar().setTitle(mTitle);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }else{
                    getSupportActionBar().setTitle(mTitle);
                    supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            }
            public void onDrawerOpened(View view) {
                if(Build.VERSION.SDK_INT > 11){
                    if(view.getId() == R.id.left_drawer){
                        getSupportActionBar().setTitle("User");
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                }else{
                    if(view.getId() == R.id.left_drawer){
                        getSupportActionBar().setTitle("User");
                        supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                }

            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerLeftList);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
//        return true;
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
//        if (Build.VERSION.SDK_INT >= 11){
//            getActionBar().setTitle(mTitle);
//        }else{
//            getSupportActionBar().setTitle(mTitle);
//        }
    }
    protected void closeDrawer(){
        mDrawerLayout.closeDrawers();
    }
    protected DrawerLayout getmDrawerLayout(){
        return mDrawerLayout;
    }


}
