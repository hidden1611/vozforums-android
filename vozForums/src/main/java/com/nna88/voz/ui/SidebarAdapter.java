/*****************************************************************************
 * SidebarAdapter.java
 *****************************************************************************
 * Copyright © 2012 VLC authors and VideoLAN
 * Copyright © 2012 Edward Wang
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package com.nna88.voz.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.nna88.voz.main.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SidebarAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Object> mItems;

    static class SidebarEntry {
        String id;
        String name;
        int drawableID;

        public SidebarEntry(String _id, String _name, int _drawableID) {
            this.id = _id;
            this.name = _name;
            this.drawableID = _drawableID;
        }
    }

    private LayoutInflater mInflater;
    static final List<SidebarEntry> entries;

    static {
        SidebarEntry entries2[] = {};
        entries = Arrays.asList(entries2);
    }

    public SidebarAdapter(Context context, ArrayList<Object> items) {
        this.context = context;
    	mItems = items;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
    	return mItems.size();
    }

    @Override
    public Object getItem(int position) {
    	return mItems.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
    	return arg0;
    }
    @Override
    public int getItemViewType(int position) {
        return getItem(position) instanceof Item ? 0 : 1;
    }
    @Override
    public int getViewTypeCount() {
        return 2;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View v = convertView;
        final Object item = getItem(position);
        if (item instanceof Category) {
            if (v == null) {
                v = mInflater.inflate(R.layout.navigationdrawer_catarory, parent, false);
            }

            ((TextView) v).setText(((Category) item).mTitle);

        } else {
            if (v == null) {
                v = mInflater.inflate(R.layout.navigationdrawer_item, parent, false);
            }
            TextView tv = (TextView) v;
            tv.setText(((Item) item).mTitle);

            Drawable img =((Item) item).mIcon;
            if(!((Item) item).isAvatart){
                int dp_32 = Util.convertDpToPx(context, 32);
                img.setBounds(0, 0, dp_32, dp_32);
            }else{
                int dp_32 = Util.convertDpToPx(context,32);
                int width = (img.getIntrinsicWidth()*dp_32/img.getIntrinsicHeight());
                img.setBounds(0, 0, width, dp_32);
            }
            tv.setCompoundDrawables(img, null, null, null);
            tv.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				if(mItemClickListener!= null)
    					mItemClickListener.onItemClick(position,item);
    			}
    		});
        }
        
        return v;
 
    }
    private OnActionItemClickListener mItemClickListener;
	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mItemClickListener = listener;
	}
	public interface OnActionItemClickListener {
		public abstract void onItemClick(int pos, Object object);
	}
	
	public static class Item {

        public String mTitle;
        public Drawable mIcon;
        public boolean isAvatart = false;
        public Item(String title, Drawable mIcon) {
            mTitle = title;
            this.mIcon = mIcon;
        }
        public Item(String title, Drawable mIcon, boolean isAvatart) {
            mTitle = title;
            this.mIcon = mIcon;
            this.isAvatart = isAvatart;
        }

    }

    public static class Category {

        String mTitle;

        public Category(String title) {
            mTitle = title;
        }
    }

}
