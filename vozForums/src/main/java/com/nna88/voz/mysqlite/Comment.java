package com.nna88.voz.mysqlite;

public class Comment {
	public long id;
	public String url;
	public String forum;
	public String title;
	public String iPage;
	public String idThread;
	public String View;
	public String LastPost;
	public String urlFirstNews;
	public String urlLastPost;
	public Comment(){
		
	}
	public Comment(String idThread,String title,String url,String forum,String iPage,String View, String LastPost, String urlFirstNews, String urlLastPost){
		this.idThread = idThread;
		this.url = url;
		this.title = title;
		this.forum = forum;
		this.iPage = iPage;
		this.View = View;
		this.LastPost = LastPost;
		this.urlFirstNews = urlFirstNews;
		this.urlLastPost = urlLastPost;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id+";"+url+";"+forum+";"+title+";"+iPage;
	}

}
