package com.nna88.voz.mysqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_COMMENTS = "bookmark";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_IDTHREAD = "idthread";
	public static final String COLUMN_TITLE= "title";
	public static final String COLUMN_URL = "url";
	public static final String COLUMN_FORUM = "forum";
	public static final String COLUMN_PAGE = "page";
	public static final String COLUMN_VIEW = "view";
	public static final String COLUMN_LASTPOST = "lastpost";
	public static final String COLUMN_URLFIRSTNEWS = "urlfirstnews";
	public static final String COLUMN_URLLASTPOST = "urllastpost";
	

	private static final String DATABASE_NAME = "bookmark.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_CREATE = "create table "
		      + TABLE_COMMENTS + "(" + COLUMN_ID
		      + " integer primary key autoincrement, " + 
		      COLUMN_IDTHREAD + " text not null, "+
		      COLUMN_TITLE + " text not null, "+
		      COLUMN_URL + " text not null, "+
		      COLUMN_FORUM + " text not null, "+
		      COLUMN_PAGE + " text not null, "+
		      COLUMN_VIEW + " text not null, "+
		      COLUMN_LASTPOST + " text not null, "+
		      COLUMN_URLFIRSTNEWS + " text not null, "+
		      COLUMN_URLLASTPOST + " text not null"+
		      ");";
	
	
	public MySQLiteHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	public MySQLiteHelper(Context context){
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
	    onCreate(db);
	}

}
