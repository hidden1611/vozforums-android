package com.nna88.voz.mysqlite;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import com.nna88.voz.contain.Thread;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class CommentsDataSource {
	// Database fields
	  private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	  private String[] allColumns = {
			  MySQLiteHelper.COLUMN_ID,
			  MySQLiteHelper.COLUMN_IDTHREAD,
			  MySQLiteHelper.COLUMN_TITLE,
			  MySQLiteHelper.COLUMN_URL,
			  MySQLiteHelper.COLUMN_FORUM,
			  MySQLiteHelper.COLUMN_PAGE,
			  MySQLiteHelper.COLUMN_VIEW,
			  MySQLiteHelper.COLUMN_LASTPOST,
			  MySQLiteHelper.COLUMN_URLFIRSTNEWS,
			  MySQLiteHelper.COLUMN_URLLASTPOST};

	  public CommentsDataSource(Context context) {
	    dbHelper = new MySQLiteHelper(context);
	  }

	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }
	  public void close() {
	    dbHelper.close();
	  }

	  public void deleteBookmark(Thread contain){
          try{
		    database.delete(MySQLiteHelper.TABLE_COMMENTS, MySQLiteHelper.COLUMN_IDTHREAD
                    + " = " + contain.mIdThread, null);
          }catch (SQLiteException e){
              e.printStackTrace();
          }
	  }
	  public void addBookmark(Thread contain){
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_IDTHREAD, contain.mIdThread);
		values.put(MySQLiteHelper.COLUMN_TITLE, contain.Thread());
		values.put(MySQLiteHelper.COLUMN_URL, contain.UrlThread());
		values.put(MySQLiteHelper.COLUMN_FORUM, "");
		values.put(MySQLiteHelper.COLUMN_LASTPOST,contain.LastPost());
		if(contain.Reply() == null)
			values.put(MySQLiteHelper.COLUMN_PAGE,"");
		else
			values.put(MySQLiteHelper.COLUMN_PAGE,contain.Reply());
		
		if(contain.View() == null)
			values.put(MySQLiteHelper.COLUMN_VIEW,"");
		else
			values.put(MySQLiteHelper.COLUMN_VIEW,contain.View());
		
		if(contain.UrlLastPosst() == null)
			values.put(MySQLiteHelper.COLUMN_URLFIRSTNEWS,"");
		else
			values.put(MySQLiteHelper.COLUMN_URLFIRSTNEWS,contain.UrlLastPosst());
		values.put(MySQLiteHelper.COLUMN_URLLASTPOST,contain.getUrlLastLast());
		database.insert(MySQLiteHelper.TABLE_COMMENTS, null,
				values);
	  }
	  public void updateBookmark(Thread contain){
		  ContentValues values = new ContentValues();
		  values.put(MySQLiteHelper.COLUMN_IDTHREAD, contain.mIdThread);
		  values.put(MySQLiteHelper.COLUMN_TITLE, contain.Thread());
		  values.put(MySQLiteHelper.COLUMN_URL, contain.UrlThread());
		  values.put(MySQLiteHelper.COLUMN_FORUM, "");
		  if(contain.Reply() == null)
				values.put(MySQLiteHelper.COLUMN_PAGE,"");
		  else
			    values.put(MySQLiteHelper.COLUMN_PAGE,contain.Reply());
		
		  if(contain.View() == null)
			    values.put(MySQLiteHelper.COLUMN_VIEW,"");
		  else
			  	values.put(MySQLiteHelper.COLUMN_VIEW,contain.View());
		  values.put(MySQLiteHelper.COLUMN_LASTPOST,contain.LastPost());
		  if(contain.UrlLastPosst() == null)
				values.put(MySQLiteHelper.COLUMN_URLFIRSTNEWS,"");
			else
				values.put(MySQLiteHelper.COLUMN_URLFIRSTNEWS,contain.UrlLastPosst());
		  values.put(MySQLiteHelper.COLUMN_URLLASTPOST,contain.getUrlLastLast());
		  database.update(MySQLiteHelper.TABLE_COMMENTS, values, MySQLiteHelper.COLUMN_IDTHREAD + " = ?",
				  new String[] {contain.mIdThread});
	  }
  
	  public List<Comment> getAllComments() {
	    List<Comment> comments = new ArrayList<Comment>();
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_COMMENTS,
	        allColumns, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Comment comment = cursorToComment(cursor);
	      comments.add(comment);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return comments;
	  }
	  private Comment cursorToComment(Cursor cursor) {
	    Comment comment = new Comment();
	    comment.id = cursor.getLong(0);
	    comment.idThread = cursor.getString(1);
	    comment.title = cursor.getString(2);
	    comment.url = cursor.getString(3);
	    comment.forum = cursor.getString(4);
	    comment.iPage = cursor.getString(5);
	    comment.View = cursor.getString(6);
	    comment.LastPost = cursor.getString(7);
	    comment.urlFirstNews = cursor.getString(8);
	    comment.urlLastPost = cursor.getString(9);
	    return comment;
	  }
}
