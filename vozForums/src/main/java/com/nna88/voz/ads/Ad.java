package com.nna88.voz.ads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.nna88.voz.main.R;
import com.startapp.android.publish.banner.Banner;
import com.startapp.android.publish.banner.BannerListener;
import com.startapp.android.publish.banner.bannerstandard.BannerStandard;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nna on 11/2/13.
 */
public class Ad {
    public final static int TIME = 120000;
    public static final int ADMOB = 0;
    public static final int AIRPUSH = 1;
    private Activity activity;
    private RelativeLayout mLayoutAds;

    private boolean bTest = false;
    private int mType = ADMOB;
    private AdRequest adRequest;
    private com.google.android.gms.ads.AdView adView;
    private SharedPreferences settings;
    private long mTimeClicAd;
    private String mUser = "no";
//    private static final String AD_UNIT_ID = "a151ee7f81e9f85";
    private static final String AD_UNIT_ID = "ca-app-pub-4243466839280809/3179146171";

    public Ad(Activity activity, RelativeLayout layoutAds){
        this.activity = activity;
        this.mLayoutAds = layoutAds;
        settings = activity.getSharedPreferences("Setting", Context.MODE_PRIVATE);
    }
    public void setTest(boolean value){
        bTest = value;
    }
    public void loadAds(){
        mLayoutAds.setVisibility(View.VISIBLE);
        BannerStandard bannerStandard = (BannerStandard)mLayoutAds.findViewById(R.id.startAppStandardBanner);
        bannerStandard.setBannerListener(new BannerListener() {
            @Override
            public void onReceiveAd(View view) {
                Log.d("nna","receivedAds");
            }

            @Override
            public void onFailedToReceiveAd(View view) {

            }

            @Override
            public void onClick(View view) {

            }
        });
        bannerStandard.showBanner();
//        Banner startAppBanner = new Banner(mLayoutAds.getContext());
//        RelativeLayout.LayoutParams bannerParameters =
//                new RelativeLayout.LayoutParams(
//                        RelativeLayout.LayoutParams.WRAP_CONTENT,
//                        RelativeLayout.LayoutParams.WRAP_CONTENT);
//        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
//        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        mLayoutAds.addView(startAppBanner, bannerParameters);
    }
    public void hideAds(){
        mLayoutAds.setVisibility(View.VISIBLE);
        BannerStandard bannerStandard = (BannerStandard)mLayoutAds.findViewById(R.id.startAppStandardBanner);
        bannerStandard.setBannerListener(new BannerListener() {
            @Override
            public void onReceiveAd(View view) {
                Log.d("nna","receivedAds");
            }

            @Override
            public void onFailedToReceiveAd(View view) {

            }

            @Override
            public void onClick(View view) {

            }
        });
        bannerStandard.hideBanner();
//        Banner startAppBanner = new Banner(mLayoutAds.getContext());
//        RelativeLayout.LayoutParams bannerParameters =
//                new RelativeLayout.LayoutParams(
//                        RelativeLayout.LayoutParams.WRAP_CONTENT,
//                        RelativeLayout.LayoutParams.WRAP_CONTENT);
//        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
//        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        mLayoutAds.addView(startAppBanner, bannerParameters);
    }
    private void loadAdmod(){

        SimpleDateFormat timingFormat = new SimpleDateFormat("yyMMddHHmmss");
        long timecurrent = Long.parseLong(timingFormat.format(new Date()));
        long timesave = settings.getLong("museridd",0);
        //check xem co bi sua time khong
        if(timesave - timecurrent > TIME){
            timesave = timecurrent;
            writeTimeAd(timecurrent);
        }
        //check time de hien quakng cao
        if(timecurrent >= timesave){
            try {
                mLayoutAds.setVisibility(View.VISIBLE);
                adRequest = new AdRequest.Builder()
//                        .addTestDevice("AB6BC4DDBCCBD267578D4A29B98BCE64")
//                        .addTestDevice("81F7E2186D810A22139D15B9AD586D6B")
                        .build();
                adView = new com.google.android.gms.ads.AdView(activity);
                adView.setAdSize(AdSize.BANNER);
                adView.setAdUnitId(AD_UNIT_ID);
                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLeftApplication() {
                        super.onAdLeftApplication();
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        SimpleDateFormat timingFormat = new SimpleDateFormat("yyMMddHHmmss");
                        mTimeClicAd = Long.parseLong(timingFormat.format(new Date()));
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        SimpleDateFormat timingFormat = new SimpleDateFormat("yyMMddHHmmss");
                        long timecurrent = Long.parseLong(timingFormat.format(new Date()));
                        if(timecurrent - mTimeClicAd >= 5) {
                            writeTimeAd(timecurrent + TIME);
                            mLayoutAds.removeAllViews();
                            mLayoutAds.setVisibility(View.GONE);
                        }
                    }

                });
                mLayoutAds.removeAllViews();
                mLayoutAds.addView(adView);
                adView.loadAd(adRequest);
                if(mLayoutAds.getChildCount() < 1){
                    mLayoutAds.addView(adView);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            try {
                mLayoutAds.setVisibility(View.GONE);
                if(mLayoutAds.getChildCount() >= 1){
                    mLayoutAds.removeAllViews();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class TaskClickAds extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String url;
                url = "http://sonic-fact-242.appspot.com/clickad?a=" + mUser;
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                httpclient.execute(httpget);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    public void writeTimeAd (long data) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong("museridd", data);
        editor.commit();
    }
    private void toast(String ss){
        Toast.makeText(activity,ss,Toast.LENGTH_LONG).show();
    }

}
