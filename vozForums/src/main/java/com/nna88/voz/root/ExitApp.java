package com.nna88.voz.root;

import android.os.AsyncTask;

public class ExitApp extends AsyncTask<String, Void, Void>{

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		if (Shell.SU.available()) {
			Shell.SU.run(new String[] {
					"id",
					"am force-stop com.nna88.voz.main"
			});
		}
		try { Thread.sleep(3000); } catch(Exception e) { }
		return null;
	}



}
