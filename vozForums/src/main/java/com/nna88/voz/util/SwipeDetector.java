package com.nna88.voz.util;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SwipeDetector implements OnTouchListener {

	public static final int DIRECTION_SWIPE_LEFT = 2;
	public static final int DIRECTION_SWIPE_RIGHT = 3;
	public static final int DIRECTION_SWIPE_UP = 4;
	public static final int DIRECTION_SWIPE_DOWN = 5;
	public static final int DIRECTION_SWIPE_CLICK = 0;
	private long downTime;
	private float startX, startY;
	private float MIN_SWIPE_DISTANCE = 100;
	private float MAX_CLICK_DISTANCE = 2;
	
	private OnSwipeListener mListener;

	public void setOnSwipeListener(OnSwipeListener listener)
	{
		mListener = listener;
	}
	private void log(String ss){
		Log.d("nna",ss)  ;
	}
	@Override
	public boolean onTouch(View view, MotionEvent event)
	{
		// if no listener has been registered, ignore the event.
		
		if(mListener == null)
			return true;
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			downTime = System.currentTimeMillis();
			startX = event.getRawX();
			startY = event.getRawY();
			//log("StartX:"+startX+";StartY:"+startY);
			return false;
		}
		if(event.getAction() == MotionEvent.ACTION_UP){
			float endX, endY;
			endX = event.getRawX();
			endY = event.getRawY();
			//log("EndX:"+endX+";EndY:"+endY);
			if((startX - endX) <= MAX_CLICK_DISTANCE && (startY - endY) < MAX_CLICK_DISTANCE){
				mListener.onSwipe(view,DIRECTION_SWIPE_CLICK);
				return false;
			}
		}
		if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
		{
			float endX, endY;
			endX = event.getRawX();
			endY = event.getRawY();
			//log("EndX:"+endX+";EndY:"+endY);
				
			long upTime = System.currentTimeMillis();
			long swipeTime = (upTime - downTime) / 1000;

			float swipeDistance = (float)Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
			float velocity = swipeDistance / swipeTime;

			if(swipeDistance >= MIN_SWIPE_DISTANCE)
			{
				float dx = Math.abs(startX - endX);
				float dy = Math.abs(startY - endY);
				if (startX > endX)
					mListener.onSwipe(view,DIRECTION_SWIPE_LEFT);
				else
					mListener.onSwipe(view,DIRECTION_SWIPE_RIGHT);
//				if(dy > dx)
//				{
//					if (startY > endY)
//						mListener.onSwipe(DIRECTION_SWIPE_DOWN);
//					else
//						mListener.onSwipe(DIRECTION_SWIPE_UP);
//				}
//				else
//				{
//					if (startX > endX)
//						mListener.onSwipe(DIRECTION_SWIPE_LEFT);
//					else
//						mListener.onSwipe(DIRECTION_SWIPE_RIGHT);
//				}
			}

			return false;
		}

		return false;
	}

	public interface OnSwipeListener
	{
		void onSwipe(View v,int direction); // 0=left, 1=right, 2=up, 3=down
	}
}
