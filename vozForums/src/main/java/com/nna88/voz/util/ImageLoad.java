package com.nna88.voz.util;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;

public class ImageLoad {
	public DisplayImageOptions options;
	public ImageLoader imageLoader;
	public ImageLoad(){
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
//                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Config.ARGB_8888)
                .delayBeforeLoading(0)
                .build();
//		File cacheDir = StorageUtils.getOwnCacheDirectory(context, "vozForums/Cache");
//		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
//	        //.memoryCacheExtraOptions(1280, 800) // max width, max height
//	        .threadPoolSize(10)
//	        .threadPriority(Thread.NORM_PRIORITY - 1)
//	        .denyCacheImageMultipleSizesInMemory()
//	        //.memoryCache(new WeakMemoryCache())
//	        .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // You can pass your own memory cache implementation
//	        .discCache(new UnlimitedDiscCache(cacheDir)) // You can pass your own disc cache implementation
//	        .discCacheSize(50 * 1024 * 1024)
//	        .discCacheFileCount(100)
//	        .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
//	        .tasksProcessingOrder(QueueProcessingType.FIFO)
//	        .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//	        //.defaultDisplayImageOptions(options)
//	        //.enableLogging()
//	        .build();
//		imageLoader.init(config);
		
			
	}
}
