package com.nna88.voz.util;

import java.util.ArrayList;


public class spanable {
	ArrayList<String> listString;
	ArrayList<Integer> listStart;
	ArrayList<Integer> listEnd;
	public spanable(){
		this.listString = new ArrayList<String>();
		this.listStart = new ArrayList<Integer>();
		this.listEnd = new ArrayList<Integer>();
	}
	public void add(String str,int start,int end){
		listEnd.add(end);
		listStart.add(start);
		listString.add(str);
	}
	public int getSize(){
		return listString.size();
	}
	public String getStr(int index){
		if(index >= getSize() )		return "";
		return listString.get(index);
	}
	public Integer getStart(int index){
		if(index >= getSize() ) return 0;
		return listStart.get(index);
	}
	
	public Integer getEnd(int index){
		if(index >= getSize() ) return 0;
		return listEnd.get(index);
	}
}
