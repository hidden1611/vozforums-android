package com.nna88.voz.util;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.nna88.voz.main.R;

public class DialogGoFast{
	public Dialog dialog;
	private AutoCompleteTextView edit1;
	public TextView textTitle;
	public Button OK;
	public Button Cancle;
	public DialogGoFast(Context mContext,String title){
	    dialog = new Dialog(mContext);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.go_address);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.FILL_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		
		dialog.getWindow().setAttributes(lp);
		edit1 = (AutoCompleteTextView)dialog.findViewById(R.id.alert_edit1);
		textTitle = (TextView) dialog.findViewById(R.id.alert_title);
		textTitle.setText(title);
		OK = (Button) dialog.findViewById(R.id.alert_ok);
		Cancle = (Button) dialog.findViewById(R.id.alert_cancle);
		show();
	}
	
	public String text(){
		return edit1.getText().toString();
	}
	public void show(){
		if(!dialog.isShowing()) 
			dialog.show();
	}
	public void hide(){
		if(dialog.isShowing())	
			dialog.hide();
	}

}
