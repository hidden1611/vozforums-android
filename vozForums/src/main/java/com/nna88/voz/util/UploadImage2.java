package com.nna88.voz.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;


/**
 * Created by An on 6/5/13.
 */
public class UploadImage2 extends AsyncTask {
    private Context mContext;
    public String url = null;
    public String urlPost;
    private Uri mUrlImage;
    public UploadImage2(Context context, Uri uri){
        mContext = context;
        mUrlImage = uri;
    }
    @Override
    protected Object doInBackground(Object... objects) {

        url = uploadimg(mUrlImage);

        return null;
    }
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = mContext.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public File getResizedBitmap(Bitmap bm) {
        int newWidth = 640;
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth, scaleHeight;
        Bitmap resizedBitmap;
        File myDir=new File("/mnt/sdcard/vozforums/tmp");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        if(width >= newWidth){
            scaleWidth = (float) (640.0/width);
            scaleHeight= scaleWidth;
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);
            // "RECREATE" THE NEW BITMAP
            resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        }else
            resizedBitmap = bm;
        try {
            FileOutputStream out = new FileOutputStream(file);
            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
        // CREATE A MATRIX FOR THE MANIPULATION

    }
    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);
        return bm;
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            } else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }

        return inSampleSize;
    }
    private String  uploadimg(Uri path){
        try {
            Bitmap bitmaptemp = decodeSampledBitmapFromUri(getRealPathFromURI(path),640,640);
            File file = getResizedBitmap(bitmaptemp);
            final String upload_to = "https://api.imgur.com/3/upload.json";
            final String API_key = "6fb648c1b59b327";//f848fee3201af17
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(upload_to);
            httpPost.setHeader("Authorization", "Client-ID "+ API_key);
            try {
                final MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
    //            entity.addPart("image", new FileBody(new File(getRealPathFromURI(path))));
                entity.addPart("image", new FileBody(file));
                entity.addPart("key", new StringBody(API_key));
                entity.addPart("description",new StringBody("test upimgae"));
                httpPost.setEntity(entity);
                final HttpResponse response = httpClient.execute(httpPost, localContext);
                final String response_string = EntityUtils.toString(response.getEntity());
                final JSONObject json = new JSONObject(response_string);
                String link = json.getJSONObject("data").getString("id");
                link = "http://i.imgur.com/"+link+"l.png";
                file.deleteOnExit();
                return link;
            } catch (Exception e) {
                e.printStackTrace();
                file.deleteOnExit();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
