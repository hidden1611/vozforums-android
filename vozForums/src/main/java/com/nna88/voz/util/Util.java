/*****************************************************************************
 * Util.java
 *****************************************************************************
 * Copyright © 2011-2012 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package com.nna88.voz.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;


public class Util {
	 public static int convertPxToDp(Context context, int px) {
	        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
	        Display display = wm.getDefaultDisplay();
	        DisplayMetrics metrics = new DisplayMetrics();
	        display.getMetrics(metrics);
	        float logicalDensity = metrics.density;
	        int dp = Math.round(px / logicalDensity);
	        return dp;
	    }
	    public static int convertDpToPx(Context context,int dp) {
	    	WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
	        Display display = wm.getDefaultDisplay();
	        DisplayMetrics metrics = new DisplayMetrics();
	        display.getMetrics(metrics);
	        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,metrics));
	   }
	   public static float pixelsToSp(Context context, Float px) {
		   float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
		   return px/scaledDensity;
	   }
	   public static float spTopixels(Context context, Float sp) {
		   float px =  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
		   return px;
	   }
	   //int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, getResources().getDisplayMetrics());
}
