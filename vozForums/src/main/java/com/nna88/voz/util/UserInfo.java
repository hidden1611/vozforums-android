package com.nna88.voz.util;

import android.graphics.Bitmap;
import android.webkit.CookieSyncManager;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.BasicCommentHandler;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserInfo {
	String mUser;
	String mPass;
	Map<String, String> cookies ;
    public static BasicCookieStore mCookieStore;

	String UserId;
	boolean isLogin;
	Bitmap avatar =  null;

	String securityToken = "guest";
		
	public UserInfo(){
		
	}
	public Bitmap getAvatar(){
		return avatar;
	}
	public void setAvatart(Bitmap avatar){
		this.avatar = avatar;
	}
	public void cookies(Map<String, String> m_cookies){
		cookies = m_cookies;
        setCookieStore(cookies);
	}
    public String getCookieString(){
        String result = "";
        List<Cookie> cookieList = mCookieStore.getCookies();
        for (Cookie cookie : cookieList) {
            result += cookie.getName() + "=" + cookie.getValue() + "; ";
        }
        result += "vflastactivity=0; vflastvisit=1400060756";
        return result;
    }
	public Map<String, String> cookies(){
        Map<String,String> map = new HashMap<String, String>();
        try {
            List<Cookie> cookieList = mCookieStore.getCookies();
            for (Cookie cookie : cookieList) {
                map.put(cookie.getName(),cookie.getValue());
            }
            map.put("vflastactivity","0");
            map.put("vflastvisit","1400060756");
            map.put("nnc","1");
        }catch (Exception e){
            return null;
        }

		return map;
	}
	public void SetUser(String muser){
		mUser = muser;
	}
	public UserInfo(String muser,String mpass){
		mUser = muser;
		mPass = mpass;
	}
    public void setCookieStore(Map<String,String> map){
        mCookieStore = new BasicCookieStore();
        if(map != null) {
            BasicClientCookie basicClientCookie;
            basicClientCookie = new BasicClientCookie("vfuserid", map.get("vfuserid"));
            mCookieStore.addCookie(basicClientCookie);
            basicClientCookie = new BasicClientCookie("vfpassword", map.get("vfpassword"));
            mCookieStore.addCookie(basicClientCookie);
            basicClientCookie = new BasicClientCookie("vfimloggedin", map.get("vfimloggedin"));
            mCookieStore.addCookie(basicClientCookie);
            basicClientCookie = new BasicClientCookie("vfsessionhash", map.get("vfsessionhash"));
            mCookieStore.addCookie(basicClientCookie);
        }

    }
    public String getMultiquoteId(){
        try {
            List<Cookie> cookieList = mCookieStore.getCookies();
            for (Cookie cookie : cookieList) {
                if(cookie.getName().equals("vbulletin_multiquote")){
                    return cookie.getValue();
                }
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }
    public boolean isMultiquote(){
        try {
            List<Cookie> cookieList = mCookieStore.getCookies();
            for (Cookie cookie : cookieList) {
                if(cookie.getName().equals("vbulletin_multiquote")){
                    return true;
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }
    public void addMultiquote(String vbulletin_multiquote){
//        mUser.cookies().put("vbulletin_multiquote",vbulletin_multiquote);
        try {
            mCookieStore.addCookie(new BasicClientCookie("vbulletin_multiquote", vbulletin_multiquote));
        }catch (Exception e){
           e.printStackTrace();
        }
    }

    public String getCookiStore(){
        String result="";
        try {
            List<Cookie> cookieList = mCookieStore.getCookies();
            for (Cookie cookie : cookieList) {
                result += cookie.getName() + "=" + cookie.getValue() + ";";
            }
        }catch (Exception e){
            return null;
        }
        return result;
    }
    public BasicCookieStore getCookieStore(){
        BasicCookieStore basicCookieStore = new BasicCookieStore();
        BasicClientCookie basicClientCookie = new BasicClientCookie("vflastvisit",cookies.get("vflastvisit"));
        basicCookieStore.addCookie(basicClientCookie);
        basicClientCookie = new BasicClientCookie("vflastactivity",cookies.get("vflastactivity"));
        basicCookieStore.addCookie(basicClientCookie);
        basicClientCookie = new BasicClientCookie("vfuserid",cookies.get("vffuserid"));
        basicCookieStore.addCookie(basicClientCookie);
        basicClientCookie = new BasicClientCookie("vfpassword",cookies.get("vfpassword"));
        basicCookieStore.addCookie(basicClientCookie);
        basicClientCookie = new BasicClientCookie("vfimloggedin",cookies.get("vfimloggedin"));
        basicCookieStore.addCookie(basicClientCookie);
        basicClientCookie = new BasicClientCookie("vfsessionhash",cookies.get("vfsessionhash"));
        basicCookieStore.addCookie(basicClientCookie);
        return  basicCookieStore;
    }
	public void setLogin(boolean bool){
		isLogin = bool;
	}
	public boolean isLogin(){
		return isLogin;
	}
	public void setUserId(String mUserId){
		UserId = mUserId;
	}
	public String UserId(){
		return UserId;
	}
	public void setToken(String securityToken){
		this.securityToken = securityToken;
	}

    public String Token(){
		return securityToken;
	}
    public void setUser(String mUser){
        this.mUser = mUser;
    }
	public String User(){
		return mUser;
	}
	public String Pass(){
		return mPass;
	}
	public void add(String muser,String mpass){
		mUser = muser;
		mPass = mpass;
	}
	public String md5Pass(){
		return md5(mPass);
	}
	private String md5(String md5){
		StringBuffer sb = new StringBuffer();
		try {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        byte[] array = md.digest(md5.getBytes());
	        System.out.println(array);
	        for (int i = 0; i < array.length; ++i) {
	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
	        }
		} catch (java.security.NoSuchAlgorithmException e) {
	   
		}
		return sb.toString();
	}
}
