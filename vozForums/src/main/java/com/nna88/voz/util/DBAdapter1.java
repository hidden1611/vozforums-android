package com.nna88.voz.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter1 {
	/****************TABLE and property database**************************/
	public static final String KEY_ROWID = "_id";
	public static final String KEY_THREAD = "thread";
	public static final String KEY_URLTHREAD= "urlthread";
	private static final String DATABASE_NAME="MyDB1";
	private static final String DATABASE_TABLE="VOZ";
	private static final int DATABESE_VERSION = 1;	
	private static final String DATABASE_CREATE = 
		"create table "+ DATABASE_TABLE +"(_id integer primary key autoincrement, "
		+ "thread text not null, urlthread text not null);";
	/**********************Declaration database***************************/
	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;	
	
	public DBAdapter1 (Context ctx){
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);	
		
		
	}
	/*****************Class Database helper*********************************/
	private static class DatabaseHelper extends SQLiteOpenHelper{

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABESE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXITS "+ DATABASE_TABLE);
			onCreate(db);
		}
	}
	public DBAdapter1 open(){
		db = DBHelper.getWritableDatabase();
		return this;
	}
	public void close(){
		DBHelper.close();
	}
	public long insertContact(String thread, String urlthread){
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_THREAD, thread);
		initialValues.put(KEY_URLTHREAD, urlthread);
		return db.insert(DATABASE_TABLE, null, initialValues);
	}
	public Cursor getThread(String selection,String[] SelectionArgs, String groupBy, String having, String order){
		Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,KEY_THREAD,KEY_URLTHREAD},
				selection,SelectionArgs,groupBy,having,order,null);
		return mCursor;
	}
	public Cursor getAllContacts(){
		return db.query(DATABASE_TABLE, new String[]{KEY_ROWID,KEY_THREAD,KEY_URLTHREAD},
				null, null, null, null, null);
	}
	public boolean deleteThread(String thread){
		return db.delete(DATABASE_TABLE, KEY_THREAD + "='"+thread+"'",null) > 0;
	}
}
