package com.nna88.voz.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.nna88.voz.contain.Post;
import com.nna88.voz.listview.listViewCustom3;
import com.nna88.voz.listview.listViewCustom3.OnImageClickListestener;
import com.nna88.voz.quickaction.ActionItem;
import com.nna88.voz.quickaction.QuickAction;
import com.nna88.voz.ui.SidebarAdapter;
import com.nna88.voz.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;


public class Page3 extends Page{
    private ArrayList<Post> ListContains;
    private listViewCustom3 adapter;
    private int iPostType;
    private String mTextTitle;
    private QuickAction mQuickAction;
    private String url;
    private FullScreenImage fullScreen;
    private String mPostId;
    private int iCallFromNotification=1;
    private int iCallFromNotificationQuote=1;
    private ArrayList<TaskLoadAvatart> mListTaskDownAvatart;
    private ArrayList<DownImageAttach> mListTaskImageAttach;
    private String recaptcha_challenge_field;
    private String humanverify_hash;
    private String linkapicaptcha;
    private String urlCaptcha;
    private Bitmap bmImageStart;
    private Bitmap bmImageFailed;
    private ArrayList<String> lImage;
    private ArrayList<EmoClass2> lEmo;
    private DisplayImageOptions optionsImage;
    private ImageSize mImageSize;

    private class EmoClass2{
        public int i;
        public int j;
        public String url;
        public EmoClass2(String url,int i,int j){
            this.i = i;
            this.j = j;
            this.url = url;
        }
    }
    private MyNetwork myNetwork;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        iPage=Global.PAGE_3;
        super.onCreate(savedInstanceState);
        ListContains = new ArrayList<>();
        mListTaskImageAttach = new ArrayList<>();
        mListTaskDownAvatart = new ArrayList<>();
        bmImageStart = BitmapFactory.decodeResource(getResources(), R.drawable.stub_image);
        bmImageFailed = BitmapFactory.decodeResource(getResources(), R.drawable.image_for_empty_url);
        adapter = new listViewCustom3(mContext,ListContains,bmImageStart,mTextSize);
        mObjectAdapter = adapter;
        adapter.setSize(mTextSize);
        mList.setAdapter(adapter);
        myNetwork = new MyNetwork();
        if(getIntent().getIntExtra("NOTIFICATION",1)!= 1){
            iCallFromNotification = getIntent().getIntExtra("NOTIFICATION",1);
        }
        if(getIntent().getIntExtra("NOTIFICATIONQUOTE", 1)!= 1){
            iCallFromNotificationQuote = getIntent().getIntExtra("NOTIFICATIONQUOTE",1);
        }
        url = getIntent().getStringExtra("URL");
        if(url.contains(Global.URL) || url.contains(Global.URL2))
            mParser.setUrl(getIntent().getStringExtra("URL"));
        else
            mParser.setUrl(Global.URL+getIntent().getStringExtra("URL"));
        mTask.execute(Global.TASK_GETDOC);

//      fullScreen = new FullScreenImage(mContext);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if(fullScreen == null || (mUser.isLogin() && !fullScreen.isShowing())){
                    try {
                        ActionPopup(view,pos-1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        adapter.setOnImageClickListener(new OnImageClickListestener() {
            @Override
            public void onImageClick(int type, final String urlImage) {
                // TODO Auto-generated method stub
                if (type == 0) {
                    if (urlImage.contains("attachmentid") || urlImage.contains("http://") || (urlImage.contains("https://"))) {
//                        fullScreen.show(Page3.this.getWindow().getDecorView(),lImage, urlImage);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fullScreen = new FullScreenImage(mContext, getWindow().getDecorView());
                                fullScreen.show(urlImage);
                                if (mQuickAction != null)
                                    mQuickAction.dismiss();
                            }
                        });
                    }
                } else if (type == 1) {
                    toast("onquote click");
                }

            }
        });

        optionsImage = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();
        mImageSize =  new ImageSize(Global.width/4,Global.width/4);
        hideAds();
    }

    private class TaskLoadAvatart{
        int index;
        ImageLoader imageLoader;
        public TaskLoadAvatart(int i,ImageLoader imageloader){
            index = i;
            this.imageLoader = imageloader;
        }
        public void cancle(){
            imageLoader.stop();
        }
        public void execute(){
            imageLoader.loadImage(ListContains.get(index).UrlAvatar(), mImageLoad.options, new ImageLoadingListener() {
                @Override
                public void onLoadingCancelled(String arg0, View arg1) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onLoadingComplete(String arg0, View arg1,
                                              Bitmap arg2) {
                    // TODO Auto-generated method stub
                    if (ListContains.size() > index)
                        ListContains.get(index).setAvatar(arg2);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onLoadingFailed(String arg0, View arg1,
                                            FailReason arg2) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onLoadingStarted(String arg0, View arg1) {
                    // TODO Auto-generated method stub
                }
            });
        }

    }
    private void downloadAttach(){
        int size1,size2;
        size1 = ListContains.size();
        for(int i=0;i<size1;i++){
            Post post = ListContains.get(i);
            size2 = post.image.sizeBitmap();
            for(int j=0;j<size2;j++){

                if(post.image.getStr(j).contains("attachment.php")){
                    mListTaskImageAttach.add(new DownImageAttach());
                    mListTaskImageAttach.get(mListTaskImageAttach.size()-1).execute(i,j);
                }
            }
        }
    }
    private class DownImageAttach extends AsyncTask<Integer, Void, String>{

        @Override
        protected String doInBackground(Integer... params) {
            // TODO Auto-generated method stub
            if(!mUser.isLogin()){
                return null;
            }
            if((ListContains.size() > params[0]) && (ListContains.get(params[0]).image.sizeBitmap() > params[1])){
                if(!ListContains.get(params[0]).image.getBitmap(params[1]).equals(bmImageStart)){
                    return null;
                }

                try {
                    String url = ListContains.get(params[0]).image.getStr(params[1]);
                    if(!url.contains("https://vozforums.com/")) url = "https://vozforums.com/" + url;
                    Response resultImageResponse = Jsoup.connect(url).cookies(mUser.cookies()).ignoreContentType(true).execute();
                    byte[] data=resultImageResponse.bodyAsBytes();
//                    if((ListContains.size() > params[0]) && (ListContains.get(params[0]).image.sizeBitmap() > params[1]))
//                        ListContains.get(params[0]).image.SetBitmap(params[1],NnaBitmap.decodeSampledBitmapFromResource(data,(Global.width*9)/10,Global.height) );
                    return "OK";
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    if((ListContains.size() > params[0]) && (ListContains.get(params[0]).image.sizeBitmap() > params[1]))
                        ListContains.get(params[0]).image.SetBitmap(params[1], bmImageFailed);
                    e.printStackTrace();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    if((ListContains.size() > params[0]) && (ListContains.get(params[0]).image.sizeBitmap() > params[1]))
                        ListContains.get(params[0]).image.SetBitmap(params[1], bmImageFailed);
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(result != null)
                adapter.notifyDataSetChanged();
        }

    }
    private String getPostIdFromUrl(String url){
        String postId = null;
        try {
            if(url.contains("p=")){
                postId = url.split("p=")[1];
            }
            if(postId.contains("#post")){
                postId = postId.split("#post")[1];
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return postId;
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if(fullScreen!= null && fullScreen.isShowing()){
            fullScreen.hide();
        }else{
            if(iCallFromNotification != 1){
                startActivity(new Intent(this, PageCP.class));
            }
            if(iCallFromNotificationQuote != 1){
                Intent i = new Intent(this, PageQuote.class);
                //toast("notification:"+iCallFromNotificationQuote);
                i.putExtra("NUMQUOTE", iCallFromNotificationQuote);
                startActivity(i);
            }
            super.onBackPressed();
        }

    }
    void ActionPopup(final View arg1,final int pos) throws Exception{
        ActionItem replyItem 	= new ActionItem(1, "Reply", getResources().getDrawable(R.drawable.tapatalk_bubble_reply));
        ActionItem quoteItem 	= new ActionItem(2, "Quote",getResources().getDrawable(R.drawable.tapatalk_bubble_quote) );
        ActionItem multiItem 	= new ActionItem(3, "Multi",getResources().getDrawable(R.drawable.tapatalk_bubble_multi) );
        ActionItem editItem 	= new ActionItem(4, "Edit ",getResources().getDrawable(R.drawable.tapatalk_bubble_edit) );
        ActionItem deleteItem 	= new ActionItem(5, "Delete",getResources().getDrawable(R.drawable.tapatalk_bubble_delete));
        ActionItem pmItem 	= new ActionItem(6, " PM  ",getResources().getDrawable(R.drawable.tapatalk_bubble_pm));
        ActionItem findPostItem 	= new ActionItem(7, "Find Post",getResources().getDrawable(R.drawable.tapatalk_find_post));
        mQuickAction 	= new QuickAction(mContext);
        mQuickAction.addActionItem(replyItem);
        mQuickAction.addActionItem(quoteItem);
        mQuickAction.addActionItem(multiItem);
        mQuickAction.addActionItem(pmItem);
        if(ListContains.get(pos).m_UserId.equals(mUser.UserId())){
            mQuickAction.addActionItem(editItem);
            mQuickAction.addActionItem(deleteItem);
        }
        mQuickAction.addActionItem(findPostItem);
        mQuickAction.show(arg1);
        mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                if (actionId == 3) {
                    clickMultiQuote(pos);
                } else if (actionId == 7) {
                    if(mUser.isLogin()){
                        Intent i = new Intent(mContext,PageRecentPost.class);
                        i.putExtra("USERID",ListContains.get(pos).m_UserId);
                        startActivity(i);
                    }
                } else if (actionId == 6) {
                    Intent i = new Intent(mContext, PageNewThread.class);
                    Bundle extras = new Bundle();
                    extras.putInt("POSTTYPE", Global.REQUEST_PMNEW);
                    extras.putString("MID", ListContains.get(pos).m_UserId);
                    i.putExtras(extras);
                    startActivityForResult(i, Global.REQUEST_PMNEW);
                } else {
                    String urlGetPageRepley = "https://vozforums.com/"+doc.select("a:has(img[src=images/buttons/reply.gif])").attr("href");
                    if (actionId == 1){
                        iPostType = Global.REQUEST_REPLY;
                    } else if (actionId == 2) {
                        iPostType = Global.REQUEST_QUOTE;
                        urlGetPageRepley = "https://vozforums.com/newreply.php?do=newreply&p="+ListContains.get(pos).Id();
                    }else if (actionId == 5){
                        urlGetPageRepley = "https://vozforums.com/editpost.php?do=editpost&p="+ListContains.get(pos).Id();
                        iPostType = Global.REQUEST_DELETE;
                    }else if (actionId == 4){
                        iPostType = Global.REQUEST_EDIT;
                        urlGetPageRepley = "https://vozforums.com/editpost.php?do=editpost&p="+ListContains.get(pos).Id();
                    }
                    myNetwork.getDoc(urlGetPageRepley,new MyNetwork.OnResult() {
                        @Override
                        public void onResultListenter(Document doc, int status) {
                            doc2 = doc;
                            if(iPostType == Global.REQUEST_DELETE){
                                //postDelete(ListContains.get(pos).Id());
                                TaskCancle();
                                mTask = new TaskGetHtml();
                                mTask.execute(Global.TASK_POSTDELETE);
                            }else{
                                try {
                                    if (doc2 == null) return;
                                    String mTextQuote = "";
                                    Intent i = new Intent(mContext, PageNewThread.class);
                                    Bundle extras = new Bundle();
                                    if (iPostType != Global.REQUEST_REPLY) {
                                        Element eleQuote = doc2.select("textarea[name=message]").first();
                                        if (eleQuote != null)
                                            mTextQuote = eleQuote.textNodes().get(0).getWholeText();
                                    }
                                    extras.putString("msg", mTextQuote);
                                    extras.putString("title", mTextTitle);
                                    extras.putFloat("textsize", mTextSize);
                                    extras.putInt("POSTTYPE", iPostType);
                                    i.putExtras(extras);
                                    startActivityForResult(i, iPostType);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        });
    }
    private void clickMultiQuote(int index){
        if(mUser.isLogin() == true){
            String vbulletin_multiquote;
            if(ListContains.get(index).isMultiQuote()){
                ListContains.get(index).setMultiQuote(false);
                String temp =mUser.cookies().get("vbulletin_multiquote").replace(ListContains.get(index).Id(), "");
                if(temp.contains("%2C") && temp.subSequence(0, 3).equals("%2C"))
                    temp = temp.substring(3, temp.length());
                if(temp.contains("%2C")&& temp.subSequence(temp.length()-3, temp.length()).equals("%2C"))
                    temp = temp.substring(0, temp.length()-3);
                if(temp.contains("%2C%2C")) temp = temp.replace("%2C%2C", "%2C");
                vbulletin_multiquote = temp;
            }else{
                ListContains.get(index).setMultiQuote(true);
                if(mUser.cookies().containsKey("vbulletin_multiquote") != true){
                    vbulletin_multiquote = ListContains.get(index).Id();
                }else{
                    vbulletin_multiquote = mUser.cookies().get("vbulletin_multiquote")+"%2C"+
                            ListContains.get(index).Id();
                }
            }

//            mUser.cookies().put("vbulletin_multiquote",vbulletin_multiquote);
            mUser.addMultiquote(vbulletin_multiquote);
            adapter.notifyDataSetChanged();
        }

    }
    @Override
    protected void TaskCancle() {
        // TODO Auto-generated method stub
        super.TaskCancle();
        clearTask();
        ListContains.clear();
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        adapter.destroy();
    }
    @Override
    protected void updateSetFont(float value) {
        // TODO Auto-generated method stub
        super.updateSetFont(value);
        adapter.setSize(value);
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch (requestCode) {
                case Global.REQUEST_REPLY:
                case Global.REQUEST_QUOTE:
                    specifiedpost="1";
                    sPost = data.getExtras().getString("msg");
                    if(doc2==null) return;
                    Element eleCaptcha = doc2.select("fieldset[class=fieldset]:has(input[name*=humanverify]").first();
                    if(eleCaptcha != null){
                        humanverify_hash = eleCaptcha.select("input[name*=humanverify]").attr("value");
                        linkapicaptcha = eleCaptcha.select("iframe[src*=api.recaptcha.net]").attr("src");
                        alertCatcha();
                    }else{
                        TaskCancle();
                        mTask = new TaskGetHtml();
                        mTask.execute(Global.TASK_POSTREPLY);
                    }


                    break;
                case Global.REQUEST_EDIT:
                    sPost = data.getExtras().getString("msg");
                    try {
                        String sign = "\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL]"+ "[/I]";
                        if(sPost.indexOf(sign) != -1)                    sPost = sPost.substring(0,sPost.indexOf(sign));
                        sPost += "\n\n[I]Sent from my "+Global.sYourDevice +" using vozForums"+ "[/I]";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    TaskCancle();
                    mTask = new TaskGetHtml();
                    mTask.execute(Global.TASK_POSTEDIT);
                    //postEdit();
                    break;
                case Global.REQUEST_PMNEW:
                    startActivity(new Intent(mContext,PagePM.class));
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        updateSetFont(mTextSize);
    }
    private void getImageCaptcha(final ImageView img){
        myNetwork.getDoc(linkapicaptcha, new MyNetwork.OnResult() {
            @Override
            public void onResultListenter(Document doc, int status) {
                Document doctemp = doc;
                urlCaptcha = "http://api.recaptcha.net/" + doctemp.select("img[src]").attr("src");
                recaptcha_challenge_field = doctemp.select("input[name*=recaptcha_challenge_field]").attr("value");
                mImageLoad.imageLoader.loadImage(urlCaptcha, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        int width = bitmap.getWidth();
                        int height = bitmap.getHeight();
                        float scaleWidth, scaleHeight;
                        int dpw = Util.convertDpToPx(mContext, 480);
                        int dph = Util.convertDpToPx(mContext, 360);
                        scaleWidth = ((float) (dpw) / (float) width);
                        scaleHeight = ((float) (dph) / (float) width);
                        Matrix matrix = new Matrix();
                        // RESIZE THE BIT MAP
                        matrix.postScale(scaleWidth, scaleHeight);
                        // "RECREATE" THE NEW BITMAP
                        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
                        img.setImageBitmap(resizedBitmap);

                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
            }
        });

    }

    private void alertCatcha(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertcaptcha);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final EditText edit1 = (EditText) dialog.findViewById(R.id.alert_edit1);
        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
        final ImageView img = (ImageView)dialog.findViewById(R.id.imgCaptcha);
        edit1.setFocusable(true);
        edit1.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getImageCaptcha(img);
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String sign = "\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]";
                if(sPost.indexOf(sign) != -1)             sPost = sPost.substring(0,sPost.indexOf(sign));
                sPost += "\n\n[I]Sent from my "+Global.sYourDevice +" using vozForums[/I]";
                sPost = sPost.replace("[url]","[ur l]");
                sPost = sPost.replace("[URL","[UR L");
                sPost = sPost.replace("[IMG","[IM G");
                //myVolley.PostReplyQuoteCatcha(doc2, mUser, sPost, humanverify_hash, recaptcha_challenge_field, edit1.getText().toString());
                myNetwork.PostReplyQuoteCatcha(doc2, new MyNetwork.OnResult() {
                    @Override
                    public void onResultListenter(Document doctemp, int status) {
                        doc = doctemp;
                        Element eleCheckPost = doc.select("table[class=tborder]:contains(The following errors occurred)").first();
                        if( eleCheckPost!= null){
                            try {
                                Parser(0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }else{
                            try {
                                Parser(0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },sPost, humanverify_hash, recaptcha_challenge_field, edit1.getText().toString());
                dialog.dismiss();
            }
        });
        buttonCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getImageCaptcha(img);
            }
        });
        dialog.show();
    }
    @Override
    void Parser(int taskType) throws Exception {
        super.Parser(taskType);
        if(doc==null) return;

        if(taskType == Global.TASK_GETQUOTE){
            if(iPostType == Global.REQUEST_DELETE){
                mTask.cancel(true);
                mTask = new TaskGetHtml();
                mTask.execute(Global.TASK_POSTDELETE);
            }else{
                String mTextQuote = "";
                Intent i = new Intent(mContext, PageNewThread.class);
                Bundle extras = new Bundle();
                if(iPostType !=  Global.REQUEST_REPLY){
                    Element eleQuote = doc.select("textarea[name=message]").first();
                    if(eleQuote != null)
                        mTextQuote = eleQuote.textNodes().get(0).getWholeText();
                }
                extras.putString("msg",mTextQuote);
                extras.putString("title", mTextTitle);
                extras.putFloat("textsize", mTextSize);
                extras.putInt("POSTTYPE", iPostType);
                i.putExtras(extras);
                startActivityForResult(i,iPostType);
            }
        }else if(taskType == Global.TASK_POSTREPLY){
            Element eleError = doc.select("table[class=tborder]:contains(The following errors occurred)").first();
            if(eleError!=null && eleError.text().contains("To be able to post links or images your post count")){
                try {
                    toast(eleError.text());
                    String sign = "\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL]"+ "[/I]";
                    if(sPost.contains(sign)){
                        sPost = sPost.substring(0,sPost.indexOf(sign));
                        sPost += "\n\n[I]Sent from my "+Global.sYourDevice +" using vozForums"+ "[/I]";
                        toast("Try Again");
                        doc2 = doc;
                        TaskCancle();
                        mTask = new TaskGetHtml();
                        mTask.execute(Global.TASK_POSTREPLY);
                    }else{
                        iPostType = Global.REQUEST_QUOTE;
                        String mTextQuote = "";
                        Intent i = new Intent(mContext, PageNewThread.class);
                        Bundle extras = new Bundle();
                        if(iPostType !=  Global.REQUEST_REPLY){
                            Element eleQuote = doc.select("textarea[name=message]").first();
                            if(eleQuote != null)
                                mTextQuote = eleQuote.textNodes().get(0).getWholeText();
                        }

                        extras.putString("msg",mTextQuote);
                        extras.putString("title", mTextTitle);
                        extras.putFloat("textsize", mTextSize);
                        extras.putInt("POSTTYPE", iPostType);
                        i.putExtras(extras);
                        startActivityForResult(i,iPostType);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else
                Parser(0);
        }else{
            log("start parser");
            ListContains.clear();
            Post mContain;
            Element mainElement;
            if(doc.select("div[align=center]").size() > 1){
                mainElement = doc.select("div[align=center]").get(1);
            }else{
                mainElement = doc;
            }
            Element eleLogin = mainElement.select("a[href=private.php]").first();
            mPostId = getPostIdFromUrl(mParser.getUrl());
            setPage(parsePage(Global.GO_INDEXPAGE,0));
            if(eleLogin != null){
                String temp = mainElement.select("td[class=alt2]").get(0).select("a[href*=mem]").attr("href");
                temp = temp.split("=")[1];
                mUser.setUserId(temp);
                Element eleSecurity = mainElement.select("input[name*=securitytoken]").first();
                if(eleSecurity!=null){
                    String secToken;
                    secToken = eleSecurity.attr("value");
                    mUser.setToken(secToken);
                }
                Element eleButPost = doc.select("div[style=margin-top:6px]:has(input[value=Post Quick Reply])").first();
                if(eleButPost!=null){
                    sIdThread = eleButPost.select("input[name=t]").attr("value");
                }
                Element eleThreadTools = doc.select("td[class=alt1]:has(a[href *= subscription.php]").first();
                if(eleThreadTools != null){
                    String sSubscribe = eleThreadTools.select("a[href *= subscription.php]").attr("href");

                    if(sSubscribe.contains("removesubscription")){
                        isSubscribe = true;
                        ((SidebarAdapter.Item)mListSideMenu2.get(3)).mTitle= getResources().getString(R.string.UnSubscribe);
                        mAdapterSideMenu2.notifyDataSetInvalidated();
                    }else{
                        ((SidebarAdapter.Item)mListSideMenu2.get(3)).mTitle= getResources().getString(R.string.Subscribe);
                        mAdapterSideMenu2.notifyDataSetInvalidated();
                        isSubscribe = false;
                    }
                }
            }
            String m_user = null,m_time,m_AvatartUrl = null;
            Elements eleTag1s = mainElement.select("td[id*=td_post]");
            Iterator<Element> iteTag1 = eleTag1s.iterator();
            iPositiion = 0;
            while(iteTag1.hasNext()){
                m_AvatartUrl = null;
                mContain = new Post();
                String mUserTitle = "";
                Element eleTag1 = iteTag1.next();
                Element eleContain =  eleTag1.parent();
                if(eleTag1s.select("div[class=smallfont]:has(strong)").first() != null){
                    mTextTitle = eleTag1s.select("div[class=smallfont]:has(strong)").first().text();
                    setTitle(mTextTitle);
                }
                Element eleAvatar = eleContain.previousElementSibling();
                Element eleTime = eleAvatar.previousElementSibling();
                if(eleAvatar.select("img[src*=avatars]").first() != null){
                    m_AvatartUrl = eleAvatar.select("img[src*=avatars]").attr("src");
                    if(!m_AvatartUrl.contains(Global.URL))
                        m_AvatartUrl = Global.URL + m_AvatartUrl;
                }

                if(eleAvatar.select("div:containsOwn(Join Date)").first() != null){
                    String jd = eleAvatar.select("div:containsOwn(Join Date)").first().text();
                    if(jd.contains("Date:")){
                        jd = jd.split("Date:")[1];
                    }
                    mContain.setJD("Jd:"+jd);
                }else{
                    mContain.setJD("");
                }
                if(eleAvatar.select("div:containsOwn(Posts: )").first() != null){
                    String posts = eleAvatar.select("div:containsOwn(Posts: )").first().text();
                    mContain.setPosts(posts);
                }else{
                    mContain.setPosts("");
                }

                if(eleAvatar.select("img[src*=line.gif").first() != null){
                    if(eleAvatar.select("img[src*=line.gif").attr("src").contains("online"))
                        mContain.isOnline = true;
                    else
                        mContain.isOnline = false;
                }

                if(eleAvatar.select("a[class=bigusername]").first() != null){
                    m_user = eleAvatar.select("a[class=bigusername]").text();
                    mContain.m_UserId = eleAvatar.select("a[class=bigusername]").first().attr("href").split("u=")[1];
                }
                if(eleAvatar.select("div[class=smallfont]").first()!=null){
                    mUserTitle = eleAvatar.select("div[class=smallfont]").first().text();
                }


                m_time = eleTime.text();
                Element elePostMessage = eleTag1.select("div[id*=post_message").first();
                if(elePostMessage!= null){
                    if(elePostMessage.attr("id").split("_").length > 2)
                        mContain.setId(elePostMessage.attr("id").split("_")[2]);
                    PaserPage3(elePostMessage,mContain,false);
                }
                Element eleAttachment = eleTag1.select("fieldset[class=fieldset]").first();
                if(eleAttachment!= null){
                    mContain.addText("\n");
                    PaserPage3(eleAttachment,mContain,false);
                }
                mContain.Info(m_user,mUserTitle, m_time, null,m_AvatartUrl);
                if(Global.bSign && eleTag1.select("div:contains(_______)").first() != null){
                    mContain.addText("\n");
                    PaserPage3(eleTag1.select("div:contains(_______)").first(),mContain,false);
                }
                ListContains.add(mContain);
                if(mContain.Id().equals(mPostId))
                    iPositiion = ListContains.indexOf(mContain)+1;
            }
            lImage = new ArrayList<String>();
            int size,size2,i,j;
            // get list image using in click view image
            for(Post mPost:ListContains){
                size = mPost.image.getSize();
                String url;
                for(i=0;i<size;i++){
                    url = mPost.image.getStr(i);
                    if(url.contains("http://") || url.contains("https://")){
                        lImage.add(url);
                    }
                }
            }


            // download avatar, image attach, emo.
            loadAvatarUniversal(0);
            downloadAttach();

            size = ListContains.size();
            lEmo = new ArrayList<EmoClass2>();
            for(i=0;i<size;i++){
                size2 = ListContains.get(i).image.getSize();
                for(j =0;j<size2;j++){
                    if (ListContains.get(i).image.getStr(j).contains("images/smilies")){
                        lEmo.add(new EmoClass2(ListContains.get(i).image.getStr(j),i,j));
                    }
                }
            }
            loadEmo(0);


            // jump post unread
            mItemCount = 20+3;
            if (mItemOffsetY == null) {
                mItemOffsetY = new int[mItemCount+1];
                mItemtemp = new int[mItemCount+1];
            }
            scrollIsComputed = true;
            adapter.notifyDataSetChanged();
            if(iPositiion != 0){
                mList.clearFocus();
                mList.requestFocusFromTouch();
                mList.post(new Runnable() {
                    @Override
                    public void run() {
                        mList.setSelection(iPositiion);
                        mList.clearFocus();
                    }
                });
            }

//            for(Post post:ListContains){
////                loadIamge(post,0);
//            }

            loadIamge();

        }
    }


    private void loadIamge(){
        try {
            for (final Post post : ListContains) {
                int size = post.image.getSize();
                for (int i = 0; i < size; i++){
                    String url = post.image.getStr(i);
                    if (!url.contains("http://") && !url.contains("https://")) {
                        continue;
                    } else if (url.contains("attachmentid")) {
                        continue;
                    } else {
                        log("url " + url);
                        final int finalI = i;
                        imageLoader.loadImage(url,mImageSize, optionsImage, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }
                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                log("failed ");
                                post.image.SetBitmap(finalI, bmImageFailed);
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                log("finish ");
                                try {
                                    if (bitmap != null) {
                                        post.image.SetBitmap(finalI,  getResizedBitmap(bitmap));
                                    }
                                    adapter.notifyDataSetChanged();
                                } catch (Exception e) {
                                    post.image.SetBitmap(finalI, bmImageFailed);
                                    adapter.notifyDataSetChanged();
                                    adapter.notifyDataSetChanged();
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                log("cancel ");
                                post.image.SetBitmap(finalI, bmImageFailed);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            log("excep ");
            e.printStackTrace();
        }
    }

    ImageLoader imageLoader = ImageLoader.getInstance();
    public Bitmap getResizedBitmap(Bitmap bm) {
        try {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth, scaleHeight;
            if(width >= (Global.width*9.0/10.0)){
                scaleWidth =  ((float)(Global.width*9.0/10.0)/(float)width);
                scaleHeight= scaleWidth;
                Matrix matrix = new Matrix();
                // RESIZE THE BIT MAP
                matrix.postScale(scaleWidth, scaleHeight);
                // "RECREATE" THE NEW BITMAP
                Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
                return resizedBitmap;
            }else
                return bm;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        // CREATE A MATRIX FOR THE MANIPULATION
    }
    private void loadEmo(final int i){
        try {
            if(i >= lEmo.size()) return;
            String url = lEmo.get(i).url;
            url = getlinkBitmapAssert(url);
            mImageLoad.imageLoader.loadImage(url, mImageLoad.options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }
                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    try{
                        ListContains.get(lEmo.get(i).i).image.SetBitmap(lEmo.get(i).j, bmImageFailed);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();
                    loadEmo(i + 1);
                }
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    try {
                        if (loadedImage != null)
                            ListContains.get(lEmo.get(i).i).image.SetBitmap(lEmo.get(i).j, loadedImage);
                        adapter.notifyDataSetChanged();
                        loadEmo(i + 1);
                    } catch (Exception e) {
                        try{
                            ListContains.get(lEmo.get(i).i).image.SetBitmap(lEmo.get(i).j, bmImageFailed);
                        }catch (Exception e1){
                            e1.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        loadEmo(i + 1);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    try{
                        ListContains.get(lEmo.get(i).i).image.SetBitmap(lEmo.get(i).j, bmImageFailed);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();
                    loadEmo(i + 1);
                }
            });
        } catch (Exception e) {
            loadEmo( i + 1);
            e.printStackTrace();
        }
    }

//    private void loadIamge2(final Post mPost,final int i){
//        try {
//            if(i >= mPost.image.getSize()) return;
//            String url = mPost.image.getStr(i);
//            if(!url.contains("http://") && !url.contains("https://")  ){
//                loadIamge(mPost,i + 1);
//            }else if(url.contains("attachmentid")){
//                loadIamge(mPost,i + 1);
//            }else{
//                if(Global.iSizeImage == Global.REDUCENONE){
//                    loadIamge(mPost,i + 1);
//                }else if(!mPost.image.getBitmap(i).equals(bmImageStart)){
//                    loadIamge(mPost,i + 1);
//                }else{
//                    imageLoader.loadImage(url, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//
//                        }
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                            mPost.image.SetBitmap(i,bmImageFailed);
//                            adapter.notifyDataSetChanged();
//                            loadIamge(mPost, i + 1);
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//
//                            try {
//                                if(loadedImage != null)
////                                    mPost.image.SetBitmap(i,getResizedBitmap(loadedImage));
//                                    mPost.image.SetBitmap(i,loadedImage);
//                                adapter.notifyDataSetChanged();
//                                loadIamge(mPost,i + 1);
//                            } catch (Exception e) {
//                                mPost.image.SetBitmap(i,bmImageFailed);
//                                adapter.notifyDataSetChanged();
//                                loadIamge(mPost, i + 1);
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String imageUri, View view) {
//                            mPost.image.SetBitmap(i,bmImageFailed);
//                            adapter.notifyDataSetChanged();
//                            loadIamge(mPost, i + 1);
//                        }
//                    });
//                }
//            }
//        } catch (Exception e) {
//            loadIamge(mPost, i + 1);
//            e.printStackTrace();
//        }
//    }

    public Bitmap getResizedBitmapAvatart(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth, scaleHeight;
        return bm;
//        if(Global.ScreenDensity >= DisplayMetrics.DENSITY_HIGH) return bm;
//        else{
//            scaleWidth =  0.5F;
//            scaleHeight= scaleWidth;
//            Matrix matrix = new Matrix();
//            matrix.postScale(scaleWidth, scaleHeight);
//            // "RECREATE" THE NEW BITMAP
//            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
//            return resizedBitmap;
//        }
        // CREATE A MATRIX FOR THE MANIPULATION

    }


    private void loadAvatarUniversal(final int i){
        try {
            if(i >= ListContains.size()) return;
            String url = ListContains.get(i).UrlAvatar();
            mImageLoad.imageLoader.loadImage(url, mImageLoad.options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    loadAvatarUniversal(i + 1);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    try {
                        if (loadedImage != null)
                            ListContains.get(i).setAvatar(getResizedBitmapAvatart(loadedImage));
                        adapter.notifyDataSetChanged();
                        loadAvatarUniversal(i + 1);
                    } catch (Exception e) {
                        loadAvatarUniversal(i + 1);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    loadAvatarUniversal(i + 1);
                }
            });
        } catch (Exception e) {
            loadAvatarUniversal(i + 1);
            e.printStackTrace();
        }
    }

    @Override
    protected void GoPage(int state, int numPage) throws Exception {
        clearTask();
        super.GoPage(state, numPage);
    }

    private void clearTask(){
        for(DownImageAttach loadImageAttach:mListTaskImageAttach){
            if(loadImageAttach != null && !loadImageAttach.isCancelled())
                loadImageAttach.cancel(true);
        }
        for(TaskLoadAvatart loadAvatart:mListTaskDownAvatart){
            if(loadAvatart != null)
                loadAvatart.cancle();
        }
        mListTaskImageAttach = new ArrayList<DownImageAttach>();
        mListTaskDownAvatart = new ArrayList<TaskLoadAvatart>();
    }
    @Override
    protected void onStop() {
        clearTask();
        super.onStop();
    }

    private void PaserPage3(Element ele,Post m_Contain,boolean pre){
        Element eleTemp;
        int start,end;
        if(ele != null){
            for (Node child : ele.childNodes()) {
                if(child instanceof Element){
                    if(((Element)child).tagName().equals("div")){
                        eleTemp = ((Element)child).select("div").first();
                        if(eleTemp.attr("style").contains("padding")){
                            m_Contain.addText("\n");
                        }
                        if(eleTemp.ownText().contains("Originally Posted by")){
                            m_Contain.addText("Originally Posted by ");
                            start = m_Contain.getText().length();
                            m_Contain.addText(eleTemp.select("strong").text());
                            end = m_Contain.getText().length();
                            m_Contain.web.add(Global.URL + eleTemp.select("a").attr("href"), start, end);
                            m_Contain.type.add("", start, end,Typeface.BOLD);
                            m_Contain.addText("\n");

                        }else{
                            PaserPage3(eleTemp,m_Contain,pre);
                            m_Contain.addText("\n");
                        }
                    }else if(((Element)child).tagName().equals("blockquote")){
                        eleTemp = ((Element)child).select("blockquote").first();
                        m_Contain.addText("\n");
                        PaserPage3(eleTemp,m_Contain,pre);
                        m_Contain.addText("\n");
                    }else if(((Element)child).tagName().equals("fieldset")){
                        eleTemp = ((Element)child).select("fieldset").first();
                        m_Contain.addText("\n");
                        PaserPage3(eleTemp,m_Contain,pre);
                        m_Contain.addText("\n");
                    }else if(((Element)child).tagName().equals("b")){
                        eleTemp = ((Element)child).select("b").first();
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp,m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.type.add("", start, end,Typeface.BOLD);
                    }else if(((Element)child).tagName().equals("i")){
                        eleTemp = ((Element)child).select("i").first();
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp,m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.type.add("", start, end,Typeface.ITALIC);
                    }else if(((Element)child).tagName().equals("pre")){
                        eleTemp = ((Element)child).select("pre").first();
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp,m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.quote.add("", start,end);
                    }else if(((Element)child).tagName().equals("table")){
                        eleTemp = ((Element)child).select("table").first();
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp,m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.quote.add("", start,end);
                    }else if(((Element)child).tagName().equals("ol")){
                        eleTemp = ((Element)child).select("ol").first();
                        PaserPage3(eleTemp,m_Contain,pre);
                    }else if(((Element)child).tagName().equals("tbody")){
                        eleTemp = ((Element)child).select("tbody").first();
                        PaserPage3(eleTemp,m_Contain,pre);
                    }else if(((Element)child).tagName().equals("li")){
                        eleTemp = ((Element)child).select("li").first();
                        PaserPage3(eleTemp,m_Contain,pre);
                    }else if(((Element)child).tagName().equals("tr")){
                        eleTemp = ((Element)child).select("tr").first();
                        m_Contain.addText("\n");
                        PaserPage3(eleTemp,m_Contain,pre);
                    }else if(((Element)child).tagName().equals("td")){
                        eleTemp = ((Element)child).select("td").first();
                        PaserPage3(eleTemp,m_Contain,pre);
                    }else if(((Element)child).tagName().equals("img")){
                        String urlImage = (((Element)child).select("img[src]").attr("src"));
                        if(urlImage.contains("https://vozforums.com/")){
                            if(urlImage.subSequence(0, 21).equals("https://vozforums.com/")){
                                if(!urlImage.contains("https://vozforums.com/attachment.php?attachmentid") &&
                                        !urlImage.contains("https://vozforums.com/customavatars/"))
                                    urlImage = urlImage.substring(21);
                            }
                        }
                        if(urlImage.substring(0, 1).equals("/"))
                            urlImage = urlImage.substring(1, urlImage.length());
                        start = m_Contain.getText().length();
                        end = start + urlImage.length();
                        if(!(urlImage.contains("http://")||
                                urlImage.contains("https://")||
                                urlImage.contains("attachment.php?attachmentid"))){
                            end = start + 2;
                            m_Contain.image.add(urlImage, start, end, null);
                            urlImage = "  ";
                        }else{
                            m_Contain.image.add(urlImage, start, end, bmImageStart);
                        }
                        if(!urlImage.contains("images/buttons/viewpost.gif")){
                            m_Contain.addText(urlImage);
                            if(child.hasAttr("onload")){
                                m_Contain.addText("\n");
                            }
                        }
                    }else if(((Element)child).tagName().equals("br")){
                        m_Contain.addText("\n");
                    }else if(((Element)child).tagName().equals("u")){
                        eleTemp = ((Element)child).select("u").first();
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp,m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.typeU.add("", start, end);
                    }else if(((Element)child).tagName().equals("font")){
                        eleTemp = ((Element)child).select("font").first();
                        String mfont="while",msize="3";
                        if(((Element)child).select("font[color]").first()!=null)
                            mfont = ((Element)child).select("font[color]").attr("color");
                        if(((Element)child).select("font[size]").first()!=null)
                            msize = ((Element)child).select("font[size]").attr("size");
                        start = m_Contain.getText().length();
                        PaserPage3(eleTemp, m_Contain,pre);
                        end = m_Contain.getText().length();
                        m_Contain.font.add("", start, end, mfont, Integer.parseInt(msize));
                    }else if (((Element)child).tagName().equals("a")){
                        Element eleA = ((Element)child).select("a[href]").first();
                        if(eleA.select("img").first() == null){
                            String temp = ((Element)child).select("a[href]").attr("href");
                            temp=temp.replace("%3A",":");
                            temp=temp.replace("%2F","/");
                            temp=temp.replace("%3F","?");
                            temp=temp.replace("%3D", "=");
                            temp=temp.replace("%26", "&");
                            if(temp.contains("mailto:")){
                                temp = temp.substring(7, temp.length());
                                String tempText = ((Element)child).select("a[href]").text();
                                start = m_Contain.getText().length();
                                end = start + tempText.length();
                                m_Contain.web.add(temp, start, end);
                                m_Contain.addText(tempText);
                            }else if(temp.contains("http")){
                                temp = temp.substring(temp.indexOf("http"), temp.length());
                                String tempText = ((Element)child).select("a[href]").text();
                                start = m_Contain.getText().length();
                                end = start + tempText.length();
                                m_Contain.web.add(temp, start, end);
                                m_Contain.addText(tempText);
                            }
                        }else{
                            PaserPage3(eleA,m_Contain,true);
                        }

                    }else{
                        m_Contain.addText(((Element)child).text());
                    }
                }
                if (child instanceof TextNode) {
                    if(pre) m_Contain.addText(((TextNode) child).getWholeText());
                    else m_Contain.addText(((TextNode) child).text());
                }
            }
        }else{
            m_Contain.addText(ele.text());
        }
    }

}
