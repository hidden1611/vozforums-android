package com.nna88.voz.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PageRecentThread extends Page{
	private ArrayList<Thread> ListContains;
	private listViewCustom2 adapter;
	List<Comment> lComment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_RECENTTHREAD;
		super.onCreate(savedInstanceState);
		ListContains = new ArrayList<Thread>();
		adapter = new listViewCustom2(mContext,ListContains);
	    adapter.setSize(mTextSize);
	    mObjectAdapter = adapter;
	    mList.setAdapter(adapter);
        //_needfix
//	    mMenu.showContent();
	    mTask.execute(Global.TASK_RECENTTHREAD);
        //needfix
        //myVolley.myRencentThread(mUser);
//	    mMenuTitle.setText("My Recent Thread");
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                try{
                    Intent i = null;
                    vibrate();
                    if (ListContains.get(pos).isUrl() == 0) { // forum
                        i = new Intent(mContext, Page2.class);
                        if(ListContains.get(pos).UrlThread() == null)		return;
                        i.putExtra("URL", ListContains.get(pos).UrlThread());

                    } else if (ListContains.get(pos).isUrl() == 1){									//thread
                        i = new Intent(mContext, Page3.class);
                        if((ListContains.get(pos).UrlLastPosst() != null) &&
                                (!ListContains.get(pos).UrlLastPosst().equals(""))	){
                            if(ListContains.get(pos).UrlLastPosst() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
                        }else{
                            if(ListContains.get(pos).UrlThread() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlThread());
                        }
                    }else
                        return;

                    i.putExtra("TITLE", ListContains.get(pos).Thread());
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
            }
        });
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                vibrate();
                return false;
            }
        });
	    lComment = mDataBookmark.getAllComments();
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	void Parser(int taskType) throws Exception{
		// TODO Auto-generated method stub
        super.Parser(taskType);
        if(doc==null) return;
		Thread contain;
		ListContains.clear();
		Bitmap bitmap = null;
		String text1 = null,text2="",text3=null,text4=null,text1_url1=null,text2_url2=null,url_lastpost=null;
		Iterator<Element> ite = doc.select("tr:has(td[id*=td_threadstatusicon])").iterator();
		Element eleTag1;
        setPage(parsePage(Global.GO_INDEXPAGE,0));
		while(ite.hasNext()){
			ite.hasNext();
			eleTag1 = ite.next();
			text1 = "";text2 = "";
			if( eleTag1.select("a[id*=thread_title]").first()!= null){
				text1 =  eleTag1.select("a[id*=thread_title]").first().text();
                if(eleTag1.select("div:has(span[onclick*=member.php])").first() != null){
                    text1 += " " + eleTag1.select("div:has(span[onclick*=member.php])").first().text();
                }
				text1_url1 = eleTag1.select("a[id*=thread_title]").first().attr("href");
			}
			Elements temp = eleTag1.select("a[href*=forumdisplay]");
			if( temp.size() > 1){
				text2 = "Forums: " + temp.get(1).text()+"\n";
				text1 = temp.get(0).text()+" - "+text1;
			}else if(temp.size()==1){
				text2 = "Forums: " + temp.get(0).text()+"\n";
			}
			if( eleTag1.select("div[style*=text-align:right]").first()!= null){
				text2 += eleTag1.select("div[style*=text-align:right]").first().text() + "\n";
			}
			if( eleTag1.select("td[title*=Replies]").first()!= null){
				text2 += eleTag1.select("td[title*=Replies]").first().attr("title");
			}
            if(Global.bTopicHeader){
                //<img src="images/statusicon/thread_dot_hot.gif" id="
                if(eleTag1.select("img[src*=statusicon]").first() != null){
                    bitmap = loadBitmapAssert(eleTag1.select("img[src*=statusicon]").first().attr("src"));
                }
            }
			contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
			ListContains.add(contain);
		}
        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
		
		adapter.notifyDataSetChanged();
        mList.clearFocus();
        mList.requestFocusFromTouch();
        mList.post(new Runnable() {
            @Override
            public void run() {
                mList.setSelection(0);
                mList.clearFocus();
            }
        });
	}
		
	
}
