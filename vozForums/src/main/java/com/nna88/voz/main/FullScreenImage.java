// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package com.nna88.voz.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewSwitcher.ViewFactory;

import com.nna88.voz.PhotoView.PhotoViewAttacher;
import com.nna88.voz.util.UserInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public class FullScreenImage implements  ViewFactory, PopupWindow.OnDismissListener{
    public static boolean isInitShow;
    private static Bitmap bitmap;
    static boolean isLandscape;
    static ImageView iv;
    private static  Context mContext;
    private static boolean mIsShowing;
    private static String mLocalUri;
    private static PopupWindow mPopupWindow;
    private static ImageButton mShareButton;
    private static ImageButton mSaveButton;
    private static ImageButton mCancelButton;
    private static ProgressBar progressBar;
    static int mode;

    private static ImageLoader imageLoader = ImageLoader.getInstance();;
    private static  DisplayImageOptions options;
    public FullScreenImage(Context activity, View view) {
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .delayBeforeLoading(0)
                .build();
        mIsShowing = false;
        mode = 0;
        mContext = activity;
        isInitShow = false;
        initShow(view);
    }
    public void initShow(View view){
//        int width,height;

//        isLandscape = false;
//        Point size = new Point();
//        WindowManager w = ((WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            w.getDefaultDisplay().getSize(size);
//            width = size.x;
//            height = size.y;
//        } else {
//            Display d = w.getDefaultDisplay();
//            width = d.getWidth();
//            height = d.getHeight();
//        }
//        if(width > height)  isLandscape = true;

        View view1 = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fullscreen, null);
        mPopupWindow = new PopupWindow(view1,WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mIsShowing = false;
            }
        });
        iv = (ImageView)view1.findViewById(R.id.fullscreenimage);
        progressBar = (ProgressBar)view1.findViewById(R.id.progressbar);
        mSaveButton = (ImageButton)view1.findViewById(R.id.save);
        mCancelButton = (ImageButton)view1.findViewById(R.id.cancel);
        mShareButton = (ImageButton)view1.findViewById(R.id.share);
        mCancelButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hide();
            }
        });
        mShareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mLocalUri);
                sendIntent.setType("text/plain");
                mContext.startActivity(sendIntent);
            }
        });
        mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSaveImg();
            }
        });

    }
    public void show( String s){
        mLocalUri = s;
        mIsShowing = true;
        setImage();
    }

    public void hide()
    {
        try {
            mIsShowing = false;
            if (mPopupWindow != null) {
                mPopupWindow.dismiss();
                mPopupWindow = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private PhotoViewAttacher mAttacher;
    public void initialView(ImageView imageview)
    {
        mAttacher = new PhotoViewAttacher(imageview);
        mAttacher.canZoom();

    }


    public boolean isShowing()
    {
        return mIsShowing;
    }

    public View makeView()
    {
        return null;
    }




    private void alertSaveImg(){
        File myDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "vozForums");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String name = "Image-"+ n +".jpg";
        File file = new File (myDir,name);
        try {
            FileOutputStream out = new FileOutputStream(file);
            ((BitmapDrawable)iv.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
            toast("Save image in " + myDir.getPath());
        } catch (Exception e) {
            e.printStackTrace();
            toast("Error when saving the image");
        }
    }
//        final Dialog dialog = new Dialog(mContext);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.saveimage);
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        final EditText imgPath = (EditText) dialog.findViewById(R.id.imgPath);
//        final EditText imgName = (EditText) dialog.findViewById(R.id.imgName);
//        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
//        Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
//        imgName.setFocusable(true);
//        imgName.requestFocus();
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        imgPath.setText(Global.mSavePath);
//        final File myDir=new File(Global.mSavePath);
//        myDir.mkdirs();
//        Random generator = new Random();
//        int n = 10000;
//        n = generator.nextInt(n);
//        final String fname = "Image-"+ n +".jpg";
//        imgName.setText(fname);
//        buttonOK.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                File file = new File (myDir, imgName.getText().toString());
//                if (file.exists ()) file.delete ();
//                try {
//                    FileOutputStream out = new FileOutputStream(file);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//                    out.flush();
//                    out.close();
//                    mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
//                    toast("Save image in " + myDir.getPath());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                dialog.dismiss();
//            }
//        });
//        buttonCancle.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//}
    private void toast(String ss){
        Toast.makeText(mContext,ss,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDismiss() {
        mPopupWindow.dismiss();
    }

private class DownImageAttach extends AsyncTask<String, Void, String>{
    Bitmap bitmap = null;
    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        Map<String, String> cookies = new UserInfo().cookies();
        if(cookies==null) return null;
        try {
            String url = params[0];
            if(!url.contains("https://vozforums.com/")) url = "https://vozforums.com/" + url;
            url =  url.replace("thumb=1","thumb=0");
            log(url);
            Response resultImageResponse = Jsoup.connect(url).cookies(cookies).ignoreContentType(true).execute();
            byte[] data=resultImageResponse.bodyAsBytes();
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            return "OK";
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        //d = new BitmapDrawable(bitmap);
        try {
            iv.setImageBitmap(bitmap);
            initialView(iv);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
    private void log(String ss){
        Log.d("nna", ss);
    }

    public void setImage()
    {

        if(mLocalUri.contains("attachmentid")){
            new DownImageAttach().execute(mLocalUri);
        }else{
            progressBar.setVisibility(View.VISIBLE);
            imageLoader.displayImage(mLocalUri, iv, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap arg2) {
                    initialView(iv);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    progressBar.setVisibility(View.GONE);
                }
            }, new ImageLoadingProgressListener() {

           @Override
           public void onProgressUpdate(String s, View view, int i, int i2) {
                    progressBar.setProgress((i * 100) / i2);
                }
            });

        }

    }



    private View insertPhoto(Bitmap bitmap, final String url){
        //Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);
        LinearLayout layout = new LinearLayout(mContext);
        LinearLayout.LayoutParams pr = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(pr);
        layout.setGravity(Gravity.CENTER);
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageBitmap(bitmap);
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLocalUri = url;
                setImage();
            }
        });
        layout.addView(imageView);
        return layout;
    }
    /**
     * Set listener for window dismissed. This listener will only be fired if the quicakction dialog is dismissed
     * by clicking outside the dialog or clicking on sticky item.
     */



}
