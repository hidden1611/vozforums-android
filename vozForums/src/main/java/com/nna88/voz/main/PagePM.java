package com.nna88.voz.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PagePM extends Page{
	private ArrayList<Thread> ListContains;
	private listViewCustom2 adapter;
	List<Comment> lComment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_QUOTE;
		super.onCreate(savedInstanceState);
		ListContains = new ArrayList<Thread>();
		adapter = new listViewCustom2(mContext,ListContains);
	    adapter.setSize(mTextSize);
	    mObjectAdapter = adapter;
	    mList.setAdapter(adapter);
		mTask.execute(Global.TASK_PM);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if(ListContains.get(pos).UrlThread() != null){
                    Intent i = new Intent(mContext,PagePMDetail.class);
                    Bundle extras = new Bundle();
                    extras.putString("URL", ListContains.get(pos).UrlThread());
                    extras.putString("TITLE", "");
                    i.putExtras(extras);
                    startActivityForResult(i, 1);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }
            }
        });
	    lComment = mDataBookmark.getAllComments();
	    
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            TaskCancle();
            mTask = new TaskGetHtml();
            mTask.execute(Global.TASK_PM);
        }
    }

    @Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	void Parser(int taskType) throws Exception {
		// TODO Auto-generated method stub
		super.Parser(taskType);
		if(doc==null) return;
		Thread contain;
		ListContains.clear();
		Bitmap bitmap = null;
		String text1 = null,text2="",text3=null,text4=null,text1_url1=null,text2_url2=null,url_lastpost=null;
		Iterator<Element> ite = doc.select("td[id*=m]").iterator();
		Element eleTag1;
		while(ite.hasNext()){
			ite.hasNext();
			eleTag1 = ite.next();
			text1 = "";text2 = "";
			if( eleTag1.select("a[href*=private.php]").first()!= null){
				text1 =  eleTag1.select("a[href*=private.php]").first().text();
				text1_url1 = eleTag1.select("a[href*=private.php]").first().attr("href");
			}
			if(eleTag1.select("span[onclick*=member]").first() != null){
				text2 = eleTag1.select("span[onclick*=member]").first().text();
			}
			if(eleTag1.select("span[class*=smallfont]").first() != null){
				text2 += "\n"+eleTag1.select("span[class*=smallfont]").first().text();
			}
			if(eleTag1.select("span[class*=time]").first() != null){
				text2 += "  "+eleTag1.select("span[class*=time]").first().text();
			}
				
			
			contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
			ListContains.add(contain);
		}
        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
		adapter.notifyDataSetChanged();
	}
		
	
}
