package com.nna88.voz.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.*;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.nna88.voz.ads.Ad;
import com.nna88.voz.colorpicker.ColorPicker;
import com.nna88.voz.colorpicker.OpacityBar;
import com.nna88.voz.colorpicker.SVBar;
import com.nna88.voz.listview.SettingAdapter;
import com.nna88.voz.listview.WebkitCookieManagerProxy;
import com.nna88.voz.listview.listViewCustom1;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.listview.listViewCustom3;
import com.nna88.voz.listview.listViewCustom3Html;
import com.nna88.voz.mysqlite.CommentsDataSource;
import com.nna88.voz.parserhtml.parser;
import com.nna88.voz.ui.ExpandableHeightGridView;
import com.nna88.voz.ui.NavigationDrawerActivity;
import com.nna88.voz.ui.SidebarAdapter;
import com.nna88.voz.ui.UtilLayout;
import com.nna88.voz.util.ChangeLog;
import com.nna88.voz.util.Help;
import com.nna88.voz.util.ImageLoad;
import com.nna88.voz.util.UserInfo;
import com.nna88.voz.util.Util;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import org.apache.commons.io.FileUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Page extends NavigationDrawerActivity {
    protected  Context mContext;
    protected ListView mList;
    protected Document doc;
    protected UserInfo mUser;
    protected float mTextSize;
    protected TaskGetHtml mTask;
    protected parser mParser;
    protected String sPost;
    protected String sIdThread;
    protected SharedPreferences settings;
    protected TextView butPageHeader;
    protected TextView butPageFooter;
    protected ImageView mImg1Header;
    protected ImageView mImg2Header;
    protected ImageView mImg3Header;
    protected ImageView mImg4Header;
    protected ImageView mImg1Footer;
    protected ImageView mImg2Footer;
    protected ImageView mImg3Footer;
    protected ImageView mImg4Footer;
    protected String mTitle;
    protected Vibrator myVib;
    protected SidebarAdapter mAdapterSideMenu1;
    protected SidebarAdapter mAdapterSideMenu2;
    protected ArrayList<Object> mListSideMenu1;
    protected ArrayList<Object> mListSideMenu2;
    protected ImageLoad mImageLoad;
    protected int iPage=0;
    protected CommentsDataSource mDataBookmark;
    protected Object mObjectAdapter;
    protected boolean isSubscribe = false;
    protected LinearLayout linearHeader;
    protected LinearLayout linearFooter;
    protected ArrayList<String> lsForum;
    protected ArrayList<String> lIdForum;
    protected String sMessSearch;
    protected String sForum;
    protected String sSearch_ShowPost = "0";
    //quick return for listview
    protected int mItemCount;
    protected int mItemOffsetY[];
    protected int mItemtemp[];
    protected boolean scrollIsComputed = false;
    protected int mQuickReturnHeight;
    protected LinearLayout mQuickReturnLayout;
    protected static final int STATE_ONSCREEN = 0;
    protected static final int STATE_OFFSCREEN = 1;
    protected static final int STATE_RETURNING = 2;
    protected int mState = STATE_ONSCREEN;
    protected int mScrollY;
    protected int mMinRawY = 0;
    protected TranslateAnimation anim;

    protected int iPositiion=0;
    protected ArrayList<String> mArrayUsered = new ArrayList<String>();
    protected ArrayList<String> mArrayPass = new ArrayList<String> () ;
    protected ArrayList<String> mArrayUseredId = new ArrayList<String> () ;

    private String mUsername;
    private String mPassword;

    protected Document doc2;
    protected String specifiedpost="0";
    protected String mRadioUser ;
    protected RelativeLayout mLayoutAds;

    protected int mColorText2;
    private Ad mAd;
    private LinearLayout mLayoutProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        mColorText2 = Global.getTextClolor2();
        settings = getSharedPreferences("Setting", MODE_PRIVATE);
        mUser = new UserInfo();
        Global.iTheme = settings.getInt("THEME", Global.THEME_BLACK);
        if(Global.iTheme == Global.THEME_CUSTOM)
            readCustomTheme();
        getRevolution();
        initUI();
        mLayoutAds = (RelativeLayout)findViewById(R.id.myAd);
        mLayoutAds.setVisibility(View.VISIBLE);
        readSettings();
        mParser = new parser(Global.URL);
        mTask = new TaskGetHtml();
        mAd =  new Ad(Page.this,mLayoutAds);
        android.webkit.CookieSyncManager.createInstance(this);
        android.webkit.CookieManager.getInstance().setAcceptCookie(true);
        // magic starts here
        WebkitCookieManagerProxy coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
        java.net.CookieHandler.setDefault(coreCookieManager);
        mLayoutProgress = (LinearLayout)findViewById(R.id.layoutprogress);
        Global.iDensity =  getResources().getDisplayMetrics().density;

    }
    protected void setProgress(boolean value){
        if(value){
            mLayoutProgress.setVisibility(View.VISIBLE);
        }else{
            mLayoutProgress.setVisibility(View.GONE);
        }
    }
    private void readCustomTheme(){
        Global.mCusThemeBg = settings.getInt("THEMEBG", 0xFFFFFFFF);//0xFFFFFFFF
        Global.mCusThemeBgFocus = settings.getInt("THEMEBGFOCUS", 0xFF33B5E5);
        Global.mCusThemeTitleBg = settings.getInt("THEMETITLEBG", 0xFFB3C833);//0xFFB3C833
        Global.mCusThemeTxtTitle = settings.getInt("THEMETXTTITLE", 0xFFFFFFFF);
        Global.mCusThemeTxt1 = settings.getInt("THEMETXT1", 0xFF000000);
        Global.mCusThemeTxt2 = settings.getInt("THEMETXT2", 0xFF555555);
        Global.mCusThemeQuicklink = settings.getInt("THEMEQUICKLINK", 0xFFB3C833);
        Global.themeColor[Global.THEME_CUSTOM][Global.bg] = Global.mCusThemeBg;
        Global.themeColor[Global.THEME_CUSTOM][Global.bgtitle] = Global.mCusThemeTitleBg;
        Global.themeColor[Global.THEME_CUSTOM][Global.txtitle] = Global.mCusThemeTxtTitle;
        Global.themeColor[Global.THEME_CUSTOM][Global.txColor1] = Global.mCusThemeTxt1;
        Global.themeColor[Global.THEME_CUSTOM][Global.txColor2] = Global.mCusThemeTxt2;
        Global.themeColor[Global.THEME_CUSTOM][Global.quicklink] = Global.mCusThemeQuicklink;
    }
    protected void loadAds(){
        mAd.loadAds();
    }
    protected void hideAds(){
        mAd.hideAds();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        UpdateUI();

        super.onResume();
    }
    protected void vibrate(){
        if(Global.bVibrate){
            if(myVib == null)
                myVib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            myVib.vibrate(50);

        }
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        // mTask.cancel(true);
        TaskCancle();
        mDataBookmark.close();
        mContext = null;
        mObjectAdapter = null;

        super.onDestroy();
    }
    @SuppressWarnings("deprecation")
    void getRevolution() {

        Point size = new Point();
        WindowManager w = getWindowManager();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Global.width = size.x;
            Global.height = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            Global.width = d.getWidth();
            Global.height = d.getHeight();
        }
    }

    public void writeQuickreturn (int value,String data) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("QUICKLINK"+value, data);
        editor.commit();
    }
    public void readStringForum(){
        String stringForums = settings.getString("FORUMS", "");
        int length;
        String[] lForums = stringForums.split(";");
        length = lForums.length;
        if(length >0){
            lsForum = new ArrayList<String>();
            lIdForum = new ArrayList<String>();
            String forums;
            for(int i=0;i<length;i++){
                forums = lForums[i];
                if(forums.contains(",")){
                    lsForum.add(forums.split(",")[0]);
                    String temp =forums.split(",")[1];
                    if(temp.contains("f=")){
                        lIdForum.add(forums.split(",")[1].split("f=")[1]);
                    }else{
                        lIdForum.add(null);
                    }
                }
            }
        }
    }
    public void readSettings() {
        mTextSize = settings.getFloat("FontSize", 1.0F);
        Global.iSize = mTextSize;
        Global.bSwipe = settings.getBoolean("SWIPE",true);
        Global.bTopicHeader = settings.getBoolean("TOPICHEADER", false);
        Global.bIconRefresh = settings.getBoolean("ICONREFRESH", false);
        Global.bClickAd = settings.getBoolean("CLICKAD", false);
        Global.bEffect = settings.getBoolean("EFFECT", false);
        Global.bSign = settings.getBoolean("SIGN", false);
        Global.bDevider = settings.getBoolean("DEVIDER", true);
        Global.iHome = settings.getInt("SETHOMEVOZ", 0);
        Global.mSavePath = settings.getString("PATHSAVE","/mnt/sdcard/vozforums");
        Global.bNotifSubscribe = settings.getBoolean("NOTIFICATIONSUBSCRIBE", true);
        Global.bNotifQuote = settings.getBoolean("NOTIFICATIONQUOTE", true);
        Global.iNotifminutes = settings.getInt("NOTIFICATIONTIMER", 30);
        Global.iNumQuote = settings.getInt("NUMQUOTE", 0);
        Global.bVibrate = settings.getBoolean("VIBRATE", true);
        Global.bFullScreen = settings.getBoolean("FULLSCREEN", false);
        Global.iSizeImage = settings.getInt("SIZEIMAGE", Global.REDUCE8);
        Global.sYourDevice = settings.getString("YOURDEVICE", Build.MODEL);
        toggleFullScreen(Global.bFullScreen);

        Map<String, String> map = new HashMap<String, String>();
        if (!settings.getString("vfuserid", "").equals("") && !settings.getString("vfuserid", "").equals("deleted")) {
            map.put("vflastvisit", settings.getString("vflastvisit", ""));
            map.put("vflastactivity", settings.getString("vflastactivity", ""));
            map.put("vfuserid", settings.getString("vfuserid", ""));
            map.put("vfpassword", settings.getString("vfpassword", ""));
            map.put("vfimloggedin", settings.getString("vfimloggedin", ""));
            map.put("vfsessionhash", settings.getString("vfsessionhash", ""));
            mUser.cookies(map);
            mUser.setCookieStore(map);
            mUser.setUserId(settings.getString("vfuserid", ""));
            String mUsered = settings.getString("usered",null);
            if(mUsered != null){
                String lUsered[] = mUsered.split(";");
                for(String tmp:lUsered){
                    mArrayUsered.add(settings.getString("username-" + tmp, ""));
                    mArrayPass.add(settings.getString("password-" + tmp, ""));
                    mArrayUseredId.add(settings.getString("userid-" + tmp, ""));
                }
            }
            mUser.SetUser(settings.getString("username-" + mUser.UserId(), getResources().getString(R.string.Login)));
            ((SidebarAdapter.Item)mListSideMenu2.get(0)).mTitle = mUser.User();
            mAdapterSideMenu2.notifyDataSetInvalidated();
        }
    }

    public void writeSetTheme(int iTheme) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("THEME", iTheme);
        editor.commit();
    }
    public void writeSetCusTheme() {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("THEMEBG", Global.mCusThemeBg);
        editor.putInt("THEMEBGFOCUS", Global.mCusThemeBgFocus);
        editor.putInt("THEMETITLEBG", Global.mCusThemeTitleBg);
        editor.putInt("THEMETXTTITLE", Global.mCusThemeTxtTitle);
        editor.putInt("THEMETXT1", Global.mCusThemeTxt1);
        editor.putInt("THEMETXT2", Global.mCusThemeTxt2);
        editor.putInt("THEMEQUICKLINK", Global.mCusThemeQuicklink);
        editor.commit();
    }
    public void writeSetPathSave(String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("PATHSAVE", value);
        editor.commit();
    }
    public void writeSetHome(int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("SETHOMEVOZ", value);
        editor.commit();
    }
    public void writeSetNumQuickLink(String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("NUMQUICKLINK", value);
        editor.commit();
    }
    protected void writeSetNumQuote(int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("NUMQUOTE", value);
        editor.commit();
    }
    public void writeSetImageSize(int imageSize) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("SIZEIMAGE", imageSize);
        editor.commit();
    }
    public void writeSetFont(float fontsize) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat("FontSize", fontsize);
        editor.commit();
    }

    public void writeStringForums (String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("FORUMS", value);
        editor.commit();
    }
    public void writeSetYourDevices (String yourdevices) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("YOURDEVICE", yourdevices);
        editor.commit();
    }
    public void writeSetingUser(Map<String, String> map) {

        SharedPreferences.Editor editor = settings.edit();
        String userid = null;
        if(map!=null){
            editor.putString("vflastvisit", map.get("vflastvisit"));
            editor.putString("vflastactivity", map.get("vflastactivity"));
            editor.putString("vfuserid", map.get("vfuserid"));
            editor.putString("vfpassword", map.get("vfpassword"));
            editor.putString("vfimloggedin", map.get("vfimloggedin"));
            editor.putString("vfsessionhash", map.get("vfsessionhash"));
            mUser.setCookieStore(map);
            userid = map.get("vfuserid");
            if(mUsername!= null && mPassword != null ){
                editor.putString("username-" + userid,mUsername);
                editor.putString("password-" + userid,mPassword);
                editor.putString("userid-" + userid,userid);
                mUsername = null;
                mPassword = null;
            }
            String mUsered = settings.getString("usered",null);
            if(mUsered != null){
                if(!mUsered.contains(userid))
                    editor.putString("usered",mUsered+";"+userid);
            }else{
                editor.putString("usered",userid);
            }
        }else{
            editor.putString("vflastvisit","");
            editor.putString("vflastactivity", "");
            editor.putString("vfuserid", "");
            editor.putString("vfpassword", "");
            editor.putString("vfimloggedin", "");
            editor.putString("vfsessionhash", "");
            mUser.setCookieStore(null);
        }

        editor.commit();
    }
    protected String getlinkBitmapAssert(final String url){
        String mUrl;
        if(!url.contains("smilies"))
            mUrl = url;
        else{
            mUrl = url.substring(0, url.length()-3)+"gif";
            try {
                String list[] = mContext.getAssets().list("images/smilies/Off");
                for(String ss:list){
                    if((url.substring(0, url.length()-3)+"png").contains(ss)){
                        mUrl = url.substring(0, url.length()-3) + "png";
                        break;
                    }
                }
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return "assets://"+mUrl;
    }
    Bitmap loadBitmapAssert(final String url) {
        InputStream is;
        Bitmap emo;
        String mUrl;
        if(!url.contains("smilies"))
            mUrl = url;
        else{
            mUrl = url.substring(0, url.length()-3)+"gif";
            try {
                String list[] = mContext.getAssets().list("images/smilies/Off");
                for(String ss:list){
                    if((url.substring(0, url.length()-3)+"png").contains(ss)){
                        mUrl = url.substring(0, url.length()-3) + "png";
                        break;
                    }
                }
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //toast(mUrl);
        }

        try {
            is = mContext.getAssets().open(mUrl);
            emo = BitmapFactory.decodeStream(is);
            if(mUrl.contains("statusicon")){
                int dp_32 = Util.convertDpToPx(mContext.getApplicationContext(),24);
                is.close();
                return Bitmap.createScaledBitmap(emo,dp_32,dp_32,false);
            }else{
                is.close();
                return emo;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    class TaskGetHtml extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected Integer doInBackground(Integer... params) {
            // TODO Auto-generated method stub
            switch (params[0]) {
                case Global.TASK_PM:
                    doc = mParser.getPM(mUser);
                    break;
                case Global.TASK_SEARCH:
                    doc = mParser.Search(mUser,mUser.Token(), sMessSearch, sSearch_ShowPost);
                    break;
                case Global.TASK_ADVANCESEARCH:
                    doc = mParser.AdvanceSearch(mUser, mUser.Token(), sMessSearch,sForum , sSearch_ShowPost);
                    break;

                case Global.TASK_RECENTTHREAD:
                    doc = mParser.myRecentThread(mUser);
                    break;
                case Global.TASK_RECENTPOST:
                    doc = mParser.myRecentPost(mUser);
                    break;
                case Global.TASK_SEARCHQUOTE:
                    String sectoken;
                    if(doc != null && doc.select("input[name=securitytoken]").first() != null){
                        sectoken = doc.select("input[name=securitytoken]").first().attr("value");
                        mUser.setToken(sectoken);
                    }
                    doc = mParser.SearchQuote(mUser);
                    break;
                case Global.TASK_SUBSCRIBE:
                    Element eleSecurity1 = doc.select("input[name*=securitytoken]")
                            .first();
                    String secToken1 = null;
                    if (eleSecurity1 != null)
                        secToken1 = eleSecurity1.attr("value");
                    doc = mParser.Subscribe(mUser, sIdThread, secToken1);
                    break;
                case Global.TASK_UNSUBSCRIBE:
                    doc = mParser.UnSubscribe(mUser, sIdThread);
                    break;

                case Global.TASK_GETDOC:
                    if(Global.OFFILNE){
                        try {
                            String content = FileUtils.readFileToString(new File("/mnt/sdcard/paga3.html"), "UTF-8");
                            doc = Jsoup.parse(content);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        doc = mParser.getDoc(mUser);
                    }
                    break;
                case Global.TASK_LOGIN:
                    doc = mParser.login(mUser);
                    break;
                case Global.TASK_LOGOUT:
                    doc = mParser.Logout(mUser);
                    break;
                case Global.TASK_GETQUOTE:
                    doc = mParser.quoteGet(mUser,params[1], params[2]);
                    break;

                case Global.TASK_POSTREPLY:
                    try {
                        doc = mParser.PostReplyQuote(doc2,mUser,sPost,specifiedpost);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    break;
                case Global.TASK_POSTEDIT:
                    try {

                        Element mainElement = doc2.select("div[align=center]").get(1);
                        Element eleSecurity = mainElement.select("input[name*=securitytoken]").first();
                        Element elePostHash = mainElement.select("input[name*=posthash]").first();
                        Element elePostStartTime = mainElement.select("input[name*=poststarttime]").first();
                        Element eleP = mainElement.select("input[name=p]").first();
                        Element eleTitle = mainElement.select("input[name=title]").first();
                        String url = null, secToken = null, postHash = null, postStartTime = null, p = null, title = null;
                        if (eleSecurity != null)
                            secToken = eleSecurity.attr("value");
                        if (elePostHash != null)
                            postHash = elePostHash.attr("value");
                        if (elePostStartTime != null)
                            postStartTime = elePostStartTime.attr("value");
                        if (eleP != null)
                            p = eleP.attr("value");
                        if (eleTitle != null)
                            title = eleTitle.attr("value");
                        url = "editpost.php?do=updatepost&p=" + p;
                        doc = mParser.PostEditReply(mUser, 0, sPost, url, title, secToken, p, postHash, postStartTime);
                    } catch (Exception e) {
                        // TODO: handle exception
                        // toast("Error: cannot Edit");
                    }
                    break;
                case Global.TASK_POSTDELETE:
                    try {
                        Element mainElement = doc2.select("div[align=center]").get(1);
                        Element eleSecurity = mainElement.select("input[name*=securitytoken]").first();
                        Element eleP = mainElement.select("input[name=p]").first();
                        Element eleTitle = mainElement.select("input[name=title]").first();
                        String url = null, secToken = null, p = null, title = null;
                        if (eleSecurity != null)
                            secToken = eleSecurity.attr("value");
                        if (eleP != null)
                            p = eleP.attr("value");
                        if (eleTitle != null)
                            title = eleTitle.attr("value");
                        url = "editpost.php?do=deletepost&p=" + p;
                        doc = mParser.PostEditReply(mUser, 1, sPost, url, title, secToken, p, null, null);
                    } catch (Exception e) {
                        // TODO: handle exception
                        // toast("Error: cannot Edit");
                    }
                    break;
                default:
                    break;
            }
            //Parser(params[0]);
            return params[0];
        }
        @Override
        protected void onPostExecute(Integer result) {
            // TODO Auto-generated method stub
            try {
                setProgress(false);
                Parser(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            switch (result) {
                case Global.TASK_LOGIN:
                    if(doc == null){
                        toast("Tên đăng nhập hoặc mật khẩu sai!");
                        alertLogin();
                    }else{
                        writeSetingUser(mUser.cookies());
                        mUser.setLogin(true);
                        ((SidebarAdapter.Item)mListSideMenu2.get(0)).mTitle= mUser.User();
                        mUser.setAvatart(null);
                        mAdapterSideMenu2.notifyDataSetInvalidated();
                    }
                    break;

                case Global.TASK_LOGOUT:
                    mUser.mCookieStore = null;
                    mUser.cookies(null);
                    mUser.setLogin(false);
                    mUser.setAvatart(null);
                    mUser.SetUser(null);
                    ((SidebarAdapter.Item)mListSideMenu2.get(0)).mTitle= getResources().getString(R.string.Login);
                    mAdapterSideMenu2.notifyDataSetInvalidated();
                    writeSetingUser(mUser.cookies());
                    break;
                default:
                    break;
            }
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            setProgress(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        // Handle action buttons
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int i = item.getItemId();
        if(i == R.id.action_refresh){
            TaskCancle();
            mTask = new TaskGetHtml();
            mTask.execute(Global.TASK_GETDOC);
            return true;
        }else if (i == R.id.action_refresh2) {
//            Picasso.with(mContext).shutdown();
            TaskCancle();
            mTask = new TaskGetHtml();
            mTask.execute(Global.TASK_GETDOC);
            return true;
        }else if (i == R.id.action_help){
            Help clHelp = new Help(mContext);
            clHelp.getLogDialog().show();
            return true;
        }else if (i == R.id.action_exit) {
            Intent intent = new Intent(mContext, Page1.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Exit me", true);
            startActivity(intent);
            finish();
            return true;
        }else if (i == R.id.action_websearch) {
            alertSettings();
            return true;
        }else if (i == R.id.action_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            String url = mParser.getUrl();
            if(!url.contains("vozforums.com"))
                url = "https://vozforums.com/"+url;
            intent.putExtra(android.content.Intent.EXTRA_TEXT,url);
            intent.putExtra(android.content.Intent.EXTRA_STREAM,R.drawable.back_click);
            startActivity(Intent.createChooser(intent, "Your Chooser"));
            return true;
        }else if(i == R.id.action_back){
            onBackPressed();
            return true;
        } else{
            return super.onOptionsItemSelected(item);
        }
    }

    private class getAvatar extends AsyncTask<Void, Void, String>{
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(result ==  null) return;
            mImageLoad.imageLoader.loadImage( result, mImageLoad.options, new ImageLoadingListener() {
                @Override
                public void onLoadingCancelled(String arg0, View arg1) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onLoadingComplete(String arg0, View arg1,
                                              Bitmap arg2) {
                    // TODO Auto-generated method stub
                    mUser.setAvatart(arg2);
                    ((SidebarAdapter.Item)mListSideMenu2.get(0)).mIcon = new BitmapDrawable(getResources(),arg2);
                    mAdapterSideMenu2.notifyDataSetChanged();
                }

                @Override
                public void onLoadingFailed(String arg0, View arg1,
                                            FailReason arg2) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onLoadingStarted(String arg0, View arg1) {
                    // TODO Auto-generated method stub
                }
            });
        }
        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            String url=null;
            if(mUser.cookies() == null) return null;
            try {
                Document doc = Jsoup.connect("https://vozforums.com/profile.php?do=editavatar")
                        .cookies(mUser.cookies())
                        .get();
                Element ele = doc.select("img[alt=Custom Avatar]").first();
                if(ele != null){
                    url = Global.URL  + ele.attr("src");
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return url;
        }

    }
    void parserNotification(){
        if(doc==null) return;
        Element ele = doc.select("tr:has(td[class=tcat])").first();
        if(ele==null) return;
        ele = ele.nextElementSibling();
        if(ele!=null){
            try {
                alert(ele.text());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    void Parser(int taskType) throws Exception{
        if (doc == null && mParser.sNotif!=null) {
            alert(mParser.sNotif);
            return;
        }
    }

    void log(String ss) {
        Log.d("nna", ss);
    }

    void toast(String ss) {
        Toast.makeText(mContext, ss, Toast.LENGTH_LONG).show();
    }


    protected void TaskCancle() {
        if(mTask != null)
            mTask.cancel(true);
    }

    String parsePage(int type, int numPage) throws Exception{
        if(doc==null)	return null;
        Element elePage = doc.select("div[class=pagenav").first();
        String mIndexPage, temp = null, m_url, idthread = null;
        int curPage, lastPage, size;
        Element eleGetId;
        if (elePage != null) {
            mIndexPage = elePage.select("td[class=vbmenu_control]").text();
            curPage = Integer.parseInt(mIndexPage.split(" ")[1]);
            lastPage = Integer.parseInt(mIndexPage.split(" ")[3]);
            if (mParser.getUrl().contains("showthread.php?p=")) {
                size = elePage.getElementsByAttribute("href").size();
                for (int i = 0; i < size; i++) {
                    eleGetId = elePage.getElementsByAttribute("href").get(i);
                    if (eleGetId != null) {
                        String temp2 = eleGetId.attr("href");
                        int startId, endId;
                        if (temp2.contains("&page=") != true) {
                            temp2 = temp2.concat("&page=1");
                        }
                        startId = temp2.indexOf("?t=");
                        endId = temp2.indexOf("&page");
                        idthread = temp2.substring(startId + 3, endId);
                        break;
                    }
                }
                m_url = Global.URL + "showthread.php?t=" + idthread + "&page=" + String.valueOf(curPage);
            } else {
                m_url = mParser.getUrl();
                if (m_url.contains("&page=") != true) {
                    m_url = m_url.concat("&page=1");
                }
            }
            switch (type) {
                case Global.GO_INDEXPAGE:
                    temp = mIndexPage;
                    String temp2[] = temp.split(" ");
                    temp = temp2[1] + "/" + temp2[3];
                    break;
                case Global.GO_FIRST:
                    temp = m_url.split("&page")[0];
                    break;
                case Global.GO_NEXT:
                    temp = m_url.substring(0, m_url.lastIndexOf("=") + 1).concat(String.valueOf(curPage + 1));
                    break;
                case Global.GO_PREVIOUS:
                    temp = m_url.substring(0, m_url.lastIndexOf("=") + 1).concat(String.valueOf(curPage - 1));
                    if (temp.contains("&page=0")) {
                        temp = temp.split("&page")[0];
                    }
                    break;
                case Global.GO_LAST:
                    temp = m_url.split("&page")[0];
                    temp = temp.concat("&page=" + String.valueOf(lastPage));
                    break;
                case Global.GO_REFRESH:
                    temp = m_url.split("&page")[0];
                    temp = temp.concat("&page=" + String.valueOf(numPage));
                    break;
            }
            return temp;
        } else
            return null;

    }


    protected void GoPage(int state, int numPage) throws Exception {
        String temp = parsePage(state, numPage);
        if (temp != null) {
            mParser.setUrl(parsePage(state, numPage));
            TaskCancle();
            mTask = new TaskGetHtml();
            mTask.execute(Global.TASK_GETDOC);
        }
    }

    private View getHeaderView(int mGravity) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearHeader = (LinearLayout) inflater.inflate(R.layout.threadfoot, null);
        Global.setBackgroundItemThread(linearHeader);
        mImg1Header = (ImageView) linearHeader.findViewById(R.id.fast_prev);
        mImg2Header = (ImageView) linearHeader.findViewById(R.id.prev);
        butPageHeader = (TextView) linearHeader.findViewById(R.id.page_list);
        mImg3Header = (ImageView) linearHeader.findViewById(R.id.next);
        mImg4Header = (ImageView) linearHeader.findViewById(R.id.fast_next);

        mImg1Header.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_FIRST, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mImg2Header.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_PREVIOUS, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        butPageHeader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertGoPage();
            }
        });

        mImg3Header.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_NEXT, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mImg4Header.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_LAST, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        linearHeader.setGravity(mGravity);
        return linearHeader;

    }
    public  StateListDrawable getColorBackground(int colorNormal, int colorFocus){
        StateListDrawable states = new StateListDrawable();
        ColorDrawable clrBase = new ColorDrawable(colorNormal);
        ColorDrawable clrSelect = new ColorDrawable(colorFocus);
        states.addState(new int[] {-android.R.attr.state_enabled}, clrBase);
        states.addState(new int[] {android.R.attr.state_active}, clrBase);
        states.addState(new int[] {android.R.attr.state_selected}, clrSelect);
        states.addState(new int[]{android.R.attr.state_pressed}, clrSelect);
        return states;
    }
    private void UpdateUI(){
        mList.requestLayout();
        if(Global.bDevider){
            UtilLayout.setMargin(mContext, mList, 6, 0, 6, 0);
            mList.setDividerHeight(Util.convertDpToPx(mContext,6));
        }else{
            UtilLayout.setMargin(mContext, mList, 1, 0, 1, 0);
            mList.setDividerHeight(Util.convertDpToPx(mContext,1));
        }
        Global.setBackgoundMain(getmDrawerLayout());
        Global.setBackgroundItemThread(mLayoutAds);
        Global.setBackgroundItemThread(mQuickReturnLayout);
        Global.setBackgoundMain(mList);
        getSupportActionBar().setBackgroundDrawable(getColorBackground(Global.themeColor[Global.iTheme][Global.bgtitle], Global.themeColor[Global.iTheme][Global.bgtitle]));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Global.themeColor[Global.iTheme][Global.bgstatus]);
        }
//        textTitle.setTextColor(Global.themeColor[Global.iTheme][Global.txtitle]);
        invalidateOptionsMenu();
        try{
            if(iPage==Global.PAGE_1){
                ((listViewCustom1)mObjectAdapter).notifyDataSetChanged();
            }else if(iPage==Global.PAGE_2){
                Global.setBackgroundItemThread(linearFooter);
                ((listViewCustom2)mObjectAdapter).notifyDataSetChanged();
            }else if(iPage==Global.PAGE_3){
                Global.setBackgroundItemThread(linearHeader);
                Global.setBackgroundItemThread(linearFooter);
                ((listViewCustom3)mObjectAdapter).notifyDataSetChanged();
            }else if(iPage==Global.PAGE_3HTML){
                Global.setBackgroundItemThread(linearHeader);
                Global.setBackgroundItemThread(linearFooter);
                ((listViewCustom3)mObjectAdapter).notifyDataSetChanged();
            }else if(iPage == Global.PAGE_PMDETAIL){
                ((listViewCustom3)mObjectAdapter).notifyDataSetChanged();
            }else {
                ((listViewCustom2)mObjectAdapter).notifyDataSetChanged();
            }
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }
    private View getFooterView(int mGravity) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearFooter = (LinearLayout) inflater.inflate(R.layout.threadfoot, null);
        Global.setBackgroundItemThread(linearFooter);
        mImg1Footer = (ImageView) linearFooter.findViewById(R.id.fast_prev);
        mImg2Footer = (ImageView) linearFooter.findViewById(R.id.prev);
        butPageFooter = (TextView) linearFooter.findViewById(R.id.page_list);
        mImg3Footer = (ImageView) linearFooter.findViewById(R.id.next);
        mImg4Footer = (ImageView) linearFooter.findViewById(R.id.fast_next);
        mImg1Footer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_FIRST, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mImg2Footer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_PREVIOUS, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        butPageFooter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertGoPage();
            }
        });
        mImg3Footer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_NEXT, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mImg4Footer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    GoPage(Global.GO_LAST, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // linearlayout.setBackgroundResource(ThemeUtil.getSubBackgroundColor(mContext));
        linearFooter.setGravity(mGravity);
        return linearFooter;
    }

    private void setEnableBackHeader(boolean value){
        if(value){
            mImg1Header.setEnabled(true);
            mImg2Header.setEnabled(true);
            mImg1Header.setImageResource(R.drawable.tapatalk_fast_back_common_dark);
            mImg2Header.setImageResource(R.drawable.tapatalk_back_common_dark);
        }else{
            mImg1Header.setEnabled(false);
            mImg2Header.setEnabled(false);
            mImg1Header.setImageResource(R.drawable.tapatalk_fast_back_disable_dark);
            mImg2Header.setImageResource(R.drawable.tapatalk_back_disable_dark);
        }
    }
    private void setEnableNextHeader(boolean value){
        if(value){
            mImg3Header.setEnabled(true);
            mImg4Header.setEnabled(true);
            mImg4Header.setImageResource(R.drawable.tapatalk_fast_forward_common_dark);
            mImg3Header.setImageResource(R.drawable.tapatalk_forward_common_dark);
        }else{
            mImg3Header.setEnabled(false);
            mImg4Header.setEnabled(false);
            mImg4Header.setImageResource(R.drawable.tapatalk_fast_forward_disable_dark);
            mImg3Header.setImageResource(R.drawable.tapatalk_forward_disable_dark);
        }
    }
    private void setEnableBackFooter(boolean value){
        if(value){
            mImg1Footer.setEnabled(true);
            mImg2Footer.setEnabled(true);
            mImg1Footer.setImageResource(R.drawable.tapatalk_fast_back_common_dark);
            mImg2Footer.setImageResource(R.drawable.tapatalk_back_common_dark);
        }else{
            mImg1Footer.setEnabled(false);
            mImg2Footer.setEnabled(false);
            mImg1Footer.setImageResource(R.drawable.tapatalk_fast_back_disable_dark);
            mImg2Footer.setImageResource(R.drawable.tapatalk_back_disable_dark);
        }
    }
    private void setEnableNextFooter(boolean value){
        if(value){
            mImg3Footer.setEnabled(true);
            mImg4Footer.setEnabled(true);
            mImg4Footer.setImageResource(R.drawable.tapatalk_fast_forward_common_dark);
            mImg3Footer.setImageResource(R.drawable.tapatalk_forward_common_dark);
        }else{
            mImg3Footer.setEnabled(false);
            mImg4Footer.setEnabled(false);
            mImg4Footer.setImageResource(R.drawable.tapatalk_fast_forward_disable_dark);
            mImg3Footer.setImageResource(R.drawable.tapatalk_forward_disable_dark);
        }
    }

    protected void setPage(String page) {
        setEnableBackFooter(true);
        setEnableNextFooter(true);
        if(iPage==Global.PAGE_3){
            setEnableBackHeader(true);
            setEnableNextHeader(true);
        }

        if (page == null) {
            butPageFooter.setText("Page 1/1");
            setEnableBackFooter(false);
            setEnableNextFooter(false);
            if(iPage==Global.PAGE_3){
                butPageHeader.setText("Page 1/1");
                setEnableBackHeader(false);
                setEnableNextHeader(false);
            }
        } else {
            if(page.contains("/")){
                if(page.split("/")[0].equals("1")){
                    setEnableBackFooter(false);
                }
                if(page.split("/")[0].equals(page.split("/")[1])) {
                    setEnableNextFooter(false);
                }
            }
            butPageFooter.setText(" Page " + page + " ");

            if(iPage==Global.PAGE_3){
                if(page.contains("/")){
                    if(page.split("/")[0].equals("1")){
                        setEnableBackHeader(false);
                    }else{
                        setEnableBackHeader(true);
                    }
                    if(page.split("/")[0].equals(page.split("/")[1])){
                        setEnableNextHeader(false);
                    }else{
                        setEnableNextHeader(true);
                    }
                }
                butPageHeader.setText(" Page " + page + " ");
            }
        }
    }

    void toggleFullScreen(boolean goFullScreen)
    {
        if(goFullScreen)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
        else
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        mList.requestLayout();
    }
    public int getComputedScrollY() {
        int pos, nScrollY, nItemY;
        View view = null;
        pos = mList.getFirstVisiblePosition();
        view = mList.getChildAt(0);
        if(view==null) return 0;
        nItemY = view.getTop();
        if(pos >= mItemtemp.length) return 0;
        mItemtemp[pos] = view.getHeight();
        mItemOffsetY[pos ] = 0;
        for(int i=0;i <pos;i++){
            mItemOffsetY[pos] += mItemtemp[i];
        }
        nScrollY = mItemOffsetY[pos] - nItemY;
        return nScrollY;
    }
    protected void alertSetQuickmenu(final TextView tx,final int value) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.setquickmenu, null);
        final EditText edit1 = (EditText) dialog.findViewById(R.id.alert_edit1);
        edit1.setFocusable(true);
        edit1.requestFocus();
        edit1.setInputType(InputType.TYPE_CLASS_NUMBER);
        try {
            edit1.setText(tx.getText().toString().split("=")[1]);
        } catch (Exception e) {
            edit1.setText("0");
            e.printStackTrace();
        }
        builder.setTitle("Quick menu")
                .setView(dialog)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Global.iHome = Integer.parseInt(edit1.getText().toString());
                        writeSetHome(Global.iHome);
                        String temp = "f="+edit1.getText().toString();
                        writeQuickreturn(value,temp);
                        tx.setText(temp);
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
    }
    protected void alertSetHome(final TextView txtHome) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.sethome, null);
        final EditText editText = (EditText)layout.findViewById(R.id.alert_edit1);
        try {
            editText.setText(""+Global.iHome);
        } catch (Exception e) {
            editText.setText("0");
            e.printStackTrace();
        }
        editText.setSelection(editText.getText().length());
        builder.setTitle("Home")
                .setView(layout)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            Global.iHome = Integer.parseInt(editText.getText().toString());
                        }catch (Exception e){
                            Global.iHome = 0;
                        }
                        txtHome.setText("" + Global.iHome);
                        writeSetHome(Global.iHome);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();

    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orient = getResources().getConfiguration().orientation;
        switch(orient) {
            case Configuration.ORIENTATION_LANDSCAPE:
                getRevolution();
                initQuickReturn();
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                getRevolution();
                initQuickReturn();
                break;
        }
    }

    private void initQuickReturn(){
        mQuickReturnLayout = (LinearLayout)findViewById(R.id.compose_button_bar);
        mQuickReturnLayout.removeAllViews();
        TextView tx;
        ImageView img;
        int dpWidth = Util.convertDpToPx(mContext,40);
        Global.iNumQuickLink = settings.getString("NUMQUICKLINK", "5");
        int num = Integer.parseInt(Global.iNumQuickLink);
        LinearLayout.LayoutParams param;
        // image for up menu
        img = new ImageView(mQuickReturnLayout.getContext());
        img.setImageDrawable(getResources().getDrawable(R.drawable.menu_up));
        param = new LinearLayout.LayoutParams(dpWidth,dpWidth);
        img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    mList.clearFocus();
//                    mList.requestFocusFromTouch();
                    mList.post(new Runnable() {
                        @Override
                        public void run() {
                            mList.setSelection(0);
//                            mList.clearFocus();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Global.setBackgroundItemThread(img);
        img.setScaleType(ImageView.ScaleType.FIT_XY);
        img.setLayoutParams(param);
        mQuickReturnLayout.addView(img);

        if(num >= 5) param = new LinearLayout.LayoutParams((Global.width-2*dpWidth)/5,LinearLayout.LayoutParams.MATCH_PARENT);
        else param = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT,1.0F);
        for(int i = 0; i < Integer.parseInt(Global.iNumQuickLink); i++) {
            tx = new TextView(mQuickReturnLayout.getContext());
            Global.setBackgroundItemThread(tx);
            tx.setMinHeight(Util.convertDpToPx(mContext,40));
            if(i%5 == 0){
                tx.setTextColor(getResources().getColor(R.color.colorquiclink1));
                tx.setText(settings.getString("QUICKLINK"+i,"f=0"));
            } else if (i % 5 == 1){
                tx.setTextColor(getResources().getColor(R.color.colorquiclink2));
                tx.setText(settings.getString("QUICKLINK"+i,"f=32"));
            } else if (i % 5 == 2){
                tx.setTextColor(getResources().getColor(R.color.colorquiclink3));
                tx.setText(settings.getString("QUICKLINK"+i,"f=26"));
            } else if (i % 5 == 3){
                tx.setTextColor(getResources().getColor(R.color.colorquiclink4));
                tx.setText(settings.getString("QUICKLINK"+i,"f=17"));
            }else if (i % 5 == 4){
                tx.setTextColor(getResources().getColor(R.color.colorquiclink1));
                tx.setText(settings.getString("QUICKLINK"+i,"f=33"));
            }
            tx.setGravity(Gravity.CENTER);
            tx.setLayoutParams(param);
            final int finalI = i;
            tx.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    alertSetQuickmenu((TextView)view,finalI);
                    return false;
                }
            });
            final TextView finalTx = tx;
            tx.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String text = ((TextView) finalTx).getText().toString();
                    if(text.equals("f=0")){
//                        mList.clearFocus();
//                        mList.requestFocusFromTouch();
//                        mList.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                mList.setSelection(0);
//                                mList.clearFocus();
//                            }
//                        });
                        Intent i = new Intent(mContext, Page1.class);
                        i.setData(Uri.parse("https://vozforums.com"));
                        startActivity(i);
                        overridePendingTransition(R.anim.rail, R.anim.rail);
                    }else{

                        Intent i = new Intent(mContext, Page2.class);
                        i.putExtra("URL", "forumdisplay.php?" + text);
                        i.putExtra("TITLE", text);
                        startActivity(i);
                        overridePendingTransition(R.anim.rail, R.anim.rail);
                    }
                }
            });
            mQuickReturnLayout.addView(tx);

        }
        // image for up menu
        img = new ImageView(mQuickReturnLayout.getContext());
        img.setImageDrawable(getResources().getDrawable(R.drawable.menu_down));
        param = new LinearLayout.LayoutParams(dpWidth,dpWidth);
        Global.setBackgroundItemThread(img);
        img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    mList.clearFocus();
//                    mList.requestFocusFromTouch();
                    mList.post(new Runnable() {
                        @Override
                        public void run() {
                            int index=0;
                            if(iPage== Global.PAGE_1) index = ((listViewCustom1)mObjectAdapter).getCount();
                            else if(iPage== Global.PAGE_2) index = ((listViewCustom2)mObjectAdapter).getCount()+1;
                            else if(iPage== Global.PAGE_3) index = ((listViewCustom3)mObjectAdapter).getCount()+1;
                            else if(iPage== Global.PAGE_PMDETAIL) index = ((listViewCustom3)mObjectAdapter).getCount()+1;
                            else index = ((listViewCustom2)mObjectAdapter).getCount();
//                            log("index:"+index);
                            mList.setSelection(index);
//                            mList.clearFocus();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        img.setScaleType(ImageView.ScaleType.FIT_XY);
        img.setLayoutParams(param);
        mQuickReturnLayout.addView(img);


        mList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                mQuickReturnHeight = mQuickReturnLayout.getHeight();

            }
        });
        mList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @SuppressLint("NewApi")
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                switch(scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
//                        Global.bScrolling = false;
//                        if(iPage == Global.PAGE_3){
//                            ((listViewCustom3)mObjectAdapter).loadImage(mList.getFirstVisiblePosition()-2,mList.getLastVisiblePosition()-2);
//                        }
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        Global.bScrolling = true;
                        break;
                    default: break;
                }
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                mScrollY = 0;
                int translationY = 0;
                if (scrollIsComputed) {
                    mScrollY = getComputedScrollY();
                }
                int rawY = mScrollY;
                switch (mState) {
                    case STATE_OFFSCREEN:
                        if (rawY >= mMinRawY) {
                            mMinRawY = rawY;
                        } else {
                            mState = STATE_RETURNING;
                        }
                        translationY = rawY;
                        mQuickReturnLayout.setVisibility(View.GONE);
                        break;

                    case STATE_ONSCREEN:
                        if (rawY > mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = rawY;
                        }
                        translationY = rawY;
                        mQuickReturnLayout.setVisibility(View.VISIBLE);
                        break;

                    case STATE_RETURNING:
                        translationY = (rawY - mMinRawY) + mQuickReturnHeight;
                        if (translationY < 0) {
                            translationY = 0;
                            mMinRawY = rawY + mQuickReturnHeight;
                        }

                        if (rawY == 0) {
                            mState = STATE_ONSCREEN;
                            translationY = 0;
                        }

                        if (translationY > mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = rawY;
                        }
//                        mQuickReturnLayout.setVisibility(View.VISIBLE);
                        if(translationY==mQuickReturnHeight) mQuickReturnLayout.setVisibility(View.GONE);
                        else    mQuickReturnLayout.setVisibility(View.VISIBLE);
                        break;
                }

                /** this can be used if the build is below honeycomb **/
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
                    anim = new TranslateAnimation(0, 0, translationY, translationY);
                    anim.setFillAfter(true);
                    anim.setDuration(0);
                    mQuickReturnLayout.startAnimation(anim);
                } else {
                    mQuickReturnLayout.setTranslationY(translationY);
                }
            }
        });
    }

    private void initMenu2(){
        View sidebar2 = LayoutInflater.from(this).inflate(R.layout.sidebar, null);
        final ListView listView2 = (ListView) sidebar2.findViewById(android.R.id.list);
        listView2.setFooterDividersEnabled(true);
        mListSideMenu2 = new ArrayList<>();
        mListSideMenu2.add(new SidebarAdapter.Item("Login", getResources().getDrawable(R.drawable.logovoz),true));
        if(mUser.getAvatar() != null)
            ((SidebarAdapter.Item)mListSideMenu2.get(0)).mIcon = new BitmapDrawable(getResources(),mUser.getAvatar());

        mListSideMenu2.add(new SidebarAdapter.Category("User"));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.usercp), getResources().getDrawable(R.drawable.menu_usercp2)));
        if(iPage==Global.PAGE_3){
            if(!isSubscribe)
                mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.Subscribe), getResources().getDrawable(R.drawable.subscribe)));
            else
                mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.UnSubscribe), getResources().getDrawable(R.drawable.unsubscribe)));
        }
        if(iPage==Global.PAGE_2){
            mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.NewThread), getResources().getDrawable(R.drawable.menu_newthread)));
        }
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.ListBookmark), getResources().getDrawable(R.drawable.list_bookmark)));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.ListQuote), getResources().getDrawable(R.drawable.menu_quote)));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.Search), getResources().getDrawable(R.drawable.menu_search)));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.PM), getResources().getDrawable(R.drawable.menu_pm)));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.NewPost), getResources().getDrawable(R.drawable.menu_newpost)));
        mListSideMenu2.add(new SidebarAdapter.Category("Go Address"));
        mListSideMenu2.add(new SidebarAdapter.Item(getResources().getString(R.string.GoAddress), getResources().getDrawable(R.drawable.menu_forum)));
        mAdapterSideMenu2 = new SidebarAdapter(mContext, mListSideMenu2);
        setLeftDrawer(mAdapterSideMenu2);
        mAdapterSideMenu2.setOnActionItemClickListener(new SidebarAdapter.OnActionItemClickListener() {
            @Override
            public void onItemClick(int pos, Object object) {
                // TODO Auto-generated method stub
                String typeItem = ((SidebarAdapter.Item) object).mTitle;
                if (typeItem.equals(getResources().getString(R.string.usercp))) {
                    if(mUser.isLogin())
                        startActivity(new Intent(mContext, PageCP.class));
                    else
                        toast("Hãy đăng nhập");
                } else if (typeItem.equals(getResources().getString(R.string.GoAddress))) {
                    alertGoAddress();
                }else if(typeItem.equals(getResources().getString(R.string.Subscribe))){
                    TaskCancle();
                    mTask = new TaskGetHtml();
                    mTask.execute(Global.TASK_SUBSCRIBE);
                }else if(typeItem.equals(getResources().getString(R.string.UnSubscribe))){
                    TaskCancle();
                    mTask = new TaskGetHtml();
                    mTask.execute(Global.TASK_UNSUBSCRIBE);
                }else if(typeItem.equals(getResources().getString(R.string.ListBookmark))){
                    startActivity(new Intent(mContext, PageBookmark.class));
                }else if(typeItem.equals(getResources().getString(R.string.ListQuote))){
                    startActivity(new Intent(mContext, PageQuote.class));
                }else if (typeItem.equals(getResources().getString(R.string.NewPost))){
                    if(!mUser.isLogin()){
                        toast("Chưa đăng nhập");
                    }else{
                        Intent i = new Intent(mContext, PageNewPost.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.rail, R.anim.rail);
                    }
                }else if(typeItem.equals(getResources().getString(R.string.NewThread))){
                    if(!mUser.isLogin()){
                        toast("Chưa đăng nhập");
                    }else{

                        Intent i = new Intent(mContext, PageNewThread.class);
                        Bundle extras = new Bundle();
                        if(mParser.getUrl().contains("f=")){
                            extras.putString("FORUM",mParser.getUrl().split("f=")[1]);
                        }
                        extras.putInt("POSTTYPE", Global.REQUEST_NEWTHREAD);
                        i.putExtras(extras);
                        startActivityForResult(i, Global.REQUEST_NEWTHREAD);
                    }
                }else if(typeItem.equals(getResources().getString(R.string.Search))){
                    if(!mUser.isLogin()){
                        toast("Chưa đăng nhập");
                    }else
                        alertSearch();
                }else if(typeItem.equals(getResources().getString(R.string.PM))){
                    if(!mUser.isLogin()){
                        toast("Chưa đăng nhập");
                    }else
                        startActivity(new Intent(mContext, PagePM.class));
                }
                if (pos == 0) {
                    alertLogin();
                }
                closeDrawer();
            }
        });
    }
    private void initMenu() {
        initMenu2();
//        initMenu1();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mUser.isLogin() ) {
            new getAvatar().execute();
        } else {
            ((SidebarAdapter.Item) mListSideMenu2.get(0)).mIcon = getResources().getDrawable(R.drawable.login);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    void initUI() {
        mContext = this;
        mDataBookmark = new CommentsDataSource(mContext);
        mDataBookmark.open();
        mImageLoad = new ImageLoad();
        mList = (ListView) findViewById(R.id.content_frame);
        if (iPage==Global.PAGE_2 || iPage == Global.PAGE_NEWPOST || iPage == Global.PAGE_RECENTPOST || iPage == Global.PAGE_RECENTTHREAD ||
                iPage==Global.PAGE_SEARCH || iPage == Global.PAGE_QUOTE) {
            mList.addFooterView(getFooterView(Gravity.CENTER));
        }else if(iPage ==Global.PAGE_3 || iPage ==Global.PAGE_3HTML){
            mList.addHeaderView(getHeaderView(Gravity.CENTER));
            mList.addFooterView(getFooterView(Gravity.CENTER));
        }
        initMenu();
        initQuickReturn();
    }
    protected void addHeader(){
        if (iPage==Global.PAGE_2 || iPage == Global.PAGE_NEWPOST || iPage == Global.PAGE_RECENTPOST || iPage == Global.PAGE_RECENTTHREAD ||
                iPage==Global.PAGE_SEARCH || iPage == Global.PAGE_QUOTE) {
            mList.addFooterView(getFooterView(Gravity.CENTER));
        }else if(iPage ==Global.PAGE_3 || iPage ==Global.PAGE_3HTML){
            mList.addHeaderView(getHeaderView(Gravity.CENTER));
            mList.addFooterView(getFooterView(Gravity.CENTER));
        }
    }
    private void alertSetSign(final TextView textSign){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.setsign, null);
        final EditText editText = (EditText)layout.findViewById(R.id.editYourDevice);
        editText.setText(Global.sYourDevice);
        editText.setSelection(editText.getText().length());
        builder.setTitle("Sign")
                .setView(layout)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Global.sYourDevice = editText.getText().toString();
                        textSign.setText(Global.sYourDevice);
                        writeSetYourDevices(Global.sYourDevice);
                    }
                });
        builder.create().show();
    }
    protected void alertSeCustomTheme(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Custom Theme")
                .setSingleChoiceItems(R.array.customtheme,1,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertPickerColor(i);
                    }
                })
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
    }

    protected void alertPickerColor(final int id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.pickercolor, null);
        builder.setTitle("Choose color")
                .setView(dialog)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        ColorPicker picker = (ColorPicker)dialog. findViewById(R.id.picker);
        final EditText editHexColor = (EditText)dialog.findViewById(R.id.editColor);
        SVBar svBar = (SVBar)dialog. findViewById(R.id.svbar);
        OpacityBar opacityBar = (OpacityBar)dialog.  findViewById(R.id.opacitybar);
        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);

        int color=0;
        if (id == 2) {
            color = Global.mCusThemeTitleBg;
        } else if (id == 0) {
            color = Global.mCusThemeBg ;
        } else if (id == 1) {
            color = Global.mCusThemeBgFocus ;
        } else if (id == 4) {
            color = Global.mCusThemeTxt1 ;
        } else if (id == 5) {
            color = Global.mCusThemeTxt2;
        }else if (id == R.id.typeTextTitle) {
            color = 3 ;
        } else if (id == 6) {
            color =  Global.mCusThemeQuicklink ;
        }
        picker.setColor(color);
        editHexColor.setText(String.valueOf(Integer.toHexString(color)));
        editHexColor.setEnabled(false);

        picker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                editHexColor.setText(String.valueOf(Integer.toHexString(color)));
                if (id == 2) {
                    Global.mCusThemeTitleBg = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.bgtitle] = color;
                } else if (id == 0) {
                    Global.mCusThemeBg = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.bg] = color;
                } else if (id == 1) {
                    Global.mCusThemeBgFocus = color;
                } else if (id == 4) {
                    Global.mCusThemeTxt1 = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.txColor1] = color;
                } else if (id == 5) {
                    Global.mCusThemeTxt2 = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.txColor2] = color;
                }  else if (id == 3) {
                    Global.mCusThemeTxtTitle = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.txtitle] = color;
                } else if (id == 6) {
                    Global.mCusThemeQuicklink = color;
                    Global.themeColor[Global.THEME_CUSTOM][Global.quicklink] = color;
                }
                UpdateUI();
                writeSetCusTheme();
            }
        });
        builder.create().show();
    }

    protected void alertSetTheme(final TextView textTheme){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final String[] stringTheme={"Voz","Black","Wood","Theme 1","Theme 2","Theme 3","Theme 4","Theme 5"};
//        final String[] stringTheme={"Voz","Black","Wood","Theme 1","Theme 2","Theme 3","Theme 4","Theme 5","Custom"};
        builder.setTitle("Theme")
                .setSingleChoiceItems(stringTheme,Global.iTheme,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Global.iTheme = i;
                        if(textTheme != null) writeSetTheme(Global.iTheme);
                        textTheme.setText(stringTheme[i]);
                        UpdateUI();
                        if(Global.iTheme == Global.THEME_CUSTOM){
                            readCustomTheme();
                            alertSeCustomTheme();
                        }
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
    }

    protected Dialog alertSetSizeImage(){
        final Dialog dialog = new Dialog(mContext);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle("Set Image");
        dialog.setContentView(R.layout.setsizeimge);
//		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,  WindowManager.LayoutParams.WRAP_CONTENT);
        final RadioGroup radioSizeImage = (RadioGroup)dialog.findViewById(R.id.typeSizeImage);

        if(Global.iSizeImage==Global.REDUCE1){
            radioSizeImage.check(R.id.typeSizeImage_1);
        }else if(Global.iSizeImage==Global.REDUCE2){
            radioSizeImage.check(R.id.typeSizeImage_2);
        }else if(Global.iSizeImage==Global.REDUCE4){
            radioSizeImage.check(R.id.typeSizeImage_4);
        }else if(Global.iSizeImage==Global.REDUCE8){
            radioSizeImage.check(R.id.typeSizeImage_8);
        }else if(Global.iSizeImage==Global.REDUCENONE){
            radioSizeImage.check(R.id.typeSizeImage_none);
        }
        radioSizeImage.setOnCheckedChangeListener(new android.widget.RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if(radioSizeImage.getCheckedRadioButtonId()==R.id.typeSizeImage_1){
                    Global.iSizeImage = Global.REDUCE1;
                }else if(radioSizeImage.getCheckedRadioButtonId()==R.id.typeSizeImage_2){
                    Global.iSizeImage = Global.REDUCE2;
                }else if(radioSizeImage.getCheckedRadioButtonId()==R.id.typeSizeImage_4){
                    Global.iSizeImage = Global.REDUCE4;
                }else if(radioSizeImage.getCheckedRadioButtonId()==R.id.typeSizeImage_8){
                    Global.iSizeImage = Global.REDUCE8;
                }else if(radioSizeImage.getCheckedRadioButtonId()==R.id.typeSizeImage_none){
                    Global.iSizeImage = Global.REDUCENONE;
                }
                writeSetImageSize(Global.iSizeImage);
            }
        });
        dialog.show();
        return dialog;
    }

    protected Dialog alertSearch() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.search);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        final EditText edit1 = (EditText) dialog.findViewById(R.id.edit);
        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        final RadioGroup radioType = (RadioGroup)dialog.findViewById(R.id.type);
        Button butMyRecentPost = (Button)dialog.findViewById(R.id.MyRecentPosts);
        Button butMyRecentThread = (Button)dialog.findViewById(R.id.MyRecentThreads);
        final Button butAdvanceSearch = (Button)dialog.findViewById(R.id.AdvanceSearch);
        radioType.setOnCheckedChangeListener(new android.widget.RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if(radioType.getCheckedRadioButtonId() == R.id.showpost)
                    sSearch_ShowPost="1";
                else
                    sSearch_ShowPost="0";
            }
        });
        butAdvanceSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                readStringForum();
                final Dialog dialog1 = new Dialog(mContext);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.listforum);
                dialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT);
                ListView lv = (ListView)dialog1.findViewById(R.id.list);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_list_item_1, lsForum);
                lv.setAdapter(adapter);
                lv.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                            long arg3) {
                        // TODO Auto-generated method stub
                        butAdvanceSearch.setText("Advance Search \nSearch in forums:"+lsForum.get(arg2));
                        String sectoken;
                        if(doc!=null && doc.select("input[name=securitytoken]").first()!=null){
                            sectoken = doc.select("input[name=securitytoken]").first().attr("value");
                            mUser.setToken(sectoken);
                        }
                        sForum = lIdForum.get(arg2);
                        sMessSearch = edit1.getText().toString();
                        Intent i = new Intent(mContext,PageSearch.class);
                        i.putExtra("MESS", sMessSearch);
                        i.putExtra("FORUM", sForum);
                        i.putExtra("SHOWPOST", sSearch_ShowPost);
                        startActivity(i);
                        dialog1.dismiss();
                    }
                });
                dialog1.show();
            }
        });
        butMyRecentThread.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(mContext, PageRecentThread.class));
                dialog.dismiss();
            }
        });
        butMyRecentPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(mContext, PageRecentPost.class));
                dialog.dismiss();
            }
        });
        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(mContext,PageSearch.class);
                sMessSearch = edit1.getText().toString();
                i.putExtra("MESS", sMessSearch);
                i.putExtra("SHOWPOST", sSearch_ShowPost);
                startActivity(i);
                dialog.dismiss();

            }
        });

        dialog.show();
        return dialog;
    }
    protected Dialog alertGoAddress() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.go_address);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        final AutoCompleteTextView edit1 = (AutoCompleteTextView) dialog .findViewById(R.id.alert_edit1);
        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
        final RadioGroup radio = (RadioGroup) dialog.findViewById(R.id.type);
        edit1.setFocusable(true);
        edit1.requestFocus();
        edit1.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        final URLHistory history = new URLHistory(this,
                android.R.layout.simple_dropdown_item_1line);
        try {

            File historyFile = new File(getFilesDir(), Global.HISTORY_FILE);
            if (historyFile.exists()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        openFileInput(Global.HISTORY_FILE)));
                String str;
                StringBuilder buf = new StringBuilder();
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                    buf.append("\n");
                }
                in.close();
                history.load(buf.toString());
            }
        } catch (Throwable t) {
            Log.e("EVPlayer", "Exception in loading history", t);
        }
        edit1.setAdapter(history);
        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String url = edit1.getText().toString();
//				history.update(url);
//				try {
//					OutputStream out = openFileOutput(Global.HISTORY_FILE,
//							MODE_PRIVATE);
//					OutputStreamWriter writer = new OutputStreamWriter(out);
//					history.save(writer);
//					writer.close();
//				} catch (Throwable t) {
//					Log.e("EVPlayer", "Exception writing history", t);
//				}
//				url = url.toLowerCase();
                if (radio.getCheckedRadioButtonId() == R.id.address_forum) {
                    Intent i = new Intent(mContext, Page2.class);
                    i.putExtra("URL", "forumdisplay.php?f=" + url);
                    i.putExtra("TITLE", url);
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                } else if (radio.getCheckedRadioButtonId() == R.id.address_thread) {
                    Intent i = new Intent(mContext, Page3.class);
                    i.putExtra("URL", "showthread.php?t=" + url);
                    i.putExtra("TITLE", url);
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                } else if (radio.getCheckedRadioButtonId() == R.id.address_post) {
                    Intent i = new Intent(mContext, Page3.class);
                    i.putExtra("URL", "showpost.php?p=" + url);
                    i.putExtra("TITLE", url);
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }else if(radio.getCheckedRadioButtonId() == R.id.address_home) {
                    Intent i = new Intent(mContext, Page1.class);
                    i.setData(Uri.parse("https://vozforums.com"));
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }

                dialog.dismiss();
            }
        });
        buttonCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }
    protected void alertSetPathSave(final TextView textPath) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.sethome, null);
        final EditText editText = (EditText)layout.findViewById(R.id.alert_edit1);
        editText.setText(Global.mSavePath);
        editText.setSelection(editText.getText().length());
        builder.setTitle("Path for save image")
                .setView(layout)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Global.mSavePath = editText.getText().toString();
                        textPath.setText(Global.mSavePath);
                        writeSetPathSave(Global.mSavePath);
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
//        final Dialog dialog = new Dialog(mContext);
//        dialog.setTitle("Set path for save image");
//        dialog.setContentView(R.layout.sethome);
//        final EditText edit1 = (EditText) dialog.findViewById(R.id.alert_edit1);
//        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
//        Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
//        edit1.setFocusable(true);
//        edit1.requestFocus();
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        //edit1.setText(String.valueOf(Global.iHome));
//        edit1.setText(Global.mSavePath);
//        edit1.setSelection(edit1.getText().length());
//        buttonOK.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Global.mSavePath = edit1.getText().toString();
//                writeSetPathSave(Global.mSavePath);
//                dialog.dismiss();
//            }
//        });
//        buttonCancle.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//        return dialog;
    }
    protected void alertSetQuickLink(final TextView textQuickLink) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.setquicklink, null);
        final EditText editText = (EditText)layout.findViewById(R.id.numQuickLink);
        editText.setText(Global.iNumQuickLink);
        editText.setSelection(editText.getText().length());
        builder.setTitle("Quick Link")
                .setView(layout)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Global.iNumQuickLink = editText.getText().toString();
                        textQuickLink.setText(Global.iNumQuickLink);
                        writeSetNumQuickLink(Global.iNumQuickLink);
                        initQuickReturn();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create().show();
    }
    private void alertSettings(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.setfont2, null);
        builder.setTitle("Settings")
                .setView(layout)
                .setNegativeButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        LinearLayout rlSeekBar = (LinearLayout) layout.findViewById(R.id.layout_seekbar);
        final TextView text1 = (TextView) rlSeekBar .findViewById(R.id.alert_txt1);
        text1.setGravity(Gravity.CENTER);
        SeekBar sb = (SeekBar) layout.findViewById(R.id.alert_seekbar);
        sb.setMax(Global.FONT_MAX);
        sb.setProgress((int) (Global.FONT_MAX
                * (mTextSize - Global.FONTSCALE_MIN) / (Global.FONTSCALE_MAX - Global.FONTSCALE_MIN)));
        text1.setText(String.valueOf(sb.getProgress()));
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                text1.setText(String.valueOf(progress));
                mTextSize = (float) ((Global.FONTSCALE_MAX - Global.FONTSCALE_MIN)
                        / (Global.FONT_MAX - 0) * (progress) + Global.FONTSCALE_MIN);
                updateSetFont(mTextSize);

                Global.iSize = mTextSize;

            }
            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub
                writeSetFont(mTextSize);
            }
        });
        //Check Setting
        ArrayList<SettingAdapter.Item> itemSettings = new ArrayList<SettingAdapter.Item>();
        itemSettings.add(new SettingAdapter.Item(settings,"Fullscreen","FULLSCREEN",Global.bFullScreen,Global.bFullScreen));
        itemSettings.add(new SettingAdapter.Item(settings,"Icon header","TOPICHEADER",Global.bTopicHeader,Global.bTopicHeader));
        itemSettings.add(new SettingAdapter.Item(settings,"Vibrate","VIBRATE",Global.bVibrate,Global.bVibrate));
        itemSettings.add(new SettingAdapter.Item(settings,"Notification","NOTIFICATIONSUBSCRIBE",Global.bNotifSubscribe,Global.bNotifSubscribe));
        itemSettings.add(new SettingAdapter.Item(settings,"Divider","DEVIDER",Global.bDevider,Global.bDevider));
        itemSettings.add(new SettingAdapter.Item(settings,"Sign","SIGN",Global.bSign,Global.bSign));
        ExpandableHeightGridView gridSetting = (ExpandableHeightGridView)layout.findViewById(R.id.gridview_setting);
        SettingAdapter adapter = new SettingAdapter(mContext,itemSettings);
        adapter.setOnActionItemClickListener(new SettingAdapter.OnActionItemClickListener() {
            @Override
            public void onItemClick(int type, boolean value, SettingAdapter.Item item) {
                if (type == 0) {
                    toggleFullScreen(value);
                }else if(type ==6){
                    UpdateUI();
                }
            }
        });
        gridSetting.setAdapter(adapter);
        gridSetting.setExpanded(true);
        //Theme
        LinearLayout layoutTheme = (LinearLayout)layout.findViewById(R.id.layouttheme);
        String[] stringTheme={"Voz","Black","Wood","Theme 1","Theme 2","Theme 3","Theme 4","Theme 5","Custom"};
        final TextView textTheme = (TextView)layout.findViewById(R.id.theme);
        textTheme.setText(stringTheme[Global.iTheme]);
        layoutTheme.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSetTheme(textTheme);
            }
        });
        //YourDevice
        LinearLayout layoutYourDevice = (LinearLayout)layout.findViewById(R.id.layoutYourSign);
        final TextView textSign = (TextView)layout.findViewById(R.id.edityourdevice);
        textSign.setText(Global.sYourDevice);
        layoutYourDevice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSetSign(textSign);
            }
        });
        //Home
        LinearLayout layoutHome = (LinearLayout)layout.findViewById(R.id.layoutHome);
        final TextView textHome = (TextView)layout.findViewById(R.id.edithome);
        textHome.setText(""+Global.iHome);
        layoutHome.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSetHome(textHome);
            }
        });
        //QuickLink
        LinearLayout layoutQuickLink = (LinearLayout)layout.findViewById(R.id.layout_quickink);
        final TextView textQuickLink = (TextView)layout.findViewById(R.id.editQuicklink);
        textQuickLink.setText(Global.iNumQuickLink);
        layoutQuickLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSetQuickLink(textQuickLink);
            }
        });
        //PathSave
        LinearLayout layoutPathsave = (LinearLayout)layout.findViewById(R.id.layout_pathsave);
        final TextView textPathSave = (TextView)layout.findViewById(R.id.editPathSave);
        textPathSave.setText(Global.mSavePath);
        layoutPathsave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSetPathSave(textPathSave);
            }
        });
        builder.create().show();
    }

    protected void updateSetFont(float value) {

    }

    protected void alertGoPage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.login, null);
        final EditText edit1 = (EditText) dialog.findViewById(R.id.alert_edit1);
        final EditText edit2 = (EditText) dialog.findViewById(R.id.alert_edit2);
        final TextView text1 = (TextView) dialog.findViewById(R.id.alert_txt1);
        final TextView text2 = (TextView) dialog.findViewById(R.id.alert_txt2);
        final Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        final Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
        edit2.setVisibility(View.GONE);
        edit1.setInputType(InputType.TYPE_CLASS_NUMBER);
        text1.setVisibility(View.GONE);
        text2.setVisibility(View.GONE);
        builder.setTitle("Page")
                .setView(dialog) ;
        final Dialog mDialog = builder.create();
        mDialog.show();


        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // mTask.cancel(true);
                try {
                    GoPage(Global.GO_REFRESH, Integer.parseInt(edit1.getText().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mDialog.dismiss();
            }
        });
        buttonCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // mTask.cancel(true);
                // GoPage(GO_REFRESH,
                // Integer.parseInt(edit1.getText().toString()));
                mDialog.dismiss();
            }
        });
//        dialog.show();
//        return dialog;

    }
    private Dialog alert(String sAlarm) throws Exception{
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        TextView txAlarm = (TextView)dialog.findViewById(R.id.tx_alarm);
        Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
        txAlarm.setText(sAlarm);
        buttonCancle.setText("Trở về");
        buttonOK.setText("Tải lại");
        //buttonOK.setText(getResources().getString(R.string.Logout));
        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TaskCancle();
                mTask = new TaskGetHtml();
                mTask.execute(Global.TASK_GETDOC);
//                myVolley.getDoc(mUser);
                dialog.dismiss();
            }
        });
        buttonCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                onBackPressed();
            }
        });

        dialog.show();
        return dialog;
    }
    protected void alertLogin() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.login, null);
        final EditText edit1 = (EditText) dialog.findViewById(R.id.alert_edit1);
        final EditText edit2 = (EditText) dialog.findViewById(R.id.alert_edit2);
        final TextView text1 = (TextView) dialog.findViewById(R.id.alert_txt1);
        final TextView text2 = (TextView) dialog.findViewById(R.id.alert_txt2);
        final Button buttonOK = (Button) dialog.findViewById(R.id.alert_ok);
        final Button buttonCancle = (Button) dialog.findViewById(R.id.alert_cancle);
        final RadioGroup group = (RadioGroup)dialog.findViewById(R.id.rg_usered);
        RadioButton button;
        builder.setTitle("Login")
                .setView(dialog);
        final Dialog mDialog = builder.create();
        mDialog.show();
        mRadioUser = null;
        String mUsered = settings.getString("usered",null);
        mArrayUsered = new ArrayList<String>();
        mArrayPass = new ArrayList<String>();
        mArrayUseredId = new ArrayList<String>();
        if(mUsered != null){
            String lUsered[] = mUsered.split(";");
            for(String tmp:lUsered){
                mArrayUsered.add(settings.getString("username-" + tmp, ""));
                mArrayPass.add(settings.getString("password-" + tmp, ""));
                mArrayUseredId.add(settings.getString("userid-" + tmp, ""));
            }
        }

        for(int i = 0; i < mArrayUsered.size(); i++) {
            button = new RadioButton(this);
            button.setText(mArrayUsered.get(i));
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRadioUser = ((Button)view).getText().toString();
                    edit1.setVisibility(View.GONE);
                    edit2.setVisibility(View.GONE);
                    text1.setVisibility(View.GONE);
                    text2.setVisibility(View.GONE);
                    mUsername = null;
                    mPassword = null;
                    if(mRadioUser.equals(mUser.User())){
                        buttonOK.setText("Log out");
                        buttonCancle.setText("Cancel");
                    }else{
                        buttonOK.setText("Log in");
                        buttonCancle.setText("Clear");
                    }
                }
            });
            group.addView(button);
            if(mUser.User() != null && mUser.User().equals(mArrayUsered.get(i))){
                mRadioUser = mUser.User();
                group.check(button.getId());
                edit1.setVisibility(View.GONE);
                edit2.setVisibility(View.GONE);
                text1.setVisibility(View.GONE);
                text2.setVisibility(View.GONE);
                buttonOK.setText("Log out");
                buttonCancle.setText("Cancel");
            }
        }
        button = new RadioButton(this);
        button.setText("Add");
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                edit1.setVisibility(View.VISIBLE);
                edit2.setVisibility(View.VISIBLE);
                text2.setVisibility(View.VISIBLE);
                text1.setVisibility(View.VISIBLE);
                mRadioUser = ((Button)view).getText().toString();
                buttonOK.setText("Log in");
                buttonCancle.setText("Cancel");
            }
        });
        group.addView(button);
        if(mRadioUser == null){
            mRadioUser = "Add";
            group.check(button.getId());
            buttonOK.setText("Log in");
            buttonCancle.setText("Cancel");
        }
        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                TaskCancle();
                mTask = new TaskGetHtml();
                //myVolley.cancel();
                if (mRadioUser.equals("Add")) {
                    mUsername = edit1.getText().toString();
                    mPassword = edit2.getText() .toString();
                    mUser.add(mUsername, mPassword);
                    mTask.execute(Global.TASK_LOGIN);
                } else {
                    if(mRadioUser == null || mRadioUser.equals(mUser.User())){
                        mTask.execute(Global.TASK_LOGOUT);
                    }else{
                        mUser.add(mRadioUser,mArrayPass.get(mArrayUsered.indexOf(mRadioUser)) );
                        mTask.execute(Global.TASK_LOGIN);
                    }
                }
                mDialog.dismiss();
            }
        });
        buttonCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //if(!mRadioUser.equals("New") && mUser.User()!= null && !mRadioUser.equals(mUser.User())){
                if(buttonCancle.getText().toString().equals("Clear")){
                    SharedPreferences.Editor editor = settings.edit();
                    String userid = mArrayUseredId.get(mArrayUsered.indexOf(mRadioUser));
                    editor.remove("username-" + userid);
                    editor.remove("userid-" + userid);
                    editor.remove("password-" + userid);
                    String mUsered = settings.getString("usered",null);
                    mUsered = mUsered.replace(userid,"");
                    if(mUsered.startsWith(";")) mUsered = mUsered.substring(1,mUsered.length());
                    else if(mUsered.endsWith(";")) mUsered = mUsered.substring(0,mUsered.length()-1);
                    else if(mUsered.contains(";;")) mUsered = mUsered.replace(";;",";");
                    editor.putString("usered",mUsered);
                    editor.commit();
                }
                mDialog.dismiss();
            }
        });

    }

}
//
