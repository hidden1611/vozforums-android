package com.nna88.voz.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nna88.voz.contain.containEmo;
import com.nna88.voz.listview.GridAdapter;
import com.nna88.voz.parserhtml.parser;
import com.nna88.voz.util.UploadImage2;
import com.nna88.voz.util.UserInfo;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;


public class PageNewThread extends Activity {
	private EditText m_TextPost;
	private Button m_ButPost;
	private Button m_ButPostCancle;
	private ImageButton m_ButPostEmo;
	private ImageButton m_ButLink;
    private ImageButton m_CameraButton;
    private Context mContext;
    private String sSignature;
    private String sSignature1;
    private EditText textTitle;
    private String sforums;
    private String secToken;
    private String postHash;
    private String startTime;
    private parser mParser;
    private Task mTask;
    private Document doc;
    private GridView gridview ;
    private ArrayList<containEmo> listEmo;
    private int iPostType;
    private String pmToken,pmTitle,pmMid,pmUserId,pmReciple;
    protected Dialog mLoading;
    private int iUpanh = 0;
    private UserInfo mUser;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pagereply);
		super.onCreate(savedInstanceState);
        initUI();
        mUser = new UserInfo();
		sSignature = "\n\n[I]Sent from my "
				+ Global.sYourDevice 
				+ " using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]";
        sSignature1 = "\n\n[I]Sent from my "+Global.sYourDevice +" using vozForums[/I]";
		iPostType = getIntent().getIntExtra("POSTTYPE", 0);

		if(iPostType==Global.REQUEST_NEWTHREAD){ //new thread
			sforums = getIntent().getStringExtra("FORUM");
			mParser = new parser("https://vozforums.com/newthread.php?do=newthread&f="+sforums);
			mTask = new Task();
			mTask.execute(Global.TASK_GETDOC);
		}else if(iPostType==Global.REQUEST_PMINSERT){//repley pm
			mTask = new Task();
			mParser = new parser("https://vozforums.com/newthread.php?do=newthread&f="+sforums);
			pmToken = getIntent().getStringExtra("TOKEN");
			pmMid = getIntent().getStringExtra("MID");
			pmUserId = getIntent().getStringExtra("USERID");
			pmReciple = getIntent().getStringExtra("RECIPLIES");
			pmTitle = getIntent().getStringExtra("title");
			textTitle.setText(getIntent().getExtras().getString("title"));
			textTitle.setEnabled(false);
			m_TextPost.setText(getMesseage());
			m_TextPost.setSelection(m_TextPost.getText().length());
			m_TextPost.requestFocus();
			m_TextPost.setFocusable(true);
		}else if(iPostType == Global.REQUEST_PMNEW){
			mTask = new Task();
			pmMid = getIntent().getStringExtra("MID");
			mParser = new parser("https://vozforums.com/private.php?do=newpm&u="+pmMid);
			mTask.execute(Global.TASK_GETDOC);
//            myVolley.url = "https://vozforums.com/private.php?do=newpm&u="+pmMid;
//            myVolley.getDoc(mUser);
		}else {// reply
			textTitle.setText(getIntent().getExtras().getString("title"));
			textTitle.setEnabled(false);
			m_TextPost.setText(getMesseage());
			m_TextPost.setSelection(m_TextPost.getText().length());
			m_TextPost.requestFocus();
			m_TextPost.setFocusable(true);
		}
		
		
	}
    protected void write(String ss){
        try {
            FileUtils.writeStringToFile(new File("/mnt/sdcard/test.html"), ss);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	private void initUI(){
		mContext =this;
		textTitle = (EditText)findViewById(R.id.title);
		textTitle.setText(sforums);
		textTitle.setFocusable(true);
		m_TextPost = (EditText)findViewById(R.id.Page3_postReply);
		m_ButPost = (Button)findViewById(R.id.Page3_butReply);
		m_ButLink = (ImageButton)findViewById(R.id.link_button);
        m_CameraButton = (ImageButton)findViewById(R.id.camera_button);
        m_CameraButton.setOnClickListener(mOnCameraClick);
		m_ButPost.setText("Submit");
		m_ButPostEmo = (ImageButton)findViewById(R.id.Page3_butReplyEmo);
		m_ButPostCancle = (Button)findViewById(R.id.Page3_butReplyCancle);
		m_ButPost.setOnClickListener(mPostReplyClick);
		m_ButPostCancle.setOnClickListener(mPostCancleClick);
		m_ButPostEmo.setOnClickListener(mPostEmoClick);
		listEmo = new ArrayList<containEmo>();
		listEmo = Emo2();
		gridview = (GridView)findViewById(R.id.grid_view);
		gridview.setAdapter(new GridAdapter(mContext, listEmo));
		gridview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
            	mHandler.obtainMessage(20,listEmo.get(position).text()).sendToTarget();
            }
        });
		m_ButLink.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alertImage();
			}
		});
      //  gridview.setBackgroundColor(Color.BLACK);
		Global.setBackgroundItemThread(m_TextPost);
		//Global.setBackgroundItemThread(gridview);
		Global.setTextContain(m_TextPost);
		Global.setBackgroundMenuLogo(textTitle);
		Global.setTextMenuTitle(textTitle);

        if(Global.iTheme== Global.THEME_VOZ){
            gridview.setBackgroundColor(getResources().getColor(R.color.vozThread));
        }else if(Global.iTheme == Global.THEME_BLACK){
            gridview.setBackgroundColor(getResources().getColor(R.color.themeblack_background));
        }else if(Global.iTheme == Global.THEME_WOOD){
            gridview.setBackgroundColor(getResources().getColor(R.color.themewood_background));
        }
        mLoading = alertLoading();
	}
	private String getMesseage(){
		String msg = getIntent().getExtras().getString("msg");
		if(iPostType == Global.REQUEST_EDIT){
			if(msg.lastIndexOf(sSignature) != -1){
				msg = msg.substring(0, msg.lastIndexOf(sSignature));
			}else if(msg.lastIndexOf(sSignature1) != -1){
                msg = msg.substring(0, msg.lastIndexOf(sSignature1));
			}
		}
		
		return msg;
	}	
	private void toast(String ss){
		Toast.makeText(PageNewThread.this, ss, Toast.LENGTH_SHORT).show();
	}
	private ArrayList<containEmo> Emo2(){
		ArrayList<containEmo> listEmo = new ArrayList<containEmo>();
		try {
			String list[] = mContext.getAssets().list("images/smilies/Off");
			String temp;
			Bitmap emo;
			InputStream is ;
			for(int i=0;i<list.length;i=i+1){
                if(!list[i].contains(".png")) continue;
				temp = list[i].substring(0,list[i].length()-4);
                is  = mContext.getAssets().open("images/smilies/Off/"+list[i]);
				emo = BitmapFactory.decodeStream(is);
				if(temp.equals("embarrassed")) 				temp = ":\">";
				else if(temp.equals("nosebleed"))			temp = ":chaymau:";
				else if(temp.equals("feel_good"))			temp = ":sogood:";
				else if(temp.equals("sexy_girl"))			temp = ":sexy:";
				else if(temp.equals("look_down"))			temp = ":look_down:";
				else if(temp.equals("beat_brick"))			temp = ":brick:";
				else if(temp.equals("cry"))					temp = ":((";
				else if(temp.equals("beat_plaster"))		temp = ":plaster:";
				else if(temp.equals("confuse"))				temp = ":-s";
				else if(temp.equals("big_smile"))			temp = ":D";
				else if(temp.equals("brick"))				temp = ":gach:";
				else if(temp.equals("burn_joss_stick"))		temp = ":stick:";
				else if(temp.equals("bad_smell"))			temp = ":badsmell:";
				else if(temp.equals("cool"))				temp = ":kool:";
				else if(temp.equals("after_boom"))			temp = ":aboom:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("sweet_kiss"))			temp = ":*";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("smile"))				temp = ":)";
				else if(temp.equals("still_dreaming"))		temp = ":dreaming:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
                else if(temp.equals("bad_smelly"))          temp = ":badsmell:";
                else if(temp.equals("too_sad"))             temp = ":sosad:";
                else if(temp.equals("lay"))                 temp = "^:)^";
                else if(temp.equals("sleep"))                temp = "-_-";
                else if(temp.equals("bann"))                temp = ":ban:";
                else if(temp.equals("shit"))                temp = ":shitty:";
				else 										temp = ":"+temp+":";
				listEmo.add(new containEmo(emo, temp));
				is.close();
			}

            for(int i=0;i<list.length;i=i+1){
                if(!list[i].contains(".gif")) continue;
                temp = list[i].substring(0,list[i].length()-4);
                is  = mContext.getAssets().open("images/smilies/Off/"+list[i]);
                emo = BitmapFactory.decodeStream(is);
                if(temp.equals("embarrassed")) 				temp = ":\">";
                else if(temp.equals("nosebleed"))			temp = ":chaymau:";
                else if(temp.equals("feel_good"))			temp = ":sogood:";
                else if(temp.equals("sexy_girl"))			temp = ":sexy:";
                else if(temp.equals("look_down"))			temp = ":look_down:";
                else if(temp.equals("beat_brick"))			temp = ":brick:";
                else if(temp.equals("cry"))					temp = ":((";
                else if(temp.equals("beat_plaster"))		temp = ":plaster:";
                else if(temp.equals("confuse"))				temp = ":-s";
                else if(temp.equals("big_smile"))			temp = ":D";
                else if(temp.equals("brick"))				temp = ":gach:";
                else if(temp.equals("burn_joss_stick"))		temp = ":stick:";
                else if(temp.equals("bad_smell"))			temp = ":badsmell:";
                else if(temp.equals("cool"))				temp = ":kool:";
                else if(temp.equals("after_boom"))			temp = ":aboom:";
                else if(temp.equals("beat_shot"))			temp = ":shot:";
                else if(temp.equals("sweet_kiss"))			temp = ":*";
                else if(temp.equals("beat_shot"))			temp = ":shot:";
                else if(temp.equals("smile"))				temp = ":)";
                else if(temp.equals("still_dreaming"))		temp = ":dreaming:";
                else if(temp.equals("beat_shot"))			temp = ":shot:";
                else if(temp.equals("beat_shot"))			temp = ":shot:";
                else if(temp.equals("bad_smelly"))          temp = ":badsmell:";
                else if(temp.equals("too_sad"))             temp = ":sosad:";
                else if(temp.equals("lay"))                 temp = "^:)^";
                else if(temp.equals("sleep"))                temp = "-_-";
                else if(temp.equals("bann"))                temp = ":ban:";
                else if(temp.equals("shit"))                temp = ":shitty:";
                else 										temp = ":"+temp+":";
                listEmo.add(new containEmo(emo, temp));
                is.close();
            }
            String list2[] = mContext.getAssets().list("images/smilies/emos");
            for(int i=0;i<list2.length;i=i+1){
                temp = list2[i].substring(0,list2[i].length()-4);
                is  = mContext.getAssets().open("images/smilies/emos/"+list2[i]);
                emo = BitmapFactory.decodeStream(is);
                if(temp.equals("embarrassed")) 				temp = ":\">";
                else if(temp.equals("nosebleed"))			temp = ":chaymau:";
                else 										temp = ":"+temp+":";
                listEmo.add(new containEmo(emo, temp));
                is.close();
            }
            is  = mContext.getAssets().open("+1/button/images/icon.png");
            emo = BitmapFactory.decodeStream(is);
            listEmo.add(new containEmo(emo, ":+1:"));
            is.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listEmo;
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
//		InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
		//inputMethodManager.hideSoftInputFromInputMethod(null, 0);
	}
	
	public final Handler mHandler = new Handler(){
    	@Override
    	public void handleMessage(Message msg) {
    		 switch (msg.what) {
    		 case 20:
    			int start =m_TextPost.getSelectionStart();
 				m_TextPost.getText().insert(start,(String) msg.obj);
     			break;	
    		 }
    	};
	};
	OnItemClickListener mEmoListClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			mHandler.obtainMessage(20,listEmo.get(arg2).text()).sendToTarget();
			
		}
		
	};

	OnClickListener mPostCancleClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			setResult(RESULT_CANCELED);
            finish();
		}
	};
	private Dialog alertImage(){
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_insert);
		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, 
				WindowManager.LayoutParams.WRAP_CONTENT);
		final AutoCompleteTextView edit1 = (AutoCompleteTextView)dialog.findViewById(R.id.alert_edit1);
		final RadioGroup radioButton = (RadioGroup)dialog.findViewById(R.id.typeInsert);
		
		Button buttonOK = (Button)dialog.findViewById(R.id.alert_ok);
		Button buttonCancle = (Button)dialog.findViewById(R.id.alert_cancle);
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		buttonOK.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
				String temp = "[IMG]"+edit1.getText().toString()+"[/IMG]";
				if(radioButton.getCheckedRadioButtonId()==R.id.insert_img){
					temp = "[IMG]"+edit1.getText().toString()+"[/IMG]";
				}else if(radioButton.getCheckedRadioButtonId()==R.id.insert_link){
					temp = "[URL]"+edit1.getText().toString()+"[/URL]";
				}else if(radioButton.getCheckedRadioButtonId()==R.id.insert_B){
					temp = "[B]"+edit1.getText().toString()+"[/B]";
				}else if(radioButton.getCheckedRadioButtonId()==R.id.insert_I){
					temp = "[I]"+edit1.getText().toString()+"[/I]";
				}else if(radioButton.getCheckedRadioButtonId()==R.id.insert_U){
					temp = "[U]"+edit1.getText().toString()+"[/U]";
				}else if(radioButton.getCheckedRadioButtonId()==R.id.insert_quote){
					temp = "[QUOTE]"+edit1.getText().toString()+"[/QUOTE]";
				}
				m_TextPost.getText().insert(start,temp);
				dialog.dismiss();
			}
		});
		buttonCancle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
		return dialog;
	}
    private Dialog alertSelectImage(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_image);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final RadioGroup radioButton = (RadioGroup)dialog.findViewById(R.id.typeUpload);
        Button buttonOK = (Button)dialog.findViewById(R.id.alert_ok);
        Button buttonCancle = (Button)dialog.findViewById(R.id.alert_cancle);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        buttonOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(radioButton.getCheckedRadioButtonId()==R.id.uploadCamera){
                    dialog.dismiss();
                    final Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_a_viewer)), Global.REQUEST_CHOOSE_AN_IMAGE);
                }else if(radioButton.getCheckedRadioButtonId()==R.id.uploadGallery){
                    dialog.dismiss();
                    final Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_a_viewer)), Global.REQUEST_CHOOSE_AN_IMAGE);
                    return;
                }
            }
        });

        buttonCancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }
	OnClickListener mOnCameraClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            alertSelectImage();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Global.REQUEST_CHOOSE_AN_IMAGE){
            if(data == null) return;
            final Uri chosenImageUri = data.getData();
            if(chosenImageUri!=null){
                iUpanh++;
                UploadImage2 mUploadImage = new UploadImage2(mContext,chosenImageUri){
                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        int start =m_TextPost.getSelectionStart();
                        String temp = "[IMG]"+this.url+"[/IMG]\n";
                        String temp2 = "[IMG]"+chosenImageUri.toString()+"[/IMG]\n";
                        String text = m_TextPost.getText().toString();
                        text = text.replace(temp2,temp);
                        m_TextPost.setText(text);
                        m_TextPost.setSelection(m_TextPost.getText().length());
                        iUpanh--;

                    }

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        int start =m_TextPost.getSelectionStart();
                        String temp = "[IMG]"+chosenImageUri.toString()+"[/IMG]\n";
                        m_TextPost.getText().insert(start,temp);
                    }
                };
                mUploadImage.execute();
            }
        }
    }

    private Dialog alertLoading(){
        final Dialog dialog;
        if(Global.bFullScreen){
            dialog = new Dialog(mContext,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        }else{
            dialog = new Dialog(mContext,android.R.style.Theme_Translucent_NoTitleBar);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        TextView tx = (TextView)(dialog.findViewById(R.id.txloadding));
        tx.setVisibility(View.VISIBLE);
        return dialog;
    }
    OnClickListener mPostEmoClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			
			if(gridview.getVisibility() != View.VISIBLE){ 
				//inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
				//inputMethodManager.hideSoftInputFromWindow(null, InputMethodManager.HIDE_IMPLICIT_ONLY);
				gridview.setVisibility(View.VISIBLE);
			}else{
				//inputMethodManager.showSoftInputFromInputMethod(null, InputMethodManager.SHOW_FORCED);
				//inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				gridview.setVisibility(View.GONE);
			}

		}
	};
    OnClickListener mPostReplyClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //if(!mLoading.isShowing()) mLoading.show();
            //mLoading.dismiss();
            if(iUpanh != 0){
                toast("Vui lòng chờ up ảnh xong!");
                return;
            }
            String mss = m_TextPost.getText().toString();
            try {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(mss);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",mss);
                    clipboard.setPrimaryClip(clip);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            mss +="\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]";
            // TODO Auto-generated method stub
            if(iPostType==Global.REQUEST_NEWTHREAD){
                mTask.cancel(true);
                mTask = new Task();
                mTask.execute(Global.TASK_NEWTHREAD);
            }else if(iPostType==Global.REQUEST_PMINSERT){
                mTask.cancel(true);
                mTask = new Task();
                mTask.execute(Global.TASK_PMREPLY);
            }else if(iPostType == Global.REQUEST_PMNEW){
				mTask.cancel(true);
				mTask = new Task();
				mTask.execute(Global.TASK_PMNEW);

//              mParser.PostPMNew(pmReciple,textTitle.getText().toString(), mss, pmToken);
//                myVolley.PostPMNew(mUser,pmReciple,textTitle.getText().toString(), mss);
            }else{
                Intent i = new Intent();
                Bundle extras = new Bundle();
                extras.putString("msg",mss);
                i.putExtras(extras);
                setResult(RESULT_OK,i);
                finish();
            }
        }
    };
	private class Task extends AsyncTask<Integer, Void, Integer>{

		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String mss = m_TextPost.getText().toString();
			mss +="\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]";
			if(params[0] == Global.TASK_GETDOC){
				doc = mParser.getDoc(mUser);
				return Global.TASK_GETDOC;
			}else if(params[0] == Global.TASK_NEWTHREAD){
				mParser.NewThread(mUser,textTitle.getText().toString(),
						mss,
						sforums,
						secToken,
						postHash,
						startTime);
				return Global.TASK_NEWTHREAD;
			}else if(params[0] == Global.TASK_PMREPLY){
				mParser.PostPMReply(mUser,pmUserId,
									pmReciple,
									pmTitle,
									mss,
									pmMid,
									pmToken);
				return Global.TASK_PMREPLY;
			}else if(params[0] ==  Global.TASK_PMNEW){
				mParser.PostPMNew(mUser, pmReciple,textTitle.getText().toString(), m_TextPost.getText().toString(), pmToken);
				return Global.TASK_PMNEW;
			}
			return null;
		}
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result==Global.TASK_GETDOC){
				parser();
			}else if(result==Global.TASK_NEWTHREAD){
				setResult(RESULT_OK);
				finish();
			}else if(result==Global.TASK_PMREPLY){
				setResult(RESULT_OK);
                finish();
			}else if(result==Global.TASK_PMNEW){
				setResult(RESULT_OK);
				finish();
			}
		}
	}
	private void log(String ss){
		Log.d("nna",ss);
	}
	
	private void parser(){
		if(doc != null){
			if(iPostType==Global.REQUEST_NEWTHREAD){
				secToken = doc.select("input[name=securitytoken]").attr("value");
				postHash= doc.select("input[name=posthash]").attr("value");
				startTime = doc.select("input[name=poststarttime]").attr("value");
                mUser.setToken(secToken);
			}else if(iPostType==Global.REQUEST_PMNEW){
				pmToken = doc.select("input[name=securitytoken]").attr("value");
				pmMid = mParser.getUrl().split("u=")[1];
                //pmMid = myVolley.url.split("u=")[1];
				if(doc.select("div[id=pmrecips]").first() != null){
					pmReciple = doc.select("div[id=pmrecips]").first().text();
				}
                mUser.setToken(pmToken);
			}
		}else{
			log("doc =null");
		}
	}
}
