package com.nna88.voz.main;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Global {
    public static final Boolean DEBUG = true;
    public static final Boolean OFFILNE = false;

    public static final String TAG = "vozForums";
    public static final String URL="https://vozforums.com/";
    public static final String URL2="http://www.vozforums.com/";
    public static int themeColor[][]={
     /*  bg   */  /*bgfocus*/ /*bgtitle*/ /*txtitle*/ /*text1*/     /*text2*/  /*Qulink*/  /*stick*/ /*bg1*/
            { 0xFFF5F5FF, 0xFF38C0F4, 0xFF6A7BB0, 0xFF000000, 0xFF6A7BB0, 0xFF000000, 0xcc000000, 0xFFFF0000, 0xffDCDCDA, 0xFF6C789F },//voz
            { 0xFF000000, 0xFF20677D, 0xFF6A7BB0, 0xFFFFFFFF, 0xFF33B5E5, 0xFFEEEEEE, 0xcc000000, 0xFFFF0000, 0xff1B1B1B, 0xFF6C789F },//black
            { 0xFFFFFAEC, 0xFFFFF0AF, 0xFFDC5A1E, 0xFFFFFFFF, 0xFFA87940, 0xFF000000, 0xcc000000, 0xFFFF0000, 0xffDCDCDA, 0xFFDC5A1E },//wood
            { 0xFF000000, 0xFF20677D, 0xFFA349A4, 0xFF000000, 0xFFA349A4, 0xFFEEEEEE, 0xcc000000, 0xFFFF0000, 0xff666666, 0xFFA349A4 },//color1
            { 0xFFF7F7F7, 0xFF20677D, 0xFFA349A4, 0xFF000000, 0xFFA349A4, 0xFF000000, 0xcc000000, 0xFFFF0000, 0xffDCDCDA, 0xFFA349A4 },//color2
            { 0xFF000000, 0xFF20677D, 0xFFB9E72D, 0xFF000000, 0xFFB9E72D, 0xFFEEEEEE, 0xcc000000, 0xFFFF0000, 0xff666666, 0xFFB9E72D },//color3
            { 0xFFF7F7F7, 0xFF20677D, 0xFFB9E72D, 0xFF000000, 0xFFB9E72D, 0xFF000000, 0xcc000000, 0xFFFF0000, 0xffDCDCDA, 0xFFB9E72D },//color4
            { 0xFF000000, 0xFF38C0F4, 0xFF6F88A7, 0xFFFFFFFF, 0xFF00A2E8, 0xFFEEEEEE, 0xcc000000, 0xFFFF0000, 0xff666666, 0xFF6F88A7 },//color5
            { 0xFFFFFAEC, 0xFFFFF0AF, 0xFFDC5A1E, 0xFFFFFFFF, 0xFF888888, 0xFF888888, 0xcc000000, 0xFFFF0000, 0xffDCDCDA, 0xFFDC5A1E },//custom
    };
    public static String themeColor2[][]={
     /*  bg   */  /*bgfocus*/ /*bgtitle*/ /*txtitle*/ /*text1*/     /*text2*/  /*Qulink*/  /*stick*/ /*bg1*/
            { "F5F5FF", "38C0F4", "6A7BB0", "000000", "6A7BB0", "000000", "000000", "FF0000", "DCDCDA" },//voz
            { "000000", "20677D", "6F88A7", "FFFFFF", "33B5E5", "EEEEEE", "000000", "FF0000", "1B1B1B" },//black
            { "FFFAEC", "FFF0AF", "DC5A1E", "FFFFFF", "A87940", "000000", "000000", "FF0000", "DCDCDA" },//wood
            { "000000", "20677D", "A349A4", "000000", "A349A4", "EEEEEE", "000000", "FF0000", "666666" },//color1
            { "F7F7F7", "20677D", "A349A4", "000000", "A349A4", "000000", "000000", "FF0000", "DCDCDA" },//color2
            { "000000", "20677D", "B9E72D", "000000", "B9E72D", "EEEEEE", "000000", "FF0000", "666666" },//color3
            { "F7F7F7", "20677D", "B9E72D", "000000", "B9E72D", "000000", "000000", "FF0000", "DCDCDA" },//color4
            { "000000", "38C0F4", "6F88A7", "FFFFFF", "00A2E8", "EEEEEE", "000000", "FF0000", "666666" },//color5
            { "FFFAEC", "FFF0AF", "DC5A1E", "FFFFFF", "888888", "888888", "000000", "FF0000", "DCDCDA" },//custom
    };
    public static int ADS_ADMOD = 1; //full admob
    public static int ADS_AIRPUSH = 2; //full airpush
    public static int ADS_ADMOD_AIRPUSH = 3;  // admod > airpush
    public static int ADS_AIRPUSH_ADMOD = 4; // airpush > admod
    public static int ADS_ADMOD_AIRPUSH2 = 5;  // admod > airpush
    public static int ADS_AIRPUSH_ADMOD2 = 6; // airpush > admod

    public static int mCusThemeBg;
    public static int mCusThemeBgFocus;
    public static int mCusThemeTitleBg;
    public static int mCusThemeTxtTitle;
    public static int mCusThemeTxt1;
    public static int mCusThemeTxt2;
    public static int mCusThemeQuicklink;

    public static final int bg = 0;
    public static final int bgfocus = 1;
    public static final int bgtitle = 2;
    public static final int txtitle = 3;
    public static final int txColor1 = 4;
    public static final int txColor2 = 5;
    public static final int quicklink = 6;
    public static final int stick = 7;
    public static final int bgmain = 8;
    public static final int bgstatus = 9;

    public static final int TASK_GETDOC = 0;
    public static final int TASK_LOGIN = 1;
    public static final int TASK_LOGOUT = 2;
    public static final int TASK_GETQUOTE = 3;
    public static final int TASK_POSTREPLY = 4;
    public static final int TASK_POSTEDIT = 5;
    public static final int TASK_POSTDELETE = 6;
    public static final int TASK_SUBSCRIBE = 7;
    public static final int TASK_UNSUBSCRIBE= 8;
    public static final int TASK_NEWTHREAD= 9;
    public static final int TASK_SEARCHQUOTE= 10;
    public static final int TASK_RECENTPOST= 11;
    public static final int TASK_RECENTTHREAD= 12;
    public static final int TASK_ADVANCESEARCH= 13;
    public static final int TASK_SEARCH = 14;
    public static final int TASK_PM = 15;
    public static final int TASK_PMREPLY = 16;
    public static final int TASK_PMNEW = 17;

    public static final int REQUEST_REPLY = 0;
    public static final int REQUEST_QUOTE = 1;
    public static final int REQUEST_EDIT = 2;
    public static final int REQUEST_DELETE =3;
    public static final int REQUEST_INTENTFILTER =4;
    public static final int REQUEST_NEWTHREAD =5;
    public static final int REQUEST_PMINSERT =6;
    public static final int REQUEST_PMNEW =7;
    public static final int REQUEST_CHOOSE_AN_IMAGE=8;


    public static final int GO_INDEXPAGE = 0;
    public static final int GO_REFRESH = 1;
    public static final int GO_NEXT = 2;
    public static final int GO_PREVIOUS = 3;
    public static final int GO_FIRST = 4;
    public static final int GO_LAST = 5;
    public static final double FONTSCALE_MIN = 0.2;
    public static final double FONTSCALE_MAX = 2.5;
    public static final int REDUCE1 = 1;
    public static final int REDUCE2 = 2;
    public static final int REDUCE4 = 4;
    public static final int REDUCE8 = 8;
    public static final int REDUCENONE = 0;

    public static final int THEME_VOZ = 0;
    public static final int THEME_BLACK = 1;
    public static final int THEME_WOOD = 2;
    public static final int THEME_COLOR1 = 3;
    public static final int THEME_COLOR2 = 4;
    public static final int THEME_COLOR3 = 5;
    public static final int THEME_COLOR4 = 6;
    public static final int THEME_COLOR5 = 7;
    public static final int THEME_CUSTOM = 8;

    public static final int PAGE_1 = 1;
    public static final int PAGE_2 = 2;
    public static final int PAGE_3 = 3;
    public static final int PAGE_CP = 4;
    public static final int PAGE_BOOKMARK = 5;
    public static final int PAGE_QUOTE = 7;
    public static final int PAGE_RECENTPOST = 8;
    public static final int PAGE_RECENTTHREAD = 9;
    public static final int PAGE_PMDETAIL = 11;
    public static final int PAGE_NEWPOST = 12;
    public static final int PAGE_SEARCH = 13;
    public static final int PAGE_3HTML = 14;

    public static final int FONT_MAX = 25;
    public static final String HISTORY_FILE="history.json";
    public static int width;
    public static int height;
    public static boolean bVibrate;
    public static boolean bFullScreen;
    public static boolean bTopicHeader;
    public static int iSizeImage;
    public static int iHome;
    public static int iTheme;
    public static int iNumQuote ;
    public static boolean bNotifQuote;
    public static boolean bNotifSubscribe;
    public static boolean bEffect;
    public static boolean bSign;
    public static float iSize;
    public static boolean bSwipe;
    public static boolean bScrolling;
    public static boolean bClickAd;
    public static boolean bIconRefresh;
    public static int iNotifminutes;
    public static float iDensity;

    public static String sYourDevice;
    public static String iNumQuickLink;
    public static String mSavePath;



    public static boolean bDevider;


    public static void setColorBackground(View view,int colorNormal, int colorFocus){
        StateListDrawable states = new StateListDrawable();
        ColorDrawable clrBase = new ColorDrawable(colorNormal);
        ColorDrawable clrSelect = new ColorDrawable(colorFocus);
        states.addState(new int[] {-android.R.attr.state_enabled}, clrBase);
        states.addState(new int[] {android.R.attr.state_active}, clrBase);
        states.addState(new int[] {android.R.attr.state_selected}, clrSelect);
        states.addState(new int[]{android.R.attr.state_pressed}, clrSelect);
        if(Build.VERSION.SDK_INT >= 16){
            view.setBackground(states);
        }else{
            view.setBackgroundDrawable(states);
        }
    }

    public static void setBackgroundMenuLogo(View view){
        setColorBackground(view,themeColor[iTheme][bgtitle],themeColor[iTheme][bgfocus]);
    }

    public static void setBackgroundItemThread(View view){
        setColorBackground(view,themeColor[iTheme][bg],themeColor[iTheme][bgfocus]);
    }

    public static void setBackgroundQuickLink(View view){
        setColorBackground(view,themeColor[iTheme][quicklink],themeColor[iTheme][bgfocus]);
    }
    public static void setBackgroundQuickAction(View view){
        setColorBackground(view,themeColor[iTheme][bg],themeColor[iTheme][bgfocus]);
    }
    public static void setBackgroundItemThreadMultiQuote(View view){
        setColorBackground(view,themeColor[iTheme][bgfocus],themeColor[iTheme][bgfocus]);
    }


    public static void setTextSticky(TextView view){
        view.setTextColor(themeColor[iTheme][stick]);
    }
    public static void setTextContain(TextView view){
        view.setTextColor(themeColor[iTheme][txColor2]);
    }
    public static void setTextMenuTitle(TextView view){
        view.setTextColor(themeColor[iTheme][txtitle]);
    }
    public static void setTextColor1(TextView view){
        view.setTextColor(themeColor[iTheme][txColor1]);
    }

    public static int getTextClolor2(){
        return themeColor[iTheme][txColor2];
    }

    public static void setTextColor2(TextView view){
        view.setTextColor(themeColor[iTheme][txColor2]);
    }
    public static void setBackgoundMain(View view){
        setColorBackground(view,themeColor[iTheme][bgmain],themeColor[iTheme][bgmain]);
    }
}
