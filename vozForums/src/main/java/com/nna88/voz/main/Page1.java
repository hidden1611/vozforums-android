package com.nna88.voz.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.nna88.voz.contain.Forum;
import com.nna88.voz.listview.listViewCustom1;
import com.nna88.voz.util.ChangeLog;
import com.startapp.android.publish.StartAppSDK;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;

public class Page1 extends Page{
	private ArrayList<Forum> ListContains;
	private listViewCustom1 adapter;
	private StringBuilder sForum = new StringBuilder();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_1;
		StartAppSDK.init(this, "107784714", "208186269", false);
		super.onCreate(savedInstanceState);
//        cl = new ChangeLog(this);
//        if (cl.firstRun())            cl.getLogDialog().show();

		ListContains = new ArrayList<Forum>();
		adapter = new listViewCustom1(mContext,ListContains);
	    adapter.setSize(mTextSize);
	    mObjectAdapter = adapter;
        if( getIntent().getBooleanExtra("Exit me", false)){
            finish();
            return; // add this to prevent from doing unnecessary stuffs
        }
//        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter,mList);
//        if(Global.bEffect)  mList.setAdapter(swingBottomInAnimationAdapter);
        else mList.setAdapter(adapter);
		Uri data = getIntent().getData();
		if(data!= null){
			String url = data.toString();
			if(url.contains("forumdisplay.php")){
				Intent i = new Intent(mContext, Page2.class);
				i.putExtra("URL",url);
				i.putExtra("TITLE", "");
				startActivityForResult(i, Global.REQUEST_INTENTFILTER);
				overridePendingTransition(R.anim.rail,R.anim.rail);
			}else if(url.contains("showthread.php") || url.contains("showpost.php")){
				Intent i = new Intent(mContext, Page3.class);
				i.putExtra("URL", url);
				i.putExtra("TITLE","");
				startActivityForResult(i, Global.REQUEST_INTENTFILTER);
				overridePendingTransition(R.anim.rail,R.anim.rail);
			}else if(url.contains("index.php")){
				mTask.execute(Global.TASK_GETDOC);
			}else if(url.equals("https://vozforums.com")){
				mTask.execute(Global.TASK_GETDOC);
			}else if(url.equals("http://www.vozforums.com")){
				mTask.execute(Global.TASK_GETDOC);
			}else{
				toast("link error:"+url);
				finish();
			}

		}else{

			mParser.setUrl("https://vozforums.com");
			mTask.execute(Global.TASK_GETDOC);
//            Intent i = new Intent(mContext, Page3.class);
//            i.putExtra("URL","https://vozforums.com/showthread.php?t=4353065");
//            i.putExtra("TITLE", "");
//            startActivityForResult(i, Global.REQUEST_INTENTFILTER);
//            overridePendingTransition(R.anim.rail, R.anim.rail);
//            if(Global.iHome == 0){
//				Intent i = new Intent(mContext, Page3.class);
//				i.putExtra("URL","https://vozforums.com/showthread.php?t=4230225");
//				i.putExtra("TITLE", "");
//				startActivityForResult(i, Global.REQUEST_INTENTFILTER);
//				overridePendingTransition(R.anim.rail, R.anim.rail);
//            }else{
//                Intent i = new Intent(mContext, Page2.class);
//                i.putExtra("URL","https://vozforums.com/forumdisplay.php?f="+Global.iHome);
//                i.putExtra("TITLE", "");
//                startActivityForResult(i, Global.REQUEST_INTENTFILTER);
//                overridePendingTransition(R.anim.rail,R.anim.rail);
//            }

        }
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if(ListContains.get(pos).UrlForum() == null) return;
                Intent i = new Intent(mContext, Page2.class);
                i.putExtra("URL", ListContains.get(pos).UrlForum());
                i.putExtra("TITLE", ListContains.get(pos).Forum());
                startActivity(i);
                overridePendingTransition(R.anim.rail,R.anim.rail);
				vibrate();

            }

        });
		loadAds();
    }
    	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Global.REQUEST_INTENTFILTER){
			finish();
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	@Override
	protected void TaskCancle() {
		// TODO Auto-generated method stub
		super.TaskCancle();
        if(mTask != null) mTask.cancel(true);
		ListContains.clear();
		if(adapter != null) adapter.notifyDataSetChanged();
	}
	@Override
	void Parser(int taskType) throws Exception {
		// TODO Auto-generated method stub
		//log("execute finish");
		super.Parser(taskType);
		if(doc==null) return;
//        write(doc.toString());
        scrollIsComputed = false;
		ListContains.clear();
		String forum = null,urlForum=null, lastpost = null;
		Element eleLogin = doc.select("a[href=private.php]").first();
		if(eleLogin != null){
			String temp = doc.select("td[class=alt2]").get(0).select("a[href*=mem]").attr("href");
			temp = temp.split("=")[1];
			mUser.setUserId(temp);
            Element eleSecurity = doc.select("input[name*=securitytoken]").first();
            if(eleSecurity!=null){
                String secToken;
                secToken = eleSecurity.attr("value");
                mUser.setToken(secToken);
            }
		}
		Elements eleTag1s = doc.select("td[class*=tcat]");
        Iterator<Element> iteTag1 = eleTag1s.iterator();
		while(iteTag1.hasNext()){
			Element eleTag1 = iteTag1.next();
			if(eleTag1.text().contains("Welcome to the vozForums")) continue;
			ListContains.add(new Forum(eleTag1.text(),eleTag1.select("[href~=forum]").attr("href"),null));
			Element eleTag2 = eleTag1.parent().parent().nextElementSibling();
			if(eleTag2 != null){
				Elements eleTag3s = eleTag2.select("tr");
				Iterator<Element> iteTag3 = eleTag3s.iterator();

				while(iteTag3.hasNext()){
                    Element ele = iteTag3.next().select("div:has(a[href*=forumdisplay]").first();
                    if(ele != null){
                        forum = ele.select("a").text();
                        urlForum = ele.select("a").attr("href");
                        lastpost = ele.select("span").text();
                    }
                    ListContains.add(new Forum(forum,urlForum,lastpost));
                    sForum.append(forum).append(",").append(urlForum).append(";");

				}
			}
		}

        if(ListContains.size() < 2){
            parserNotification();
            return;
        }
		writeStringForums(sForum.toString());

        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
        adapter.notifyDataSetChanged();
    }
	

}
