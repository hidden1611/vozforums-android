package com.nna88.voz.main;

import android.os.AsyncTask;
import android.util.Log;

import com.nna88.voz.util.UserInfo;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


/**
 * Created by nguyen on 8/3/13.
 */
public class MyNetwork {
    public static final int TIMEOUT = 5000;
    UserInfo mUser = new UserInfo();
    private Task mTask;
    private Document doc;
    public String url;
    private OnResult mResultListener;
    public interface OnResult {
        public abstract void onResultListenter(Document doc, int status);
    }

    public MyNetwork(){
        mTask = new Task();
        mUser = new UserInfo();
    }
    public void getDoc(String murl,OnResult listenter){
        url = murl;
        mTask.cancel(true);
        mTask = new Task();
        mResultListener = listenter;
        mTask.execute(0);

    }
    public void PostReplyQuoteCatcha(Document mDoc,OnResult listenter,final String message,
                                     final String humanverify,  final String recaptcha_challenge_field,final String recaptcha_response_field){
        mTask.cancel(true);
        mTask = new Task();
        mResultListener = listenter;
        doc = mDoc;
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                String mUrl;
                Element eleUrl = doc.select("form[action*=newreply.php?do=postreply]").first();
                Element eleSecurity = doc.select("input[name*=securitytoken]").first();
                Element elePostHash = doc.select("input[name*=posthash]").first();
                Element elePostStartTime = doc.select("input[name*=poststarttime]").first();
                Element eleTitle = doc.select("input[name=title]").first();
                Element eleP = doc.select("input[name=p]").first();
                mUser.setToken(eleSecurity.attr("value"));
                mUrl = eleUrl.attr("action");
                mUrl = mUrl.replace("&amp;", "&");
                mUrl = "https://vozforums.com/" + mUrl;
                url = mUrl;
                final String postHash = elePostHash.attr("value");
                final String postStartTime = elePostStartTime.attr("value");
                final String p = eleP.attr("value");
                final String title = eleTitle.attr("value");
                Connection.Response res;
                try {
                    res =  Jsoup.connect(url)
                            .timeout(TIMEOUT)
                            .cookies(mUser.cookies())
                            .data("title:Re: ", title)
                            .data("message", message)
                            .data("wysiwyg", "0")
                            .data("humanverify[hash]", humanverify)
                            .data("recaptcha_challenge_field", recaptcha_challenge_field)
                            .data("recaptcha_response_field", recaptcha_response_field)
                            .data("s", " ")
                            .data("securitytoken", mUser.Token())
                            .data("do", "postreply")
                            .data("t", url.split("=")[2])
                            .data("p", p)
                            .data("specifiedpost", "1")
                            .data("posthash", postHash)
                            .data("poststarttime", postStartTime)
                            .data("loggedinuser", mUser.UserId())
                            .data("multiquoteempty:", " ")
                            .data("sbutton", "Submit Reply")
                            .data("parseurl", "1")
                            .data("rating", "0")
                            .method(Connection.Method.POST)
                            .execute();
                    url = res.url().toString();
                    doc = res.parse();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(mResultListener != null){
                    mResultListener.onResultListenter(doc,0);
                }
            }
        }.execute();
    }

    private class Task extends AsyncTask<Integer,Void,Void>{

        @Override
        protected Void doInBackground(Integer... params) {
            if(params[0]==0){
                Connection.Response res;
                try {

                    if(mUser.cookies() == null){
                        Log.d("nna", "cookies = null");
                        res = Jsoup.connect(url).timeout(TIMEOUT).execute();
                    }else{
                        Log.d("nna", "cookies != null" + mUser.cookies().containsKey("vbulletin_multiquote") + mUser.getMultiquoteId());
                        res = Jsoup.connect(url).timeout(TIMEOUT)
                                .header("Cookie",mUser.getCookieString())
//                                .cookies(mUser.cookies())
                                .execute();
                    }

                    if(res != null)      doc = res.parse();
                    if(doc != null) url = doc.baseUri();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(mResultListener != null){
                mResultListener.onResultListenter(doc,0);
            }
        }
    }
}
