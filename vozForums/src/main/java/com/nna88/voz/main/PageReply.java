package com.nna88.voz.main;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nna88.voz.contain.containEmo;
import com.nna88.voz.listview.GridAdapter;
import com.nna88.voz.listview.listViewEmo;
import com.nna88.voz.util.HorizontialListView;


public class PageReply extends Activity {
	private HorizontialListView mListEmo;
	private EditText m_TextPost;
	private Button m_ButPost;
	private Button m_ButPostCancle;
	private ImageButton m_ButPostEmo;
	private ArrayList<containEmo> listEmo;
    private AlertDialog.Builder alert;
    private View textEntryView;
    private LayoutInflater factory;
    private EditText edit1 ;
    private EditText edit2 ;
    private TextView text1 ;
    private TextView text2 ;
    private Context mContext;
    private EditText textTitle;
    private RelativeLayout layout;
    private int iPostType;
    private String sSignature ;
    private GridView gridview ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pagereply);
		super.onCreate(savedInstanceState);
		sSignature = "[I]Sent from my "
				+ Global.sYourDevice 
				+ " using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]";
		mContext =this;
		iPostType = getIntent().getIntExtra("POSTTYPE", -1);
		textTitle = (EditText)findViewById(R.id.title);
		textTitle.setText(getIntent().getExtras().getString("title"));
		textTitle.setEnabled(false);
		mListEmo = (HorizontialListView)findViewById(R.id.Page3_emo);
		m_TextPost = (EditText)findViewById(R.id.Page3_postReply);
		m_TextPost.requestFocus();
		m_TextPost.setFocusable(true);
		m_ButPost = (Button)findViewById(R.id.Page3_butReply);
		m_ButPostEmo = (ImageButton)findViewById(R.id.Page3_butReplyEmo);
		m_ButPostCancle = (Button)findViewById(R.id.Page3_butReplyCancle);
		mListEmo.setOnItemClickListener(mEmoListClick);
		m_ButPost.setOnClickListener(mPostReplyClick);
		m_ButPostCancle.setOnClickListener(mPostCancleClick);
		m_ButPostEmo.setOnClickListener(mPostEmoClick);
		listEmo = new ArrayList<containEmo>();
		listEmo = Emo2();
		listViewEmo adapter2 = new listViewEmo(PageReply.this,listEmo);
	    mListEmo.setAdapter(adapter2);
	    m_TextPost.setText(getMesseage());
		m_TextPost.setSelection(m_TextPost.getText().length());
		mListEmo.setVisibility(View.GONE);
		layout = (RelativeLayout)findViewById(R.id.layout_pagerepley);

		gridview = (GridView)findViewById(R.id.grid_view);
		GridAdapter adapter = new GridAdapter(mContext, listEmo); 
		gridview.setAdapter(adapter);
		gridview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
            	mHandler.obtainMessage(20,listEmo.get(position).text()).sendToTarget();
            }
        });
		//Global.setBackgroundItemThread(layout);
		//Global.setBackgroundItemThreadMultiQuote(layout);
		//gridview.setBackgroundResource(R.drawable.my_image);
		
		Global.setBackgroundItemThread(m_TextPost);
		//Global.setBackgroundItemThread(gridview);
		Global.setTextContain(m_TextPost);
		Global.setBackgroundMenuLogo(textTitle);
		Global.setTextMenuTitle(textTitle);
		gridview.requestLayout();
		adapter.notifyDataSetChanged();
		
	}
	private String getMesseage(){
		String msg = getIntent().getExtras().getString("msg");
		if(iPostType == Global.REQUEST_EDIT){
			if(msg.lastIndexOf(sSignature) != -1){
				msg = msg.substring(0, msg.lastIndexOf(sSignature));
			}else{
				int start, end;
				start = msg.lastIndexOf("[I]Sent from my ");
				end = msg.lastIndexOf("[URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL][/I]");
				if((start != -1) && (end != -1)){
					msg = msg.substring(0, start);
				}
			}
		}
		return msg;
	}
	private ArrayList<containEmo> Emo2(){
		ArrayList<containEmo> listEmo = new ArrayList<containEmo>();
		try {
			String list[] = mContext.getAssets().list("images/smilies/Off");
			String temp;
			Bitmap emo;
			InputStream is ;
			for(int i=0;i<list.length;i=i+1){
				temp = list[i].substring(0,list[i].length()-4);
				//Log.d("nna",temp);
				if(temp.equals("brick"))
					is  = mContext.getAssets().open("images/smilies/Off/"+temp+".png");
				else 
					is  = mContext.getAssets().open("images/smilies/Off/"+temp+".gif");
				emo = BitmapFactory.decodeStream(is);
				
				if(temp.equals("embarrassed")) 				temp = ":\">";
				else if(temp.equals("nosebleed"))			temp = ":chaymau:";
				else if(temp.equals("feel_good"))			temp = ":sogood:";
				else if(temp.equals("sexy_girl"))			temp = ":sexy:";
				else if(temp.equals("look_down"))			temp = ":look_down:";
				else if(temp.equals("beat_brick"))			temp = "brick";
				else if(temp.equals("cry"))					temp = ":((";
				else if(temp.equals("beat_plaster"))		temp = ":-s";
				else if(temp.equals("confuse"))				temp = ":chaymau:";
				else if(temp.equals("big_smile"))			temp = ":D";
				else if(temp.equals("brick"))				temp = ":gach:";
				else if(temp.equals("burn_joss_stick"))		temp = ":stick:";
				else if(temp.equals("bad_smell"))			temp = ":badsmell:";
				else if(temp.equals("cool"))				temp = ":kool:";
				else if(temp.equals("after_boom"))			temp = ":aboom:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("sweet_kiss"))			temp = ":*";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("smile"))				temp = ":)";
				else if(temp.equals("still_dreaming"))		temp = ":dreaming:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else if(temp.equals("beat_shot"))			temp = ":shot:";
				else 										temp = ":"+temp+":";
				listEmo.add(new containEmo(emo, temp));
				is.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listEmo;
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
//		InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputMethodManager.hideSoftInputFromInputMethod(null, 0);
	}
	private void setPostVisible(int type) {
		m_ButPost.setVisibility(type);
		m_ButPostCancle.setVisibility(type);
		m_ButPostEmo.setVisibility(type);
		m_TextPost.setVisibility(type);
		
	}
	public final Handler mHandler = new Handler(){
    	@Override
    	public void handleMessage(Message msg) {
    		 switch (msg.what) {
    		 case 20:
    			int start =m_TextPost.getSelectionStart();
 				m_TextPost.getText().insert(start,(String) msg.obj);
     			break;	
    		 }
    	};
	};
	OnItemClickListener mEmoListClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			mHandler.obtainMessage(20,listEmo.get(arg2).text()).sendToTarget();
			
		}
		
	};
	private void toast(String ss){
		Toast.makeText(PageReply.this, ss, Toast.LENGTH_SHORT).show();
	}
	OnClickListener mPostReplyClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			setPostVisible(View.GONE);
			mListEmo.setVisibility(View.GONE);
			Intent i = new Intent();
			Bundle extras = new Bundle();
			String mss = m_TextPost.getText().toString();
			mss +="\n\n[I]Sent from my "+Global.sYourDevice +" using [URL=\"https://play.google.com/store/apps/details?id=com.nna88.voz.main\"]vozForums[/URL]"+ "[/I]\n";
			extras.putString("msg",mss);
 			i.putExtras(extras);
			setResult(RESULT_OK,i);
			finish();
		}
	};
	OnClickListener mPostCancleClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			setResult(RESULT_CANCELED);
            finish();
		}
	};
	private Dialog alertImage(){
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.go_address);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(dialog.getWindow().getAttributes());
	    lp.width = WindowManager.LayoutParams.FILL_PARENT;
	    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(lp);
		final AutoCompleteTextView edit1 = (AutoCompleteTextView)dialog.findViewById(R.id.alert_edit1);
		TextView textTitle = (TextView)dialog.findViewById(R.id.alert_title);
		Button buttonOK = (Button)dialog.findViewById(R.id.alert_ok);
		Button buttonCancle = (Button)dialog.findViewById(R.id.alert_cancle);
		textTitle.setText("Please enter the url of your link");
		buttonOK.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
				String temp = "[IMG]"+edit1.getText().toString()+"[/IMG]";
				m_TextPost.getText().insert(start,temp);
				dialog.dismiss();
			}
		});
		buttonCancle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
		return dialog;
	}
	OnClickListener mImageClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			alertImage();
		}
	};
	private Dialog alertLink(){
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.go_address);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(dialog.getWindow().getAttributes());
	    lp.width = WindowManager.LayoutParams.FILL_PARENT;
	    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(lp);
		final AutoCompleteTextView edit1 = (AutoCompleteTextView)dialog.findViewById(R.id.alert_edit1);
		TextView textTitle = (TextView)dialog.findViewById(R.id.alert_title);
		Button buttonOK = (Button)dialog.findViewById(R.id.alert_ok);
		Button buttonCancle = (Button)dialog.findViewById(R.id.alert_cancle);
		textTitle.setText("Please enter the url of your link");
		buttonOK.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
	    		String temp = "[URL=\""+edit1.getText().toString()+"\"]"+
	    				edit1.getText().toString()+"[/URL]";
				m_TextPost.getText().insert(start,temp);
				dialog.dismiss();
			}
		});
		buttonCancle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
		return dialog;
	}
	OnClickListener mLinkClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			alertLink();
		}
	};
	OnClickListener mBoldClick = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
				m_TextPost.getText().insert(start, "[B][/B]");
			}
		};
	OnClickListener mItalicClick = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
				m_TextPost.getText().insert(start, "[I][/I]");
	
			}
		};
	OnClickListener mUnderClick = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int start =m_TextPost.getSelectionStart();
				m_TextPost.getText().insert(start, "[U][/U]");
	
			}
		};
	OnClickListener mPostEmoClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(gridview.getVisibility() != View.VISIBLE){ 
				//PageReply.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				gridview.setVisibility(View.VISIBLE);
			}else{
				//PageReply.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				gridview.setVisibility(View.GONE);
			}
		}
	};
	
	
	private void alertDialog(){
	    factory = LayoutInflater.from(PageReply.this);
	    textEntryView = factory.inflate(R.layout.login, null);    
	    edit1 = (EditText)textEntryView.findViewById(R.id.alert_edit1);
	    edit2 = (EditText)textEntryView.findViewById(R.id.alert_edit2);
	    text1 = (TextView)textEntryView.findViewById(R.id.alert_txt1);
	    text2 = (TextView)textEntryView.findViewById(R.id.alert_txt2);
	    alert = new AlertDialog.Builder(PageReply.this);
	    alert.setView(textEntryView);
    }
	 private void alertInsert(){
	    	edit2.setVisibility(View.GONE);
	    	text2.setVisibility(View.GONE);
	    	text1.setVisibility(View.GONE);
	    	edit1.setText("http://");
	    	edit1.setSelection(edit1.getText().length());
	    }
}
