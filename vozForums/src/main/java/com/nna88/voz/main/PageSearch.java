package com.nna88.voz.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PageSearch extends Page{
	private ArrayList<Thread> ListContains;
	private listViewCustom2 adapter;
	List<Comment> lComment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_SEARCH;
		super.onCreate(savedInstanceState);
		ListContains = new ArrayList<Thread>();
		adapter = new listViewCustom2(mContext,ListContains);
	    adapter.setSize(mTextSize);
	    mObjectAdapter = adapter;
	    mList.setAdapter(adapter);
        //_needfix
//	    mMenu.showContent();

		sMessSearch = getIntent().getStringExtra("MESS");
		sForum = getIntent().getStringExtra("FORUM");
		sSearch_ShowPost = getIntent().getStringExtra("SHOWPOST");
	   if(sForum!=null)
		   mTask.execute(Global.TASK_ADVANCESEARCH);
//           myVolley.advanceSearch(mUser,sMessSearch,sForum , sSearch_ShowPost);
	   else
		   mTask.execute(Global.TASK_SEARCH);
        //needfix
//           myVolley.search(mUser, sMessSearch, sSearch_ShowPost);
//	    mMenuTitle.setText("Search");
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                try{
                    Intent i = null;
                    vibrate();
                    if (ListContains.get(pos).isUrl() == 0) { // forum
                        i = new Intent(mContext, Page2.class);
                        if(ListContains.get(pos).UrlThread() == null)		return;
                        i.putExtra("URL", ListContains.get(pos).UrlThread());

                    } else if (ListContains.get(pos).isUrl() == 1){									//thread
                        i = new Intent(mContext, Page3.class);
                        if((ListContains.get(pos).UrlLastPosst() != null) &&
                                (!ListContains.get(pos).UrlLastPosst().equals(""))	){
                            if(ListContains.get(pos).UrlLastPosst() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
                        }else{
                            if(ListContains.get(pos).UrlThread() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlThread());
                        }
                    }else
                        return;

                    i.putExtra("TITLE", ListContains.get(pos).Thread());
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                alertLongClick(pos);
                vibrate();
                return false;
            }
        });
//	    adapter.setOnActionItemClickListener(new OnActionItemClickListener() {
//			@Override
//			public void onItemClick(int pos, int actionId) {
//				// TODO Auto-generated method stub
//				if(actionId==1){
//					Intent i = null;
//					if (ListContains.get(pos).isUrl() == 0) { // forum
//						i = new Intent(mContext, Page2.class);
//						if(ListContains.get(pos).UrlThread() == null)		return;
//						i.putExtra("URL", ListContains.get(pos).UrlThread());
//
//					} else if (ListContains.get(pos).isUrl() == 1){									//thread
//						i = new Intent(mContext, Page3New.class);
//						if(ListContains.get(pos).UrlLastPosst() != null){
//							if(ListContains.get(pos).UrlLastPosst() == null)		return;
//							i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
//						}else{
//							if(ListContains.get(pos).UrlThread() == null)		return;
//							i.putExtra("URL", ListContains.get(pos).UrlThread());
//						}
//					}else
//						return;
//
//					i.putExtra("TITLE", ListContains.get(pos).Thread());
//					startActivity(i);
//					overridePendingTransition(R.anim.rail, R.anim.rail);
//				}else {
//					alertLongClick(pos);
//				}
//			}
//		});
	    lComment = mDataBookmark.getAllComments();
	    
	}
    private Dialog alertLongClick(final int pos){

        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Please choose");
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pagelongclick);
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        TextView txFirstNews = (TextView)dialog.findViewById(R.id.firstnew);
        TextView txFirstPost = (TextView)dialog.findViewById(R.id.firstpost);
        TextView txLastPost = (TextView)dialog.findViewById(R.id.lastpost);

        final TextView txBookmark= (TextView)dialog.findViewById(R.id.bookmark);
        final Thread contain = ListContains.get(pos);
        if(contain.isBookmark)
            txBookmark.setText(getResources().getString(R.string.UnBookmark));
        else
            txBookmark.setText(getResources().getString(R.string.Bookmark));
        TextView txCopyLink = (TextView)dialog.findViewById(R.id.copylink);
        TextView txPrefix = (TextView)dialog.findViewById(R.id.Prefix);
        if(ListContains.get(pos).mPrefixLink != null){
            txPrefix.setVisibility(View.VISIBLE);
        }
        txPrefix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = null;
                i = new Intent(mContext, Page2.class);
                i.putExtra("URL", ListContains.get(pos).mPrefixLink);
                i.putExtra("TITLE","");
                startActivity(i);
                overridePendingTransition(R.anim.rail, R.anim.rail);
                dialog.dismiss();
            }
        });
        txCopyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int sdk = android.os.Build.VERSION.SDK_INT;
                String url = ListContains.get(pos).UrlThread();
                if(!url.contains("https://vozforums.com/"))
                    url = "https://vozforums.com/" + url;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(url);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",url);
                    clipboard.setPrimaryClip(clip);
                }
                dialog.dismiss();
            }
        });
        txFirstPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = null;
                i = new Intent(mContext, Page3.class);
                i.putExtra("URL", ListContains.get(pos).UrlThread());
                i.putExtra("TITLE","");
                startActivity(i);
                overridePendingTransition(R.anim.rail, R.anim.rail);
                dialog.dismiss();
            }
        });
        txFirstNews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (ListContains.get(pos).UrlLastPosst() == null) {
                    toast("Không co link");
                } else {
                    Intent i = null;
                    i = new Intent(mContext, Page3.class);
                    i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
                    i.putExtra("TITLE", ListContains.get(pos).UrlLastPosst());
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                    dialog.dismiss();
                }

            }
        });
        txLastPost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (ListContains.get(pos).getUrlLastLast() == null) {
                    toast("Không có link");
                } else {
                    Intent i = null;
                    i = new Intent(mContext, Page3.class);
                    i.putExtra("URL", ListContains.get(pos).getUrlLastLast());
                    i.putExtra("TITLE", "");
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                    dialog.dismiss();
                }
            }
        });

        txBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(contain.isBookmark){
                    contain.isBookmark = false;
                    mDataBookmark.deleteBookmark(contain);
                }else{
                    contain.isBookmark = true;
                    mDataBookmark.addBookmark(contain);

                }
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	void Parser(int taskType) throws Exception {
		// TODO Auto-generated method stub
		super.Parser(taskType);
		if(doc==null) return;
		Thread contain;
		ListContains.clear();
		Bitmap bitmap = null;
		String text1 = null,text2="",text3=null,text4=null,text1_url1=null,text2_url2=null,url_lastpost=null;
        setPage(parsePage(Global.GO_INDEXPAGE,0));

        if(sSearch_ShowPost.equals("0")){
			Iterator<Element> ite = doc.select("tr:has(td[id*=td_threadstatusicon])").iterator();
			Element eleTag1;
			while(ite.hasNext()){
				ite.hasNext();
				eleTag1 = ite.next();
				text1 = "";text2 = "";
				if( eleTag1.select("a[id*=thread_title]").first()!= null){
					text1 =  eleTag1.select("a[id*=thread_title]").first().text();
                    if(eleTag1.select("div:has(span[onclick*=member.php])").first() != null){
                        text1 += " " + mColorText2 + "'>- "+eleTag1.select("div:has(span[onclick*=member.php])").first().text();
                    }
					text1_url1 = eleTag1.select("a[id*=thread_title]").first().attr("href");
				}
				Elements temp = eleTag1.select("a[href*=forumdisplay]");
				if( temp.size() > 1){
					text2 = "Forums: " + temp.get(1).text()+"\n";
					text1 = temp.get(0).text()+" - "+text1;
				}else if(temp.size()==1){
					text2 = "Forums: " + temp.get(0).text()+"\n";
				}
				if( eleTag1.select("div[style*=text-align:right]").first()!= null){
					text2 += eleTag1.select("div[style*=text-align:right]").first().text() + "\n";
				}
				if( eleTag1.select("td[title*=Replies]").first()!= null){
					text2 += eleTag1.select("td[title*=Replies]").first().attr("title");
				}
                if(Global.bTopicHeader){
                    if(eleTag1.select("img[id*=thread_statusicon]").first() != null){
                        bitmap = loadBitmapAssert(eleTag1.select("img[id*=thread_statusicon]").first().attr("src"));
                    }
                }
                if(eleTag1.select("a:has(img[src*=last])").first() != null){
                    text2_url2 = eleTag1.select("a:has(img[src*=last])").attr("href");
                }
				contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
                if(eleTag1.select("a[id*=thread_gotonew]").first() != null){
                   contain.setUrlLastPost(eleTag1.select("a[id*=thread_gotonew]").attr("href"));
                }
                contain.setSticky(false);
                contain.mIdThread = text1_url1.split("t=")[1];
				ListContains.add(contain);
			}
		}else{
			Iterator<Element> ite = doc.select("table[id]").iterator();
			if(ite.hasNext())
				ite.next();
			Element eleTag1;
			
			while (ite.hasNext()) {
				eleTag1 = ite.next();
				Element eleForum= eleTag1.select("td[class=thead]").first();
				if( eleForum != null){
					text2 = "Forums: "+eleForum.select("a").first().text();
					text2 +="<br />"+ eleForum.ownText();
				}

				if (eleTag1.select("a[href*=showthread.php]").first() != null) {
					text1 = eleTag1.select("a[href*=showthread.php]").first() .text();
					text1_url1 = eleTag1.select("a[href*=showthread.php]") .first().attr("href");
				}
				if (eleTag1.select("a[href*=member.php]").first() != null) {
					// text2 += "<br />" + "Posted By "
					// +
					// "<font color='#0000FF'>"+eleTag1.select("a[href*=member.php]").first().text()+"</font>";
					// <font color='#EE0000'>red</font>
				}
				if (eleTag1.select("em").first() != null) {
					text2 += "<br />" + eleTag1.select("em").first().ownText();
					text1_url1 = eleTag1.select("em").first().select("a") .attr("href");
				}
                if(Global.bTopicHeader){
                    if(eleTag1.select("img[id*=thread_statusicon]").first() != null){
                        bitmap = loadBitmapAssert(eleTag1.select("img[id*=thread_statusicon]").first().attr("src"));
                    }
                }
				contain = new Thread(text1, text2, text3, text4, bitmap, text1_url1, text2_url2);
				ListContains.add(contain);
			}
		}
        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;

		adapter.notifyDataSetChanged();
        mList.clearFocus();
        mList.post(new Runnable() {
            @Override
            public void run() {
                mList.setSelection(0);
            }
        });
	}
		
	
}
