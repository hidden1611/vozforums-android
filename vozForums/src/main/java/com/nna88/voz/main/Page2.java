package com.nna88.voz.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Page2 extends Page{
    private ArrayList<Thread> ListContains;
    private listViewCustom2 adapter;
    private String mTextTitle;
    private String url;
    List<Comment> lComment;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        iPage=Global.PAGE_2;
        super.onCreate(savedInstanceState);
        ListContains = new ArrayList<Thread>();
        adapter = new listViewCustom2(mContext,ListContains);
        mObjectAdapter = adapter;
        adapter.setSize(mTextSize);
//        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter,mList);
//        if(Global.bEffect)  mList.setAdapter(swingBottomInAnimationAdapter);
        mList.setAdapter(adapter);
        mTextTitle = getIntent().getStringExtra("TITLE");
        setTitle(mTextTitle);
        url = getIntent().getStringExtra("URL");
        if(url.contains(Global.URL) || url.contains(Global.URL2))
            mParser.setUrl(getIntent().getStringExtra("URL"));
        else
            mParser.setUrl(Global.URL+getIntent().getStringExtra("URL"));
        mTask.execute(Global.TASK_GETDOC);
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                alertLongClick(pos);
                vibrate();
                return false;
            }
        });
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent i;
                vibrate();
                try{
                    if (ListContains.get(pos).isUrl() == 0) { // forum
                        i = new Intent(mContext, Page2.class);
                        if(ListContains.get(pos).UrlThread() == null)		return;
                        i.putExtra("URL", ListContains.get(pos).UrlThread());
                    } else if(ListContains.get(pos).isUrl() == 1){									//thread
                        i = new Intent(mContext, Page3.class);
                        if(ListContains.get(pos).UrlLastPosst() != null){
                            if(ListContains.get(pos).UrlLastPosst() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
                        }else{
                            if(ListContains.get(pos).UrlThread() == null)		return;
                            i.putExtra("URL", ListContains.get(pos).UrlThread());
                        }
                    }else
                        return;

                    i.putExtra("TITLE", ListContains.get(pos).Thread());
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        lComment = mDataBookmark.getAllComments();
        hideAds();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        mParser.setUrl(Global.URL+url);
        TaskCancle();
        mTask = new TaskGetHtml();
        mTask.execute(Global.TASK_GETDOC);
    }

    private boolean checkBookmark(String idthread){
        for(Comment comment:lComment){
            if(comment.idThread.equals(idthread)){
                return true;
            }
        }
        return false;
    }

    private void alertLongClick(final int pos){
		final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Please choose");
		dialog.setContentView(R.layout.pagelongclick);
		TextView txFirstNews = (TextView)dialog.findViewById(R.id.firstnew);
		TextView txFirstPost = (TextView)dialog.findViewById(R.id.firstpost);
		TextView txLastPost = (TextView)dialog.findViewById(R.id.lastpost);

		final TextView txBookmark= (TextView)dialog.findViewById(R.id.bookmark);
		final Thread contain = ListContains.get(pos);
		if(contain.isBookmark)
			txBookmark.setText(getResources().getString(R.string.UnBookmark));
		else
			txBookmark.setText(getResources().getString(R.string.Bookmark));
        TextView txCopyLink = (TextView)dialog.findViewById(R.id.copylink);

        TextView txPrefix = (TextView)dialog.findViewById(R.id.Prefix);
        if(ListContains.get(pos).mPrefixLink != null){
            txPrefix.setVisibility(View.VISIBLE);
        }
		txPrefix.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                Intent i = null;
                i = new Intent(mContext, Page2.class);
                i.putExtra("URL", ListContains.get(pos).mPrefixLink);
                i.putExtra("TITLE","");
                startActivity(i);
                overridePendingTransition(R.anim.rail, R.anim.rail);
                dialog.dismiss();
			}
		});
		txCopyLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                int sdk = android.os.Build.VERSION.SDK_INT;
                String url = ListContains.get(pos).UrlThread();
                if(!url.contains("https://vozforums.com/"))
                    url = "https://vozforums.com/" + url;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(url);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",url);
                    clipboard.setPrimaryClip(clip);
                }
				dialog.dismiss();
			}
		});
		txFirstPost.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = null;
				i = new Intent(mContext, Page3.class);
				i.putExtra("URL", ListContains.get(pos).UrlThread());
				i.putExtra("TITLE","");
				startActivity(i);
				overridePendingTransition(R.anim.rail, R.anim.rail);
				dialog.dismiss();
			}
		});
		txFirstNews.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (ListContains.get(pos).UrlLastPosst() == null) {
					toast("Không co link");
				} else {
					Intent i = null;
					i = new Intent(mContext, Page3.class);
					i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
					i.putExtra("TITLE", ListContains.get(pos).UrlLastPosst());
					startActivity(i);
					overridePendingTransition(R.anim.rail, R.anim.rail);
					dialog.dismiss();
				}

			}
		});
		txLastPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (ListContains.get(pos).getUrlLastLast() == null) {
					toast("Không có link");
				} else {
					Intent i = null;
					i = new Intent(mContext, Page3.class);
					i.putExtra("URL", ListContains.get(pos).getUrlLastLast());
					i.putExtra("TITLE", "");
					startActivity(i);
					overridePendingTransition(R.anim.rail, R.anim.rail);
					dialog.dismiss();
				}
			}
		});

		txBookmark.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(contain.isBookmark){
					contain.isBookmark = false;
					mDataBookmark.deleteBookmark(contain);
				}else{
					contain.isBookmark = true;
					mDataBookmark.addBookmark(contain);

				}
				dialog.dismiss();
			}
		});
		dialog.show();
//		return dialog;
    }
    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        updateSetFont(mTextSize);
    }
    @Override
    protected void updateSetFont(float value) {
        // TODO Auto-generated method stub
        super.updateSetFont(value);
        adapter.setSize(value);
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void TaskCancle() {
        // TODO Auto-generated method stub
        super.TaskCancle();
        ListContains.clear();
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void GoPage(int state, int numPage) throws Exception {
        // TODO Auto-generated method stub

        super.GoPage(state, numPage);
    }

    @Override
    void Parser(int taskType) throws Exception {
        // TODO Auto-generated method stub
        super.Parser(taskType);
        if(doc==null) return;
        ListContains.clear();
        Element eleTag1,eleTag1Image ,eleTag1Time ,eleTag1Replies,eleTag1Views ;
        Element eleLogin = doc.select("a[href=private.php]").first();
        setPage(parsePage(Global.GO_INDEXPAGE,0));
        if(eleLogin != null){
            String temp = doc.select("td[class=alt2]").get(0).select("a[href*=mem]").attr("href");
            temp = temp.split("=")[1];
            mUser.setUserId(temp);
            Element eleSecurity = doc.select("input[name*=securitytoken]").first();
            if(eleSecurity!=null){
                String secToken;
                secToken = eleSecurity.attr("value");
                mUser.setToken(secToken);
            }
        }
        Bitmap bitmap = null;
        String urlImage,text1 = null,text2=null,text3=null,text4=null,text1_url1=null,text2_url2=null,text3_url=null,mPrefixUrl=null;
        Element mainElement = doc.select("tbody:has(form[id])").first();
        if(mainElement == null){
            toast("main Element null");
            parserNotification();
            return;
        }

        Element eleTitle= mainElement.select("td[class=tcat]:contains(Threads in Forum)").first();
        if(eleTitle!= null){
            mTitle = eleTitle.select("span").text();
            mTitle = mTitle.substring(2, mTitle.length());
            setTitle(mTitle);
        }
        Element eletbody = mainElement.select("tbody[id*=threadbits_forum]").first();
        if(eletbody == null){
            toast("elebody null");
            return;
        }
        Elements eleTag1s = eletbody.select("tr:has(td[id*=td_threads])");
        Elements eleTag2s = mainElement.select("tr > td[class=alt1Active]");
        Element eleText1;
        Iterator<Element> iteTag1 = eleTag1s.iterator();
        Iterator<Element> iteTag2 = eleTag2s.iterator();
        Thread contain;
        if(iteTag2.hasNext())	ListContains.add(new Thread("Sub-Forum",null,null,null,null,null,null));
        while(iteTag2.hasNext()){
            eleTag1 = iteTag2.next();
            eleTag1Image = eleTag1.select("td > img[id]").first();
            eleTag1Time = eleTag1.nextElementSibling();
            eleTag1Replies = eleTag1Time.nextElementSibling();
            eleTag1Views = eleTag1Replies.nextElementSibling();
            if(Global.bTopicHeader){
                if(eleTag1Image != null){
                    urlImage= eleTag1Image.select("img").attr("src");
                    bitmap = loadBitmapAssert(urlImage);
                }
            }
            if(eleTag1 != null){
                text1 = eleTag1.text();

            }
            if(eleTag1.select("div > a").first() != null)
                text1_url1 = eleTag1.select("div > a").first().attr("href");
            if(eleTag1Time.select("span > a").first() != null){
                text2 = eleTag1Time.select("span > a").first().text();
                text2_url2 = eleTag1Time.select("span > a").first().attr("href");
            }
            if((eleTag1Replies != null) && (eleTag1Views != null))
                text2 += "\nReplie:" + eleTag1Replies.text() + " - View:"+eleTag1Views.text();

            contain = new Thread(text1,text2, text3,text4,	bitmap,	text1_url1, text2_url2);
            ListContains.add(contain);
        }
        ListContains.add(new Thread("Thread",null,null,null,null,null,null));
        while(iteTag1.hasNext()){
            text2_url2 = null;
            text1 = "";
            eleTag1 = iteTag1.next();
            eleText1 = eleTag1.select("a[id*=thread_title").first();
            if( eleText1!= null){
                if(eleTag1.select("a[href*=prefixid]").first()!= null){
                    text1 = eleTag1.select("a[href*=prefixid]").first().text()+"-";
                    mPrefixUrl = eleTag1.select("a[href*=prefixid]").attr("href");
                }
                text1 += eleTag1.select("a[id*=thread_title").first().text();
                text1_url1 = eleTag1.select("a[id*=thread_title").first().attr("href");
            }
            if(eleTag1.select("td[class=alt2]:has(span)").first() != null){
                text2 = eleTag1.select("td[class=alt2]:has(span)").first() .text();
            }

            if(eleTag1.select("td[align=center]").first() != null){
                text2 += "\nReplies:" + eleTag1.select("td[align=center]").get(0).text() + " - Views:"+eleTag1.select("td[align=center]").get(1).text();
            }
            if(eleTag1.select("div:has(span[onclick*=member.php])").first() != null){
                text2 = eleTag1.select("div:has(span[onclick*=member.php])").first().text() + " - "+ text2;
            }

            if(Global.bTopicHeader){
                if(eleTag1.select("img[id*=thread_statusicon]").first() != null){
                    bitmap = loadBitmapAssert(eleTag1.select("img[id*=thread_statusicon]").first().attr("src"));
                }
            }
            if(eleTag1.select("a[id*=thread_gotonew]").first() != null){
                text2_url2 = eleTag1.select("a[id*=thread_gotonew]").first().attr("href");
            }
            contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
            contain.mPrefixLink = mPrefixUrl;
            contain.mIdThread = text1_url1.split("t=")[1];
            if(eleTag1.select("a:has(img[src*=lastpost])").first() != null){
                contain.setUrlLastPost(eleTag1.select("a:has(img[src*=lastpost])").first().attr("href"));
            }
            if(eleText1.hasClass("vozsticky")){
                contain.setSticky(true);
            }else{
                contain.setSticky(false);
            }
            if( checkBookmark(contain.mIdThread)){
                contain.isBookmark = true;
                mDataBookmark.updateBookmark(contain);
            }
            ListContains.add(contain);

        }

        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
        adapter.notifyDataSetChanged();
        mList.clearFocus();
        mList.post(new Runnable() {
            @Override
            public void run() {
                mList.setSelection(0);
            }
        });
    }

}
