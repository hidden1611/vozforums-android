
package com.nna88.voz.main;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.nna88.voz.contain.Thread;
import com.nna88.voz.parserhtml.parser;
import com.nna88.voz.util.UserInfo;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Intent which is handling GCM messages and registrations. <br/>
 * <br/>
 * {@link GCMIntentService#onMessage} will handle message from GCM4Public server - show up a
 * notification and vibrate the phone. {@link GCMIntentService#onRegistered} will send the
 * registrationId and SENDER_ID constant to the server.<br/>
 * <br/>
 * In order this class to work, don't forget to copy the gcm.jar file to libs folder.
 * 
 * @author Vilius Kraujutis
 * @since 2012-12-01
 */
public class GCMIntentService extends GCMBaseIntentService {
    static final String SERVER_URL = "http://sonic-fact-242.appspot.com";
    static final String SENDER_ID = "165553162196";
    private static String deviceKey;
    private static String androidID;
    private static Context mContext;
    private ArrayList<Thread> lThread;
    private ArrayList<Thread> lThread2;
    static UserInfo mUser = new UserInfo();
    private static final int MY_NOTIFICATION_SUBSCRIBE_ID = 1;
    private static final int MY_NOTIFICATION_QUOTE_ID = 2;
    private static final int MY_NOTIFICATION_AD = 3;
    private static final int MY_NOTIFICATION_PM = 4;
    private NotificationManager mNotificationManager1;
    private NotificationCompat.Builder mBuilder1;
    private PendingIntent pendingIntent1;
    private Intent myIntent1;
    private NotificationManager mNotificationManager2;
    private NotificationCompat.Builder mBuilder2;
    private PendingIntent pendingIntent2;
    private Intent myIntent2;
    private NotificationManager mNotificationManager3;
    private NotificationCompat.Builder mBuilder3;
    private PendingIntent pendingIntent3;
    private Intent myIntent3;
    private NotificationManager mNotificationManager4;
    private NotificationCompat.Builder mBuilder4;
    private PendingIntent pendingIntent4;
    private Intent myIntent4;
    private Handler mHandler = new Handler();

    private parser mParser;
    private Task mTask;
    private Document doc;
    @Override
    protected void onError(Context context, String arg1) {
        // TODO Auto-generated method stub

    }
    private void initNotification(Context context) throws Exception{
        mNotificationManager1 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager3 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager4 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder1 = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.voz_icon)
                .setContentTitle("My notification")
                .setContentText("Hello World!");
        mBuilder1.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder1.setAutoCancel(true);

        mBuilder2 = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.voz_icon)
                .setContentTitle("My notification")
                .setContentText("Hello World!");
        mBuilder2.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder2.setAutoCancel(true);

        mBuilder3 = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.voz_icon)
                .setContentTitle("My notification")
                .setContentText("Hello World!");
        mBuilder3.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder3.setAutoCancel(true);

        mBuilder4 = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.voz_icon)
                .setContentTitle("My notification")
                .setContentText("Hello World!");
        mBuilder4.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder4.setAutoCancel(true);
    }
    @Override
    protected void onRegistered(Context arg0, String registrationId) {
        // registrationId is something like this:
        Log.d(TAG, "onRegistered: " + registrationId);
        registerGCMClient(registrationId, SENDER_ID);
    }
    /**
     * This is called when application first time registers for the GCM.<br/>
     * <br/>
     * This method registers on the opensource GCM4Public server
     * 
     * @param registrationId
     * @param senderId
     */
    private void registerGCMClient(String registrationId, String senderId) {
        String url = SERVER_URL+"/registergcmclient?registrationId=" + registrationId+
                                                    "&devicekey=" + deviceKey +
                                                    "&androidid=" + androidID +
                                                    "&email=null";

        url = url.replace(" ","");
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        try {
            httpclient.execute(httpget);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    protected static void unregisterGCMClient(String registrationId) {
        String url = SERVER_URL+"/unregistergcmclient?registrationId=" + registrationId;
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        try {
            httpclient.execute(httpget);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        // TODO Auto-generated method stub
        unregisterGCMClient(arg1);
    }
    protected static void unregisterAtGCM(Context context) {
        mContext = context;
        GCMRegistrar.checkDevice(context);
        GCMRegistrar.checkManifest(context);
        final String regId = GCMRegistrar.getRegistrationId(context);
        GCMRegistrar.unregister(context);
    }

    protected static void registerAtGCM(Context context,String mdeviceKey, String mandroidID) {
        mContext = context;
        deviceKey = mdeviceKey;
        androidID = mandroidID;
        GCMRegistrar.checkDevice(context);
        GCMRegistrar.checkManifest(context);
        final String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            GCMRegistrar.register(context, SENDER_ID);
        }
    }

    @Override
    protected void onMessage(final Context context, Intent messageIntent) {
        mContext = context;
        Map<String,String> mapCookies = new HashMap<String, String>();
        SharedPreferences sf = context.getSharedPreferences("Setting",Context.MODE_PRIVATE);
        mapCookies.put("vfimloggedin",sf.getString("vfimloggedin",""));
        mapCookies.put("vflastactivity",sf.getString("vflastactivity",""));
        mapCookies.put("vflastvisit", sf.getString("vflastvisit", ""));
        mapCookies.put("vfpassword",sf.getString("vfpassword",""));
        mapCookies.put("vfsessionhash",sf.getString("vfsessionhash",""));
        mapCookies.put("vfuserid",sf.getString("vfuserid",""));
        mUser.setUser(sf.getString("username-" + sf.getString("usered",""),""));
        mUser.cookies(mapCookies);
        Bundle extras = messageIntent.getExtras();
        String lmessage = extras.getString("message");
        String message = lmessage.split(",")[0];
        try{
            String modeAds = lmessage.split(",")[1];
            SharedPreferences.Editor editor = sf.edit();
            editor.putInt("MODE", Integer.parseInt(modeAds));
            editor.commit();
        }catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
        try {
            initNotification(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(message.equals("NOTIFICATION")){
            //CommonUtilities.displayMessage(mContext,"check subscribed & quote");
            SharedPreferences settings;
            settings = getSharedPreferences("Setting", MODE_PRIVATE);
            Global.bNotifSubscribe = settings.getBoolean("NOTIFICATIONSUBSCRIBE", true);
            if(!Global.bNotifSubscribe) return;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTask = new Task();
                    mTask.execute(1);
                }
            });
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTask = new Task();
                    mTask.execute(2);
                }
            },5000);
        }else if(message.startsWith("AD")){
            try {
                //CommonUtilities.displayMessage(mContext,"ad");
                String ad_urlIcon, ad_title, ad_msg,ad_Iconname,ad_url;
                String tmp[] = message.split(",");
                ad_urlIcon = tmp[1];
                ad_Iconname = tmp[2];
                ad_title = tmp[3];
                ad_msg = tmp[4];
                ad_url = tmp[5];
                File file = new File("/mnt/sdcard/"+ad_Iconname);
                if(!file.exists())    FileUtils.copyURLToFile(new URL(ad_urlIcon),file);
                mBuilder3.setLargeIcon(BitmapFactory.decodeFile(file.getAbsolutePath()));
                mBuilder3.setContentTitle(ad_title);
                mBuilder3.setContentText(ad_msg);
                myIntent3 =  new Intent(Intent.ACTION_VIEW, Uri.parse(ad_url));
                pendingIntent3 = PendingIntent.getActivity(context, 0, myIntent3, Intent.FLAG_ACTIVITY_NEW_TASK);
                mBuilder3.setContentIntent(pendingIntent3);
                mNotificationManager3.notify(MY_NOTIFICATION_AD, mBuilder3.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class Task extends AsyncTask<Integer,Void,Integer>{

        @Override
        protected Integer doInBackground(Integer... integers) {
            if(integers[0]==1){
                mParser = new parser("https://vozforums.com/usercp.php");
                doc = mParser.getDoc(mUser);
                return 1;
            }else {
                mParser = new parser("");
                doc = mParser.SearchQuote(mUser);
                return 2;
            }

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if(integer==1){
                try {
                    checkNotificaionSubscribe(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                try {
                    checkNotificaionQuote(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void checkNotificaionQuote(final Context context) throws Exception{
        lThread = new ArrayList<Thread>();
        try {
            parserSearchQuote(doc);
            int nummss = 0;
            for(int i=0;i<lThread.size();i++){

                if(lThread.get(i).LastPost() != null){
                    mBuilder2.setContentTitle(lThread.get(i).Thread());
                    mBuilder2.setContentText(lThread.get(i).LastPost()).setNumber(++nummss);
                    myIntent2 = new Intent(context, Page3.class);
                    myIntent2.putExtra("URL", lThread.get(i).UrlThread());
                    myIntent2.putExtra("TITLE","");
                    myIntent2.putExtra("NOTIFICATIONQUOTE",nummss);
                    pendingIntent2 = PendingIntent.getActivity(context, 1, myIntent2, Intent.FLAG_ACTIVITY_NEW_TASK);
                    mBuilder2.setContentIntent(pendingIntent2);

                }
            }
            if(nummss != 0)
                mNotificationManager2.notify(MY_NOTIFICATION_QUOTE_ID, mBuilder2.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void checkNotificaionSubscribe(final Context context) throws Exception{
        lThread = new ArrayList<Thread>();
        try {
            parserSubScribe(doc);
            int nummss = 0;
            for(int i=0;i<lThread.size();i++){
                if(lThread.get(i).UrlLastPosst() != null){
                    mBuilder1.setContentTitle(lThread.get(i).Thread());
                    mBuilder1.setContentText(lThread.get(i).LastPost()).setNumber(++nummss);
                    myIntent1 = new Intent(context, Page3.class);
                    myIntent1.putExtra("URL", lThread.get(i).UrlLastPosst());
                    myIntent1.putExtra("TITLE","");
                    myIntent1.putExtra("NOTIFICATION",nummss);
                    pendingIntent1 = PendingIntent.getActivity(context, 0, myIntent1, Intent.FLAG_ACTIVITY_NEW_TASK);
                    mBuilder1.setContentIntent(pendingIntent1);
                }
            }
            if (nummss != 0)      mNotificationManager1.notify(MY_NOTIFICATION_SUBSCRIBE_ID, mBuilder1.build());
            nummss = 0;
            for(int i=0;i<lThread2.size();i++){
                if(lThread2.get(i).UrlThread() != null){
                    mBuilder4.setContentTitle("New PM - "+lThread2.get(i).Thread());
                    mBuilder4.setContentText(lThread2.get(i).LastPost()).setNumber(++nummss);
                    myIntent4 = new Intent(context, PagePM.class);
                    pendingIntent4 = PendingIntent.getActivity(context, 0, myIntent4, Intent.FLAG_ACTIVITY_NEW_TASK);
                    mBuilder4.setContentIntent(pendingIntent4);
                }
            }
            if (nummss != 0)      mNotificationManager4.notify(MY_NOTIFICATION_PM, mBuilder4.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void parserSearchQuote(Document doc){
        Thread contain;
        lThread.clear();
        int iNumQuote = -1;
        Bitmap bitmap = null;
        String text1 = null,text2=null,text3=null,text4=null,text1_url1=null,text2_url2=null,url_lastpost=null;
        Iterator<Element> ite = doc.select("table[id]").iterator();
        Element eleTag1;
        if(ite.hasNext()){
            eleTag1 = ite.next();
            //Global.iNumQuote = 60;
            if(doc.select("span[title*=Showing results]").first() != null){
                String title = doc.select("span[title*=Showing results]").first().attr("title");
                if(title.contains("of ")){
                    title = title.split("of ")[1];
                    iNumQuote = Integer.parseInt(title);
                }
            }
        }
        if(iNumQuote == -1) return;
        while((iNumQuote > Global.iNumQuote ) && (ite.hasNext())){
            Global.iNumQuote++;
            eleTag1 = ite.next();

            if( eleTag1.select("a[href*=showthread.php]").first()!= null){
                text1 = eleTag1.select("a[href*=showthread.php]").first().text();
                text1_url1 = eleTag1.select("a[href*=showthread.php]").first().attr("href");
            }
            if( eleTag1.select("a[href*=member.php]").first()!= null){
                text2 ="Posted By " + eleTag1.select("a[href*=member.php]").first().text();
            }
            if(eleTag1.select("em").first()!=null){
                text2 += "\n" + eleTag1.select("em").first().ownText();
                text1_url1 = eleTag1.select("em").first().select("a").attr("href");
            }
            contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
            lThread.add(contain);
        }
        Global.iNumQuote = iNumQuote;
        writeNumQuote(Global.iNumQuote);
    }
    private void writeNumQuote(int value){
        SharedPreferences prefs = getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("NUMQUOTE", value);
        editor.commit();
    }

    private void parserSubScribe(Document doc){
        com.nna88.voz.contain.Thread contain;
        lThread.clear();
        lThread2 = new ArrayList<Thread>();
        String text1 = null,text2=null,text1_url1=null,text2_url2=null,url_lastpost=null;
        Element elemain = doc.select("tbody[id=collapseobj_usercp_subthreads]").first();
        Elements eles = elemain.select("tr:has(td[id*=td_threads])");
        Iterator<Element> ite = eles.iterator();
        Element eleTag1;
        text1 = "New Subscribed Threads";
        lThread.add(new Thread(text1,text2,null,null,null,text1_url1,text2_url2));
        while(ite.hasNext()){
            eleTag1 = ite.next();
            if( eleTag1.select("a[id*=thread_title]").first()!= null){
                text1 = eleTag1.select("a[id*=thread_title]").first().text();
                text1_url1 = eleTag1.select("a[id*=thread_title]").first().attr("href");
            }
            Element eleLastPost = eleTag1.select("td[class=alt2]:has(span)").first();
            if(eleLastPost != null){
                text2 = eleLastPost .text();
                if(eleLastPost.select("a:has(img)").first()!= null){
                    url_lastpost = eleLastPost.select("a:has(img)").attr("href");
                }
                url_lastpost = text2;
            }

            if(eleTag1.select("a[id*=thread_gotonew]").first() != null){
                text2_url2 = eleTag1.select("a[id*=thread_gotonew]").first().attr("href");
            }
            contain = new Thread(text1,text2,null,null,null,text1_url1,text2_url2);
            contain.mIdThread = text1_url1.split("t=")[1];
            contain.setUrlLastPost(url_lastpost);
            lThread.add(contain);
        }
        eles = doc.select("tr:has(img[src*=statusicon/pm_new])");
        ite = eles.iterator();
        while(ite.hasNext()){
            eleTag1 = ite.next();
            Log.d("nna",""+eleTag1.text());
            if( eleTag1.select("a[href*=private.php]").first()!= null){
                text1 = eleTag1.select("a[href*=private.php]").first().text();
                text1_url1 = eleTag1.select("a[href*=private.php]").first().attr("href");
            }
            //<span style="float:right" class="time">15:59</span>
            Element eleLastPost = eleTag1.select("span[class=time]").first();
            if(eleLastPost != null){
                text2 = eleLastPost .text();
                url_lastpost = text2;
            }
            contain = new Thread(text1,text2,null,null,null,text1_url1,null);
            lThread2.add(contain);
        }
    }
}
