package com.nna88.voz.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;

import java.util.ArrayList;
import java.util.List;

public class PageBookmark extends Page{
	private ArrayList<Thread> lContains;
	private listViewCustom2 adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_BOOKMARK;
		super.onCreate(savedInstanceState);
		
		lContains = new ArrayList<Thread>();
		adapter = new listViewCustom2(mContext,lContains);
		mObjectAdapter = adapter;
	    adapter.setSize(mTextSize);
	    mList.setAdapter(adapter);
	    List<Comment> lData = mDataBookmark.getAllComments();
	    lContains.add(new Thread("ListBookmark",null,null,null,null,null,null));
        int iSize = lData.size();
        for(int i=iSize-1 ; i >= 0 ;i--){
            Comment data = lData.get(i);
            Thread thread = new Thread("  "+data.title, data.LastPost, data.iPage, data.View,null , data.url, data.urlFirstNews);
            thread.mIdThread = data.idThread;
            thread.setUrlLastPost(data.urlLastPost);
            lContains.add(thread);
        }

        adapter.notifyDataSetChanged();
        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
	    //adapter.notifyDataSetChanged();
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                try{
                    Intent i = null;
                    vibrate();
                    if (lContains.get(pos).isUrl() == 0) { // forum
                        i = new Intent(mContext, Page2.class);
                        if(lContains.get(pos).UrlThread() == null)		return;
                        i.putExtra("URL", lContains.get(pos).UrlThread());

                    } else if (lContains.get(pos).isUrl() == 1){									//thread
                        i = new Intent(mContext, Page3.class);
                        if((lContains.get(pos).UrlLastPosst() != null) &&
                                (!lContains.get(pos).UrlLastPosst().equals(""))	){
                            if(lContains.get(pos).UrlLastPosst() == null)		return;
                            i.putExtra("URL", lContains.get(pos).UrlLastPosst());
                        }else{
                            if(lContains.get(pos).UrlThread() == null)		return;
                            i.putExtra("URL", lContains.get(pos).UrlThread());
                        }
                    }else
                        return;

                    i.putExtra("TITLE", lContains.get(pos).Thread());
                    startActivity(i);
                    overridePendingTransition(R.anim.rail, R.anim.rail);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                alertLongClick(pos);
                vibrate();
                return false;
            }
        });
        setProgress(false);

	}
	private Dialog alertLongClick(final int pos){
		
		final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Please choose");
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.pagelongclick);
//		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,  WindowManager.LayoutParams.WRAP_CONTENT);
		
		TextView txFirstNews = (TextView)dialog.findViewById(R.id.firstnew);
		TextView txFirstPost = (TextView)dialog.findViewById(R.id.firstpost);
		TextView txLastPost = (TextView)dialog.findViewById(R.id.lastpost);
        TextView txCopyLink = (TextView)dialog.findViewById(R.id.copylink);
		final TextView txBookmark= (TextView)dialog.findViewById(R.id.bookmark);
		final Thread contain = lContains.get(pos);
		contain.isBookmark = true;
		txBookmark.setText(getResources().getString(R.string.UnBookmark));
        TextView txPrefix = (TextView)dialog.findViewById(R.id.Prefix);
        if(lContains.get(pos).mPrefixLink != null){
            txPrefix.setVisibility(View.VISIBLE);
        }
        txPrefix.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = null;
                i = new Intent(mContext, Page2.class);
                i.putExtra("URL", lContains.get(pos).mPrefixLink);
                i.putExtra("TITLE","");
                startActivity(i);
                overridePendingTransition(R.anim.rail, R.anim.rail);
                dialog.dismiss();
            }
        });
        txCopyLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int sdk = android.os.Build.VERSION.SDK_INT;
                String url = lContains.get(pos).UrlThread();
                if(!url.contains("https://vozforums.com/"))
                    url = "https://vozforums.com/" + url;
                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setText(url);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",url);
                    clipboard.setPrimaryClip(clip);
                }
                dialog.dismiss();
            }
        });
		txFirstPost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = null;
				i = new Intent(mContext, Page3.class);
				i.putExtra("URL", lContains.get(pos).UrlThread());
				i.putExtra("TITLE","");
				startActivity(i);
				overridePendingTransition(R.anim.rail, R.anim.rail);
				dialog.dismiss();
			}
		});
		txFirstNews.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (lContains.get(pos).UrlLastPosst().equals("")) {
					toast("Không có link");
				} else {
					Intent i = null;
					i = new Intent(mContext, Page3.class);
					i.putExtra("URL", lContains.get(pos).UrlLastPosst());
					i.putExtra("TITLE", lContains.get(pos).UrlLastPosst());
					startActivity(i);
					overridePendingTransition(R.anim.rail, R.anim.rail);
					dialog.dismiss();
				}
				
			}
		});
		
		txLastPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (lContains.get(pos).getUrlLastLast() == null) {
					toast("Không có link");
				} else {
					Intent i = null;
					i = new Intent(mContext, Page3.class);
					i.putExtra("URL", lContains.get(pos).getUrlLastLast());
					i.putExtra("TITLE", "");
					startActivity(i);
					overridePendingTransition(R.anim.rail, R.anim.rail);
					dialog.dismiss();
				}
			}
		});
		txBookmark.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDataBookmark.deleteBookmark(contain);
				contain.isBookmark = false;
				lContains.remove(contain);
				adapter.notifyDataSetChanged();
				dialog.dismiss();
			}
		});
		dialog.show();
		return dialog;
	}
}
