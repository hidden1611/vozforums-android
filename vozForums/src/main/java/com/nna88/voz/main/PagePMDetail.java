package com.nna88.voz.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;

import com.nna88.voz.contain.Post;
import com.nna88.voz.listview.listViewCustom3;
import com.nna88.voz.quickaction.ActionItem;
import com.nna88.voz.quickaction.QuickAction;
import com.nna88.voz.util.Util;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;


public class PagePMDetail extends Page{
	private ArrayList<Post> ListContains;
	private listViewCustom3 adapter;
	private int iPostType;
	private String mTextTitle;
	private  QuickAction mQuickAction;
	private String url;
	
	private String mPostId;
	private int iCallFromNotification=1;
	private int iCallFromNotificationQuote=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_PMDETAIL;
		super.onCreate(savedInstanceState);
		ListContains = new ArrayList<Post>();
		adapter = new listViewCustom3(mContext,ListContains,null,mTextSize);
		mObjectAdapter = adapter;
	    adapter.setSize(mTextSize);
	    mList.setAdapter(adapter);
	    mTextTitle = getIntent().getStringExtra("TITLE");
	    url = getIntent().getStringExtra("URL");
	   if(url.contains(Global.URL) || url.contains(Global.URL2))    
	    	mParser.setUrl(getIntent().getStringExtra("URL"));
	    else	
	    	mParser.setUrl(Global.URL+getIntent().getStringExtra("URL"));
	    mTask.execute(Global.TASK_GETDOC);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int post, long l) {
                if(mUser.isLogin() ){
                    ActionPopup(view,post);
                }
            }
        });
	}
	
	private class DownAvatar extends AsyncTask<Integer, Void, String>{

		@Override
		protected String doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			if(ListContains.get(params[0]).Avatar() != null)
				return null;
			String url = ListContains.get(params[0]).UrlAvatar();
			if(url==null)	return null;
			try {
				
				Response resultImageResponse = Jsoup.connect(url).ignoreContentType(true).execute();
				byte[] data=resultImageResponse.bodyAsBytes();
				ListContains.get(params[0]).setAvatar(BitmapFactory.decodeByteArray(data, 0, data.length));
				return "OK";
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result != null)	adapter.notifyDataSetChanged();
		}
		
	}
	
	private class TaskLoadAvatart{
		int index;
		public TaskLoadAvatart(int i){
			index = i;
		}
		
		public void execute(){
			mImageLoad.imageLoader.loadImage(ListContains.get(index).UrlAvatar(), mImageLoad.options, new ImageLoadingListener() {
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					//toast("load cancle");
				}
				@Override
				public void onLoadingComplete(String arg0, View arg1,
						Bitmap arg2) {
					// TODO Auto-generated method stub
					//toast("index:"+index+arg2);
					ListContains.get(index).setAvatar(arg2);
					adapter.notifyDataSetChanged();
				}

				@Override
				public void onLoadingFailed(String arg0, View arg1,
						FailReason arg2) {
					// TODO Auto-generated method stub
					//toast("load faild");
				}

				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					//toast("load start");
				}
			});
		}
	}
	private void downloadAvatart(){
		int size1 = ListContains.size();
		for(int  index=0;index<size1;index++){
			new TaskLoadAvatart(index).execute();
		}
	}
	
	private void downloadAttach(){
		int size1,size2;
		size1 = ListContains.size();
		for(int i=0;i<size1;i++){
			Post post = ListContains.get(i);
			size2 = post.image.sizeBitmap();
			for(int j=0;j<size2;j++){
				if(post.image.getStr(j).contains("attachment.php")){
					new DownImageAttach().execute(i,j);
				}
			}
		}
	}
	private class DownImageAttach extends AsyncTask<Integer, Void, String>{

		@Override
		protected String doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			if(!mUser.isLogin())	return null;
			if(ListContains.get(params[0]).image.getBitmap(params[1]) != null) 
				return null;
			try {
				String url = ListContains.get(params[0]).image.getStr(params[1]); 
				if(!url.contains("https://vozforums.com/"))
					url = "https://vozforums.com/" + url;
				Response resultImageResponse = Jsoup.connect(url).cookies(mUser.cookies()).ignoreContentType(true).execute();
				byte[] data=resultImageResponse.bodyAsBytes();
				ListContains.get(params[0]).image.SetBitmap(params[1],
								BitmapFactory.decodeByteArray(data, 0, data.length));
				return "OK";
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result != null)
				adapter.notifyDataSetChanged();
		}
		
	}
	private String getPostIdFromUrl(String url){
		String postId = null;
		try {
			log(url);
			if(url.contains("p=")){
				postId = url.split("p=")[1];
				//toast(postId);
			}
			if(postId.contains("#post")){
				postId = postId.split("#post")[1];
			}
			log(postId);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return postId;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
        if(iCallFromNotification != 1){
            startActivity(new Intent(this, PageCP.class));
        }
        if(iCallFromNotificationQuote != 1){
            Intent i = new Intent(this, PageQuote.class);
            //toast("notification:"+iCallFromNotificationQuote);
            i.putExtra("NUMQUOTE", iCallFromNotificationQuote);
            startActivity(i);
        }
        super.onBackPressed();

	}
	void ActionPopup(final View arg1,final int pos){
        ActionItem replyItem 	= new ActionItem(1, "Reply", getResources().getDrawable(R.drawable.tapatalk_bubble_reply));
	    mQuickAction 	= new QuickAction(mContext);
		mQuickAction.addActionItem(replyItem);
		mQuickAction.show(arg1);
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int position, int actionId) {
				Intent i = new Intent(mContext, PageNewThread.class);
				Bundle extras = new Bundle();
				String sToken=null, mId=null, title=null, reciples=null, mUserId=null;
				extras.putInt("POSTTYPE", Global.REQUEST_PMINSERT);
				if(doc!=null){
					if(doc.select("textarea[name=message]").first() != null)
						extras.putString("msg",doc.select("textarea[name=message]").first().text()+"\n");
					sToken = doc.select("input[name=securitytoken]").attr("value");
					mId = url.split("id=")[1];
					title = mTextTitle;
					mUserId = mUser.UserId();
					reciples = ListContains.get(0).User()+" ;";
				}
	 			extras.putString("title", mTextTitle);
	 			extras.putString("TOKEN", sToken);
	 			extras.putString("MID", mId);
	 			extras.putString("USERID", mUserId);
	 			extras.putString("RECIPLIES", reciples);
	 			i.putExtras(extras);
				startActivityForResult(i, Global.REQUEST_PMINSERT);
			}
		});
	}

	@Override
	protected void TaskCancle() {
		// TODO Auto-generated method stub
		super.TaskCancle();
		ListContains.clear();
		adapter.notifyDataSetChanged();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adapter.destroy();
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==Global.REQUEST_PMINSERT){
            setResult(RESULT_OK);
            finish();
        }
	}
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	void Parser(int taskType) throws Exception{
		super.Parser(taskType);
		if(doc==null) return;
		ListContains.clear();
    	Post mContain;
    	Element mainElement;
    	mainElement = doc.select("div[align=center]").get(1);
    	Element eleLogin = mainElement.select("a[href=private.php]").first();
		if(eleLogin != null){
			String temp = mainElement.select("td[class=alt2]").get(0).select("a[href*=mem]").attr("href");
			temp = temp.split("=")[1];
			mUser.setUserId(temp);
	    	Element eleSecurity = mainElement.select("input[name*=securitytoken]").first();
	    	if(eleSecurity!=null){
		    	String secToken;
		    	secToken = eleSecurity.attr("value");
		    	mUser.setToken(secToken);
	    	}
		}
		
		String m_user = null,m_time,m_AvatartUrl = null;
		Elements eleTag1s = mainElement.select("td[id*=td_post]");
		//Elements eleTag1s = mainElement.select("table[id*=post]");
		Iterator<Element> iteTag1 = eleTag1s.iterator();

		while(iteTag1.hasNext()){
			
			m_AvatartUrl = null;
			mContain = new Post();
			Element eleTag1 = iteTag1.next();
			Element eleContain =  eleTag1.parent();
			
			if(eleTag1s.select("div[class=smallfont]:has(strong)").first() != null){
				mTextTitle = eleTag1s.select("div[class=smallfont]:has(strong)").first().text();
                //needfix
//				mMenuTitle.setText(mTextTitle);
			}
			Element eleAvatar = eleContain.previousElementSibling();
			Element eleTime = eleAvatar.previousElementSibling();
			if(eleAvatar.select("img[src*=avatars]").first() != null){
				m_AvatartUrl = eleAvatar.select("img[src*=avatars]").attr("src");
				if(!m_AvatartUrl.contains(Global.URL))
					m_AvatartUrl = Global.URL + m_AvatartUrl;
			}
			
			if(eleAvatar.select("div:containsOwn(Join Date)").first() != null){
				//toast("Join Date:"+eleAvatar.select("div:containsOwn(Join Date)").first().text());
				String jd = eleAvatar.select("div:containsOwn(Join Date)").first().text();
				if(jd.contains("Date:")){
					jd = jd.split("Date:")[1];
				}
				mContain.setJD("Jd:"+jd);
			}else{
				mContain.setJD("");
				//toast("khong co jd");
			}
			
			if(eleAvatar.select("a[class=bigusername]").first() != null){
				m_user = eleAvatar.select("a[class=bigusername]").text();
				mContain.m_UserId = eleAvatar.select("a[class=bigusername]").first().attr("href").split("u=")[1];
			}
			
			
			m_time = eleTime.text();
			Element elePostMessage = eleTag1.select("div[id*=post_message").first();
			if(elePostMessage!= null){
				if(elePostMessage.attr("id").split("_").length > 2)
					mContain.setId(elePostMessage.attr("id").split("_")[2]);
		    	PaserPage3(elePostMessage,mContain,true,false);
				
			}
			//<fieldset class="fieldset">
			Element eleAttachment = eleTag1.select("fieldset[class=fieldset]").first();
			if(eleAttachment!= null){
				mContain.addText("\n");
				PaserPage3(eleAttachment,mContain,true,false);
			}
			mContain.Info(m_user,"", m_time, null,m_AvatartUrl);
			ListContains.add(mContain);
			adapter.notifyDataSetChanged();
			//log(mContain.getText());
		}
		mPostId = getPostIdFromUrl(mParser.getUrl());
		for(final Post post:ListContains){
			if(post.Id()==null) continue;
			if(post.Id().equals(mPostId)){
				try {
					new Handler().post(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							mList.setSelection(ListContains.indexOf(post));
							mList.smoothScrollToPosition(ListContains.indexOf(post));
						}
					});
				} catch (Exception e) {
					// TODO: handle exception
				}
				break;
				
			}
		}
		downloadAvatart();
		downloadAttach();

        mItemCount = 20+3;
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;
           //toast("test:"+ mPostId);
			
		
	}
	

	private void PaserPage3(Element elePostMessage,Post m_Contain,boolean write,boolean pre){
    	Element ele = elePostMessage;
		if(ele != null){
			for (Node child : ele.childNodes()) {
				if(child instanceof Element){
					if(((Element)child).tagName().equals("div")){
						Element eleTemp = ((Element)child).select("div").first();
						//m_Contain.addText("\n");
						PaserPage3(eleTemp,m_Contain,false,pre);
						m_Contain.addText("\n");
					}else if(((Element)child).tagName().equals("blockquote")){
						Element eleTemp = ((Element)child).select("blockquote").first();
						m_Contain.addText("\n");
						PaserPage3(eleTemp,m_Contain,false,pre);
						m_Contain.addText("\n"); 
					}else if(((Element)child).tagName().equals("fieldset")){
						Element eleTemp = ((Element)child).select("fieldset").first();
						m_Contain.addText("\n");
						PaserPage3(eleTemp,m_Contain,false,pre);
						m_Contain.addText("\n");
					}else if(((Element)child).tagName().equals("b")){
						Element eleTemp = ((Element)child).select("b").first();
						int start,end;
						start = m_Contain.getText().length();
						PaserPage3(eleTemp,m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.type.add("", start, end,Typeface.BOLD);
					}else if(((Element)child).tagName().equals("i")){
						Element eleTemp = ((Element)child).select("i").first();
						int start,end;
						start = m_Contain.getText().length();
						PaserPage3(eleTemp,m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.type.add("", start, end,Typeface.ITALIC);
					}else if(((Element)child).tagName().equals("pre")){
						Element eleTemp = ((Element)child).select("pre").first();
						int start,end;
						start = m_Contain.getText().length();
						PaserPage3(eleTemp,m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.quote.add("", start,end);
					}else if(((Element)child).tagName().equals("table")){
						Element eleTemp = ((Element)child).select("table").first();
						int start,end;
						start = m_Contain.getText().length();
						PaserPage3(eleTemp,m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.quote.add("", start,end);
					}else if(((Element)child).tagName().equals("ol")){
						Element eleTemp = ((Element)child).select("ol").first();
						PaserPage3(eleTemp,m_Contain,false,pre);
					}else if(((Element)child).tagName().equals("tbody")){
						Element eleTemp = ((Element)child).select("tbody").first();
						PaserPage3(eleTemp,m_Contain,false,pre);
					}else if(((Element)child).tagName().equals("li")){
						Element eleTemp = ((Element)child).select("li").first();
						PaserPage3(eleTemp,m_Contain,false,pre);
					}else if(((Element)child).tagName().equals("tr")){
						Element eleTemp = ((Element)child).select("tr").first();
						m_Contain.addText("\n");
						PaserPage3(eleTemp,m_Contain,false,pre);
					}else if(((Element)child).tagName().equals("td")){
						Element eleTemp = ((Element)child).select("td").first();
						PaserPage3(eleTemp,m_Contain,false,pre);
					}else if(((Element)child).tagName().equals("img")){
						int start,end;
						String urlImage = (((Element)child).select("img[src]").attr("src"));
						if(urlImage.contains("https://vozforums.com/")){
							if(urlImage.subSequence(0, 21).equals("https://vozforums.com/")){
								if(!urlImage.contains("https://vozforums.com/attachment.php?attachmentid") &&
									!urlImage.contains("https://vozforums.com/customavatars/"))
									urlImage = urlImage.substring(21);
							}
						}
						if(urlImage.substring(0, 1).equals("/"))
							urlImage = urlImage.substring(1, urlImage.length());
						
						start = m_Contain.getText().length();
						end = start + urlImage.length();
						
						if(!(urlImage.contains("http://")||
								urlImage.contains("https://")||
								urlImage.contains("attachment.php?attachmentid")))
							m_Contain.image.add(urlImage, start, end, loadBitmapAssert(urlImage));
						else{
							m_Contain.image.add(urlImage, start, end, null);
						}
						m_Contain.addText(urlImage);
						if(((Element)child).hasAttr("onload")){
							m_Contain.addText("\n");
						}
					}else if(((Element)child).tagName().equals("br")){
						m_Contain.addText("\n");
					}else if(((Element)child).tagName().equals("u")){
						Element eleTemp = ((Element)child).select("u").first();
						int start,end;
						start = m_Contain.getText().length();
						PaserPage3(eleTemp,m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.typeU.add("", start, end);
					}else if(((Element)child).tagName().equals("font")){
						Element eleTemp = ((Element)child).select("font").first();
						int start,end;
						String mfont="while",msize="3";
						if(((Element)child).select("font[color]").first()!=null) 
							mfont = ((Element)child).select("font[color]").attr("color");
						if(((Element)child).select("font[size]").first()!=null) 
							msize = ((Element)child).select("font[size]").attr("size");
						start = m_Contain.getText().length();
						PaserPage3(eleTemp, m_Contain,false,pre);
						end = m_Contain.getText().length();
						m_Contain.font.add("", start, end, mfont, Integer.parseInt(msize));
					}else if (((Element)child).tagName().equals("a")){
						Element eleA = ((Element)child).select("a[href]").first();
						if(eleA.select("img").first() == null){
							String temp = ((Element)child).select("a[href]").attr("href");
							temp=temp.replace("%3A",":");
							temp=temp.replace("%2F","/");
							temp=temp.replace("%3F","?");
							temp=temp.replace("%3D", "=");
							temp=temp.replace("%26", "&");
							if(temp.contains("mailto:")){
								temp = temp.substring(7, temp.length());
								int start,end;
								String tempText = ((Element)child).select("a[href]").text();
								start = m_Contain.getText().length();
								end = start + tempText.length();
								m_Contain.web.add(temp, start, end);
								m_Contain.addText(tempText);
							}else if(temp.contains("http")){
								temp = temp.substring(temp.indexOf("http"), temp.length());
								int start,end;
								String tempText = ((Element)child).select("a[href]").text();
								start = m_Contain.getText().length();
								end = start + tempText.length();
								m_Contain.web.add(temp, start, end);
								m_Contain.addText(tempText);
							}
						}else{
							PaserPage3(eleA,m_Contain,false,true);
						}
						
					}else{
//						m_Contain.addText(((Element)child).text());
//						if(pre) m_Contain.addText("\n");
						m_Contain.addText(((Element)child).text());
						//if(pre) m_Contain.addText("\n");
					}
					//}
				}
			    if (child instanceof TextNode) {
			    	
			    	if(pre) m_Contain.addText(((TextNode) child).getWholeText());
			    	else m_Contain.addText(((TextNode) child).text());
			    }
			}
		}else{
			m_Contain.addText(elePostMessage.text());
		}
    }

}
