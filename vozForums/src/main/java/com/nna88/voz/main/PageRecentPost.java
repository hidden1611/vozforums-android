package com.nna88.voz.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.listview.listViewCustom2;
import com.nna88.voz.mysqlite.Comment;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PageRecentPost extends Page{
	final static String urlNewSubscribe = "https://vozforums.com/usercp.php";
	final static String urlListSubscribe= "https://vozforums.com/subscription.php?do=viewsubscription";
	private ArrayList<Thread> ListContains;
	private listViewCustom2 adapter;
	List<Comment> lComment;
	private int iNumquote = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		iPage=Global.PAGE_RECENTPOST;
		super.onCreate(savedInstanceState);
		ListContains = new ArrayList<Thread>();
		adapter = new listViewCustom2(mContext,ListContains);
	    adapter.setSize(mTextSize);
	    mObjectAdapter = adapter;
	    mList.setAdapter(adapter);
        //_needfix
//	    mMenu.showContent();
        mParser.setUserRecentPost(getIntent().getStringExtra("USERID"));
	    mTask.execute(Global.TASK_RECENTPOST);
        //needfix
        //myVolley.myRencentPost(mUser,getIntent().getStringExtra("USERID"));
//	    mMenuTitle.setText("My Recent Post");
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
               try {
                Intent i = null;
                vibrate();
                if (ListContains.get(pos).isUrl() == 0) { // forum
                    i = new Intent(mContext, Page2.class);
                    if(ListContains.get(pos).UrlThread() == null)		return;
                    i.putExtra("URL", ListContains.get(pos).UrlThread());

                } else if (ListContains.get(pos).isUrl() == 1){									//thread
                    i = new Intent(mContext, Page3.class);
                    if((ListContains.get(pos).UrlLastPosst() != null) &&
                            (!ListContains.get(pos).UrlLastPosst().equals(""))	){
                        if(ListContains.get(pos).UrlLastPosst() == null)		return;
                        i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
                    }else{
                        if(ListContains.get(pos).UrlThread() == null)		return;
                        i.putExtra("URL", ListContains.get(pos).UrlThread());
                    }
                }else
                    return;

                i.putExtra("TITLE", ListContains.get(pos).Thread());
                startActivity(i);
                overridePendingTransition(R.anim.rail, R.anim.rail);
               }catch (Exception e){
                   e.printStackTrace();
               }
            }
        });
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
//                alertLongClick(pos);
                vibrate();
                return false;
            }
        });
//	    adapter.setOnActionItemClickListener(new OnActionItemClickListener() {
//			@Override
//			public void onItemClick(int pos, int actionId) {
//				// TODO Auto-generated method stub
//				if(actionId==1){
//					Intent i = null;
//					if (ListContains.get(pos).isUrl() == 0) { // forum
//						i = new Intent(mContext, Page2.class);
//						if(ListContains.get(pos).UrlThread() == null)		return;
//						i.putExtra("URL", ListContains.get(pos).UrlThread());
//
//					} else if (ListContains.get(pos).isUrl() == 1){									//thread
//						i = new Intent(mContext, Page3New.class);
//						if(ListContains.get(pos).UrlLastPosst() != null){
//							if(ListContains.get(pos).UrlLastPosst() == null)		return;
//							i.putExtra("URL", ListContains.get(pos).UrlLastPosst());
//						}else{
//							if(ListContains.get(pos).UrlThread() == null)		return;
//							i.putExtra("URL", ListContains.get(pos).UrlThread());
//						}
//					}else
//						return;
//
//					i.putExtra("TITLE", ListContains.get(pos).Thread());
//					startActivity(i);
//					overridePendingTransition(R.anim.rail, R.anim.rail);
//				}else {
//					//alertLongClick(pos);
//				}
//			}
//		});
	    lComment = mDataBookmark.getAllComments();
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateSetFont(mTextSize);
	}
	@Override
	protected void updateSetFont(float value) {
		// TODO Auto-generated method stub
		super.updateSetFont(value);
		adapter.setSize(value);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	void Parser(int taskType) throws Exception {
		// TODO Auto-generated method stub
		super.Parser(taskType);
		if(doc==null) return;
		Thread contain;
		ListContains.clear();
		Bitmap bitmap = null;
		String text1 = null,text2=null,text3=null,text4=null,text1_url1=null,text2_url2=null,url_lastpost=null;
		Iterator<Element> ite = doc.select("table[id]").iterator();
		Element eleTag1;
        setPage(parsePage(Global.GO_INDEXPAGE,0));
		if(ite.hasNext())
			ite.next();
		while(ite.hasNext()){
			ite.hasNext();
			eleTag1 = ite.next();
			Element eleForum= eleTag1.select("td[class=thead]").first();
			if( eleForum != null){
				text2 = "Forums: "+eleForum.select("a").first().text();
				text2 +="\n"+ eleForum.ownText();
			}
			if( eleTag1.select("a[href*=showthread.php]").first()!= null){
				text1 =  eleTag1.select("a[href*=showthread.php]").first().text();
				text1_url1 = eleTag1.select("a[href*=showthread.php]").first().attr("href");
			}

			if(eleTag1.select("em").first()!=null){
				text2 += "\n" + eleTag1.select("em").first().ownText();
				text1_url1 = eleTag1.select("em").first().select("a").attr("href");
			}
            if(Global.bTopicHeader){
                if(eleTag1.select("div:has(img[src*=statusicon])").first() != null){
                    bitmap = loadBitmapAssert(eleTag1.select("div:has(img[src*=statusicon])").first().select("img").first().attr("src"));
                }
            }
			contain = new Thread(text1,text2,text3,text4,bitmap,text1_url1,text2_url2);
			ListContains.add(contain);
		}

        mItemCount = adapter.getCount();
        if (mItemOffsetY == null) {
            mItemOffsetY = new int[mItemCount+1];
            mItemtemp = new int[mItemCount+1];
        }
        scrollIsComputed = true;

		adapter.notifyDataSetChanged();
        mList.clearFocus();
        mList.post(new Runnable() {
            @Override
            public void run() {
                mList.setSelection(0);
            }
        });
	}
		
	
}
