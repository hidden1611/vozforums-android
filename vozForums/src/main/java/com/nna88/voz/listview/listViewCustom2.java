package com.nna88.voz.listview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Vibrator;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nna88.voz.contain.Thread;
import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;
import com.nna88.voz.util.SwipeDetector;
import com.nna88.voz.util.UserInfo;
import com.nna88.voz.util.Util;

import java.util.ArrayList;
public class listViewCustom2 extends BaseAdapter{
	private ArrayList<Thread> contains;
    private LayoutInflater inflater; 
    private ViewHolder holder;
    private float mTextSize1;
    private float mTextSize2;
    private Context mContext;
	public listViewCustom2(Context context,ArrayList<Thread> contain) {
        this.contains = contain;
        mContext = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	public void setSize(float mTextSize){
		this.mTextSize1 = mContext.getResources().getDimension(R.dimen.textSize1)*mTextSize;
		this.mTextSize2 = mContext.getResources().getDimension(R.dimen.textSize2)*mTextSize;

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contains.size(); 
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static class ViewHolder  
	{  
        ImageView imgView;  
        TextView txtView1;  
        TextView txtView2;
        RelativeLayout relativelayout;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
        final int pos = position;
        Thread thread = contains.get(pos);
        if(convertView==null)
        {  
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item2, null);
            holder.relativelayout = (RelativeLayout)convertView.findViewById(R.id.relativelayout);
            holder.imgView = (ImageView) convertView.findViewById(R.id.image);
            holder.txtView1 = (TextView) convertView.findViewById(R.id.text1);
            holder.txtView2 = (TextView) convertView.findViewById(R.id.text2);
            convertView.setTag(holder);
        }  
        else  
            holder=(ViewHolder)convertView.getTag();
        Global.setBackgroundItemThread(holder.relativelayout);
        holder.txtView1.setTextSize(mTextSize1);
        holder.txtView2.setTextSize(mTextSize2);
        if((thread.UrlThread()==null)){
            Global.setTextColor2(holder.txtView1);
        	holder.txtView1.setText(thread.Thread());
            holder.txtView2.setText(null);
        	holder.txtView2.setVisibility(View.GONE);
//        	holder.imgView.setVisibility(View.GONE);
        }else{
            Global.setTextColor2(holder.txtView2);
            holder.txtView1.setText(thread.Thread());
        	holder.txtView2.setVisibility(View.VISIBLE);
//        	holder.imgView.setVisibility(View.VISIBLE);
        	if(thread.isSticky()){
        		Global.setTextSticky(holder.txtView1);
        	}else{
        		Global.setTextColor1(holder.txtView1);
        	}

            if(thread.Reply() == null)
                holder.txtView2.setText(thread.LastPost());
            else
                holder.txtView2.setText(thread.LastPost()+ "- Replie:" + contains.get(position).Reply()+ " - View:" + contains.get(position).View());
            if(thread.UrlLastPosst()!= null){
             	holder.txtView1.setTypeface(null, Typeface.BOLD);
            }else{
            	holder.txtView1.setTypeface(null, Typeface.NORMAL );
            }
        }
        
        if(Global.bTopicHeader){
            try {
                holder.imgView.setImageBitmap(thread.Image());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
        	holder.imgView.setVisibility(View.GONE);
        }
        holder.relativelayout.setClickable(false);
        holder.relativelayout.setFocusable(false);
        return convertView;
	}
}
