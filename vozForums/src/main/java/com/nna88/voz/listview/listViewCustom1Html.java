package com.nna88.voz.listview;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nna88.voz.contain.Forum;
import com.nna88.voz.contain.Post;
import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;

import java.util.ArrayList;

public class listViewCustom1Html extends BaseAdapter{
     private ArrayList<Forum> contains;
     private LayoutInflater inflater;
     private ViewHolder holder;

	 public listViewCustom1Html(Context context, ArrayList<Forum> contains) {
		 //super();         
		 this.contains = contains;
	     this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 }

    private void log(String ss){
        Log.d("nna",ss);
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contains.size(); 
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

    static class ViewHolder
    {
        MyWebView webview;
        TapAwareRelativeLayout layout;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
        final int pos = position;
        Forum mForums = contains.get(pos);
        if(convertView==null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item32, null);
            holder.webview = (MyWebView)convertView.findViewById(R.id.webview);
            holder.layout = (TapAwareRelativeLayout)convertView.findViewById(R.id.layout);
            holder.webview.setBackgroundColor(0);
            convertView.setTag(holder);

        }
        else
            holder=(ViewHolder)convertView.getTag();
        Global.setBackgroundItemThread(holder.webview);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mItemClickListener.onItemClick(pos, view);
            }
        });
        if(Build.VERSION.SDK_INT <= 18) {
            holder.webview.getSettings().setJavaScriptEnabled(true);
            holder.webview.addJavascriptInterface(this, "Android");
            holder.webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            holder.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
            holder.webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
            holder.webview.applyAfterMoveFix();

        }else{
            holder.webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.4; Nexus 4 Build/KRT16H) AppleWebKit/537.36\n" +
                    "(KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
            holder.webview.getSettings().setJavaScriptEnabled(true);
            holder.webview.addJavascriptInterface(this, "Android");
            holder.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
            holder.webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            holder.webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
            holder.webview.applyAfterMoveFix();
        }
        holder.webview.loadDataWithBaseURL("https://vozforums.com/", mForums.getForum(), "text/html", "UTF-8", null);


        return convertView;

	}

}
