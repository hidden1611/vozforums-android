/*****************************************************************************
 * SidebarAdapter.java
 *****************************************************************************
 * Copyright © 2012 VLC authors and VideoLAN
 * Copyright © 2012 Edward Wang
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package com.nna88.voz.listview;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;
import com.nna88.voz.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SettingAdapter extends BaseAdapter {
    private ArrayList<Item> mItems;
    private LayoutInflater mInflater;

    private SharedPreferences sf;
    public SettingAdapter(Context activity, ArrayList<Item> items) {
    	mItems = items;
        mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sf = activity.getSharedPreferences("Setting",Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
    	return mItems.size();
    }

    @Override
    public Item getItem(int position) {
    	return mItems.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
    	return arg0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View v = convertView;
        final Item item = getItem(position);
            if (v == null) {
                v = mInflater.inflate(R.layout.setting_item, parent, false);
            }
            CheckBox check = (CheckBox)v.findViewById(R.id.check);
            check.setText(item.mName);
            check.setChecked((item).read());
            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    (item).write(b);
                    if(item.mName.equals("Fullscreen")){
                        Global.bFullScreen = b;
                       mItemClickListener.onItemClick(0,b,item);
                    }else if(item.mName.equals("Sign")){
                        Global.bSign = b;
                        mItemClickListener.onItemClick(1,b,item);
                    }else if(item.mName.equals("Icon header")){
                        Global.bTopicHeader = b;
                        mItemClickListener.onItemClick(2,b,item);
                    }else if (item.mName.equals("Swipe")){
                        Global.bSwipe = b;
                        mItemClickListener.onItemClick(3,b,item);
                    }else if(item.mName.equals("Pull to next")){
//                        Global.bPullNextPage = b;
//                        mItemClickListener.onItemClick(3,b,item);
                    }else if(item.mName.equals("Pull to refresh")){
//                        Global.bPullReload = b;
//                        mItemClickListener.onItemClick(4,b,item);
                    }else if(item.mName.equals("Vibrate")){
                        Global.bVibrate = b;
                        mItemClickListener.onItemClick(5,b,item);
                    }else if(item.mName.equals("Divider")){
                        Global.bDevider = b;
                        mItemClickListener.onItemClick(6,b,item);
                    }else if(item.mName.equals("Effect")){
                        Global.bEffect = b;
                        mItemClickListener.onItemClick(7,b,item);
                    }else if(item.mName.equals("Click to hide")){
                        Global.bClickAd = b;
                    }else if(item.mName.equals("Notification")){
                        Global.bNotifSubscribe = b;
                    }

                }
            });



        return v;
 
    }
    private OnActionItemClickListener mItemClickListener;
	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mItemClickListener = listener;
	}
	public interface OnActionItemClickListener {
		public abstract void onItemClick(int type, boolean value, Item item);
	}


    public static class Item {
        public String mName;
        public static boolean mValue;
        private boolean mValueDefaule;
        private String mSaveName;
        private SharedPreferences sf;

        public  Item(SharedPreferences sf,String mName, String mSaveName,boolean mValue, boolean mValueDefaule) {
            this.mName = mName;
            this.mSaveName = mSaveName;
            this.mValue = mValue;
            this.mValueDefaule = mValueDefaule;
            this.sf = sf;
        }
        public void write(boolean value){
            mValue = value;
            SharedPreferences.Editor editor = sf.edit();
            editor.putBoolean(mSaveName,value);
            editor.commit();

        }
        public boolean read(){
            boolean value = false;
            value = sf.getBoolean(mSaveName,mValueDefaule);
            return value;
        }
    }
}
