package com.nna88.voz.listview;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.text.Html;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.AlignmentSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.ParagraphStyle;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nna88.voz.contain.Post;
import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;
import com.nna88.voz.ui.JellyBeanSpanFixTextView;
import com.nna88.voz.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.util.ArrayList;

public class listViewCustom3 extends BaseAdapter{
    private ArrayList<Post> mcontains;
    private LayoutInflater inflater;
    private ViewHolder holder;
    private Drawable d;
    private Context context;
    private float mTextSize1;
    private float mTextSize3;
    private float mTextSize4;

    private	SpannableString ss;
    private	ImageSpan span;
    private	int i2,start,end;
    private int dp32;
    private float mTextSize;
    private Bitmap bmImageStart;
    private Bitmap bmImageFailed;
    private Drawable drawableOnline,drawableOffline, drawableAvatar;
    public listViewCustom3(Context context,ArrayList<Post> mcontain,Bitmap bmImageStart, float textSize)  {
        //super();
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        drawableOnline = context.getResources().getDrawable(R.drawable.user_online);
        drawableOffline = context.getResources().getDrawable(R.drawable.user_offline);
        drawableAvatar = context.getResources().getDrawable(R.drawable.menu_usercp2);
        dp32 =  Util.convertDpToPx(context, 32);
        int width= (int) (dp32*textSize/4);
        int height= (int) (dp32*textSize/4);
        drawableOnline.setBounds(0,0,width,height);
        drawableOffline.setBounds(0, 0, width, height);

        this.mcontains = mcontain;
        initUniversal();
        //bmImageStart = BitmapFactory.decodeResource(context.getResources(), R.drawable.stub_image);
        bmImageFailed = BitmapFactory.decodeResource(context.getResources(), R.drawable.image_for_empty_url);
        this.bmImageStart = bmImageStart;
    }
    private void initUniversal(){
//        options = new DisplayImageOptions.Builder()
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
//                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
//                .bitmapConfig(Bitmap.Config.ARGB_8888)
//                .build();
        setImageSize();
    }

    private void setImageSize(){
//        if(Global.iSizeImage != Global.REDUCENONE)
//            mImageSize =  new ImageSize(Global.width/Global.iSizeImage,Global.height/Global.iSizeImage);
//        else
//            mImageSize =  new ImageSize(Global.width/Global.REDUCE2,Global.height);
    }
    public void setSize(float mTextSize){
        this.mTextSize = mTextSize;
        this.mTextSize1 = context.getResources().getDimension(R.dimen.textSize2)*mTextSize;
        this.mTextSize3 = context.getResources().getDimension(R.dimen.textSize3)*mTextSize;
        this.mTextSize4 = context.getResources().getDimension(R.dimen.textSize4)*mTextSize;

    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mcontains.size();
    }
    public void destroy(){

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    static class ViewHolder
    {
        LinearLayout layout;
        ImageView avatar;
        TextView user;
        TextView time;
        TextView index;
        TextView jd;
        TextView posts;
        TextView contain;
        TextView userTitle;
        TextView txtViewDevider;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final int pos = position;
        Post mPost = mcontains.get(pos);
        if(convertView==null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item3, parent, false);
            holder.layout = (LinearLayout)convertView.findViewById(R.id.listLayout);
            holder.avatar = (ImageView) convertView.findViewById(R.id.list3_avatar);
            holder.user = (TextView) convertView.findViewById(R.id.list3_user);
            holder.time = (TextView) convertView.findViewById(R.id.list3_time);
            holder.index = (TextView) convertView.findViewById(R.id.list3_index);
            holder.contain = (TextView) convertView.findViewById(R.id.list3_contain);
            holder.jd = (TextView)convertView.findViewById(R.id.list3_jd);
            holder.posts = (TextView)convertView.findViewById(R.id.list3_post);
            holder.userTitle = (TextView)convertView.findViewById(R.id.list3_usertitle);
            holder.txtViewDevider = (TextView)convertView.findViewById(R.id.textDevider);
            convertView.setTag(holder);

        }
        else
            holder=(ViewHolder)convertView.getTag();

        if(Global.bDevider){
            Global.setBackgoundMain(holder.txtViewDevider);
            holder.txtViewDevider.setVisibility(View.VISIBLE);
        }else{
            holder.txtViewDevider.setVisibility(View.GONE);
        }
        Global.setTextContain(holder.contain);
        Global.setTextColor1(holder.time);
        Global.setTextColor1(holder.index);
        Global.setTextColor1(holder.userTitle);
        Global.setTextColor1(holder.jd);
        Global.setTextColor1(holder.user);
        Global.setTextColor1(holder.posts);
        Global.setTextColor1(holder.userTitle);
        holder.user.setTextSize(mTextSize3);
        holder.time.setTextSize(mTextSize3);
        holder.index.setTextSize(mTextSize3);
        holder.jd.setTextSize(mTextSize4);
        holder.posts.setTextSize(mTextSize4);
        holder.userTitle.setTextSize(mTextSize4);
        holder.contain.setTextSize(mTextSize1);

        if(mPost.Avatar() != null)
            holder.avatar.setImageBitmap(mPost.Avatar());
        else
            holder.avatar.setImageDrawable(drawableAvatar);
        holder.user.setText(mPost.User());

//        holder.contain.setTextSize(mTextSize3);
        if(mPost.Time() != null){
            try {
                if (mPost.Time().split(" ").length > 3) {
                    holder.time.setText(mPost.Time().split(" ")[4] + " " +
                            mPost.Time().split(" ")[5]);
                    holder.index.setText(mPost.Time().split(" ")[1]);
                }
            }catch (Exception e){

            }
        }

        holder.userTitle.setText(mPost.UserTitle());
        holder.jd.setText(mPost.JD());
        holder.posts.setText(mPost.Posts());

        if(mPost.isOnline)
            holder.user.setCompoundDrawables(drawableOnline, null, null, null);
        else
            holder.user.setCompoundDrawables(drawableOffline, null, null, null);
//        if(!Global.bScrolling)
//        loadIamge(mPost, 0);
        addImage(holder.contain,mPost);
        if(mPost.isMultiQuote()){
            Global.setBackgroundItemThreadMultiQuote(holder.layout);
        }else{
            Global.setBackgroundItemThread(holder.layout);
        }
        return convertView;
    }


    private void loadIamge(final Post mPost,final int i){
        try {
            if(i >= mPost.image.getSize()) return;
            String url = mPost.image.getStr(i);
            if(!url.contains("http://") && !url.contains("https://")  ){
                loadIamge(mPost,i + 1);
            }else if(url.contains("attachmentid")){
                loadIamge(mPost,i + 1);
            }else{
                if(!mPost.image.getBitmap(i).equals(bmImageStart)){
                    loadIamge(mPost,i + 1);
                }else{
                    Picasso.with(context).load(url).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            try {
                                if (bitmap != null) {
                                    mPost.image.SetBitmap(i, getResizedBitmap(bitmap));
                                }

                                listViewCustom3.this.notifyDataSetChanged();
                                loadIamge(mPost, i + 1);
                            } catch (Exception e) {
                                mPost.image.SetBitmap(i, bmImageFailed);
                                listViewCustom3.this.notifyDataSetChanged();
                                loadIamge(mPost, i + 1);
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            mPost.image.SetBitmap(i, bmImageFailed);
                            listViewCustom3.this.notifyDataSetChanged();
                            loadIamge(mPost, i + 1);

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                }
            }
        } catch (Exception e) {
            loadIamge(mPost,i + 1);
            e.printStackTrace();
        }
    }


    private void log(String ss){
        Log.d("nna",ss);
    }

    public void addImage(final TextView tview,final Post mCon) {
        ss = new SpannableString( mCon.getText());
        for(i2=0;i2< mCon.font.getSize();i2++){
            start = mCon.font.getStart(i2);
            end = mCon.font.getEnd(i2);
            if(mCon.font.color(i2).equals("Purple")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff800080), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Indigo")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff4B0082), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("SlateGray")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff708090), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("DarkOrchid")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff9932CC), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Plum")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffDDA0DD), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);

            }else if(mCon.font.color(i2).equals("Black")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff000000), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("DarkRed")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff8B0000), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Red")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFF0000), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Magenta")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFF00FF), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Pink")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFFC0CB), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);

            }else if(mCon.font.color(i2).equals("Sienna")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffA0522D), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("DarkOrange")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFF8C00), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("SandyBrown")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffF4A460), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Orange")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFFA500), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Wheat")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffF5DEB3), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);

            }else if(mCon.font.color(i2).equals("DarkOliveGreen")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff556B2F), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Olive")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff808000), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("YellowGreen")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff9ACD32), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Yellow")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFFFF00), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("LemonChiffon")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFFFACD), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }

            else if(mCon.font.color(i2).equals("DarkGreen")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff006400), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Green")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff008000), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("SeaGreen")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff2E8B57), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Lime")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff00FF00), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("PaleGreen")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff98FB98), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }

            else if(mCon.font.color(i2).equals("DarkSlateBlue")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff483D8B), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Teal")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff008080), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("MediumTurquoise")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff48D1CC), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Cyan")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff00FFFF), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("PaleTurquoise")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffAFEEEE), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }

            else if(mCon.font.color(i2).equals("Navy")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff000080), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Blue")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff0000FF), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("RoyalBlue")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff4169E1), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("DeepSkyBlue")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff00BFFF), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("LightBlue")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffADD8E6), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }

            else if(mCon.font.color(i2).equals("DarkSlateGray")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff2F4F4F), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("DimGray")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff696969), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Gray")){//greed
                ss.setSpan( new ForegroundColorSpan(0xff808080), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("Silver")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffC0C0C0), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }else if(mCon.font.color(i2).equals("White")){//greed
                ss.setSpan( new ForegroundColorSpan(0xffFFFFFF), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
            float k;
            if(mCon.font.size(i2) > 3)
                k = (float) ((mCon.font.size(i2) - 3)/10.0);
            else
                k = (float) (-(mCon.font.size(i2) - 3)/10.0);
            ss.setSpan( new RelativeSizeSpan(k+1), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        for(i2=0;i2< mCon.web.getSize();i2++){
            start = mCon.web.getStart(i2);
            end = mCon.web.getEnd(i2);
            ss.setSpan(new URLSpan(mCon.web.getStr(i2)), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        for(i2=0;i2< mCon.quote.getSize();i2++){
            start = mCon.quote.getStart(i2);
            end = mCon.quote.getEnd(i2);
            ss.setSpan(new  QuoteSpan(context.getResources().getColor(R.color.ics_blue_dark)), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            ss.setSpan( new StyleSpan(Typeface.ITALIC),start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            ss.setSpan(new ForegroundColorSpan(Color.GRAY), start, end,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        for(i2=0;i2< mCon.type.getSize();i2++){
            start = mCon.type.getStart(i2);
            end = mCon.type.getEnd(i2);
            ss.setSpan( new StyleSpan(mCon.type.type()), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        for(i2=0;i2< mCon.typeU.getSize();i2++){
            start = mCon.typeU.getStart(i2);
            end = mCon.typeU.getEnd(i2);
            ss.setSpan( new UnderlineSpan(), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        for( i2 = 0;i2 < mCon.image.getSize();i2++){

            if(!mCon.image.getStr(i2).contains("http://") && !mCon.image.getStr(i2).contains("https://")){
                d = new BitmapDrawable(context.getResources(),mCon.image.getBitmap(i2));
                if(mCon.image.getStr(i2).contains("attachmentid"))
                    d.setBounds(0, 0, d.getIntrinsicWidth(),  d.getIntrinsicHeight());
                else
                    d.setBounds(0, 0, (int)(dp32*mTextSize), (int)(dp32*mTextSize));
                try{
                    span = new ImageSpan( d, ImageSpan.ALIGN_BOTTOM);
                    start = mCon.image.getStart(i2);
                    end = mCon.image.getEnd(i2);
                    ss.setSpan(span,start,end, SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
                } catch( IndexOutOfBoundsException e ){
                    e.printStackTrace();
                }

            }else{
                try {
                    if(i2 < mCon.image.getSize()){
                        if((mCon.image.getStart(i2) < ss.length()) && (mCon.image.getEnd(i2) < ss.length())){
                            AlignmentSpan.Standard aliSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
                            span = new ImageSpan(context,mCon.image.getBitmap(i2),ImageSpan.ALIGN_BOTTOM);
                            start = mCon.image.getStart(i2);
                            end = mCon.image.getEnd(i2);
                            final String url = ss.subSequence(start, end).toString();
                            ClickableSpan click_span = new ClickableSpan() {
                                @Override
                                public void onClick(View widget) {
                                    if(mImageClickListener != null){
                                        if(url.contains("attachmentid") || url.contains("http://") || (url.contains("https://"))){
                                            mImageClickListener.onImageClick(0,url);
                                        }
                                    }
                                }
                            };

                            ss.setSpan(span,start,end, SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
                            ss.setSpan(aliSpan,start-2 >= 0 ? start-2 : 0,end, SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
                            ss.setSpan(click_span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    // TODO: handle exception
                }

            }
        }
        try{
            tview.setText(ss);
            tview.setMovementMethod(TextViewFixTouchConsume.LocalLinkMovementMethod.getInstance());
            tview.setClickable(false);
            tview.setFocusable(false);
            tview.setLongClickable(false);
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }


    private OnImageClickListestener mImageClickListener;
    public void setOnImageClickListener(OnImageClickListestener imageListener){
        mImageClickListener = imageListener;
    }
    public interface OnImageClickListestener{
        public abstract void onImageClick(int type,String url);
    }

    public Bitmap getResizedBitmap(Bitmap bm) {
        try {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth, scaleHeight;
            if(width >= (Global.width*9.0/10.0)){
                scaleWidth =  ((float)(Global.width*9.0/10.0)/(float)width);
                scaleHeight= scaleWidth;
                Matrix matrix = new Matrix();
                // RESIZE THE BIT MAP
                matrix.postScale(scaleWidth, scaleHeight);
                // "RECREATE" THE NEW BITMAP
                Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
                return resizedBitmap;
            }else
                return bm;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        // CREATE A MATRIX FOR THE MANIPULATION
    }

}
