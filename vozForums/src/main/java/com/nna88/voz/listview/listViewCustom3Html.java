package com.nna88.voz.listview;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.provider.SyncStateContract;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nna88.voz.contain.Post;
import com.nna88.voz.main.FullScreenImage;
import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;
import com.nna88.voz.util.ImageLoad;
import com.nna88.voz.util.UserInfo;
import com.nna88.voz.util.Util;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

public class listViewCustom3Html extends BaseAdapter{
    private ArrayList<Post> mcontains;
    private LayoutInflater inflater;
    private ViewHolder holder;
    private Activity activity;
    protected UserInfo mUser;
    public listViewCustom3Html(Activity activity, ArrayList<Post> mcontain )  {
        //super();
        this.activity = activity;
        this.inflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mcontains = mcontain;
        mUser = new UserInfo();
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mcontains.size();
    }
    public void destroy(){

    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    static class ViewHolder
    {
        MyWebView webview;
        TapAwareRelativeLayout layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final int pos = position;
        Post mPost = mcontains.get(pos);
        if(convertView==null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item32, null);
            holder.webview = (MyWebView)convertView.findViewById(R.id.webview);
            holder.layout = (TapAwareRelativeLayout)convertView.findViewById(R.id.layout);
//            holder.webview.setBackgroundColor(0);
            holder.webview.getSettings().setUseWideViewPort(true);
            holder.webview.getSettings().setJavaScriptEnabled(true);
            holder.webview.setWebChromeClient(new WebChromeClient());
            convertView.setTag(holder);

        }
        else
            holder=(ViewHolder)convertView.getTag();

        if(mPost.isMultiQuote()){
            Global.setBackgroundItemThreadMultiQuote(holder.webview);
        }else{
            Global.setBackgroundItemThread(holder.webview);
        }

        holder.layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClick(pos, view);
            }
        });
//        if(Build.VERSION.SDK_INT <= 18) {
//            holder.webview.getSettings().setJavaScriptEnabled(true);
//            holder.webview.addJavascriptInterface(this, "Android");
//            holder.webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//            holder.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//            holder.webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
//            holder.webview.applyAfterMoveFix();
//
//        }else{
//            holder.webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.4; Nexus 4 Build/KRT16H) AppleWebKit/537.36\n" +
//                    "(KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
//            holder.webview.getSettings().setJavaScriptEnabled(true);
//            holder.webview.addJavascriptInterface(this, "Android");
//            holder.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//            holder.webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//            holder.webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
//            holder.webview.applyAfterMoveFix();
//        }


//        holder.webview.loadDataWithBaseURL("https://vozforums.com/", mcontains.get(pos).getPost(), "text/html", "UTF-8", null);


        return convertView;
    }

    private void log(String ss){
        Log.d("nna",ss);
    }

    private OnActionItemClickListener mItemClickListener;
    public void setOnActionItemClickListener(OnActionItemClickListener listener) {
        mItemClickListener = listener;
    }
    public interface OnActionItemClickListener {
        public abstract void onItemClick(int pos, View view);
    }

    private OnActionImageClickListener mImageClickListenr;
    public void setOnActionImageClickListener(OnActionImageClickListener listener) {
        mImageClickListenr = listener;
    }
    public interface OnActionImageClickListener {
        public abstract void onItemClick(String link);
    }
    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        log("image");
        mImageClickListenr.onItemClick(toast);
    }

}