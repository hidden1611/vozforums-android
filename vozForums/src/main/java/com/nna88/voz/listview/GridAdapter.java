package com.nna88.voz.listview;

import java.util.ArrayList;

import com.nna88.voz.contain.containEmo;
import com.nna88.voz.main.Global;
import com.nna88.voz.util.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GridAdapter extends BaseAdapter{
	private Context mContext;
	private ArrayList<containEmo> listEmo;
	// Constructor
	public GridAdapter(Context c, ArrayList<containEmo> listEmo) {
		mContext = c;
		this.listEmo = listEmo;
	}

	@Override
	public int getCount() {
		return listEmo.size();
	}

	@Override
	public Object getItem(int position) {
		return listEmo.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView = new ImageView(mContext);
		imageView.setImageBitmap(listEmo.get(position).bitmap());
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		int dp_32 = Util.convertDpToPx(mContext.getApplicationContext(),32);
		imageView.setLayoutParams(new GridView.LayoutParams(dp_32, dp_32));
		return imageView;
	}
}
