package com.nna88.voz.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by AMPM on 4/21/14 AD.
 */
public class MyWebView extends WebView {

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public void applyAfterMoveFix () {
        onScrollChanged (getScrollX (), getScrollY (), getScrollX (), getScrollY ());
    }

}
