package com.nna88.voz.listview;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nna88.voz.contain.containEmo;
import com.nna88.voz.main.R;

public class listViewEmo extends BaseAdapter{
     public ArrayList<containEmo> mcontains;
     public LayoutInflater inflater; 
     private ViewHolder holder;  
     Drawable d;

	 public listViewEmo(Activity context,ArrayList<containEmo> mcontain)  {  
        super(); 
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
        mcontains = mcontain;
        
	}  
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mcontains.size(); 
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static class ViewHolder  
	{  
        ImageView emo1;  
        TextView text1;  
	}  
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
        if(convertView==null)  
        {  
            holder = new ViewHolder();  
            convertView = inflater.inflate(R.layout.list_item_emo, null);  
            holder.emo1 = (ImageView) convertView.findViewById(R.id.list3_emoBitmap1);
            holder.text1 = (TextView) convertView.findViewById(R.id.list3_emoText1);
            convertView.setTag(holder);  
        }  
        else  
            holder=(ViewHolder)convertView.getTag();  

        holder.emo1.setImageBitmap(mcontains.get(position).bitmap());
        holder.text1.setText(mcontains.get(position).text());
        holder.text1.setVisibility(View.GONE);
        return convertView;  
	}
	
	
}
