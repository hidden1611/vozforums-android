package com.nna88.voz.listview;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nna88.voz.contain.Forum;
import com.nna88.voz.main.Global;
import com.nna88.voz.main.R;

import java.util.ArrayList;

public class listViewCustom1 extends BaseAdapter{
     private ArrayList<Forum> contains;
     private ViewHolder holder;
     private  float mTextSize1;
     private  float mTextSize2;
     private Context mContext;
     
	 public listViewCustom1(Context context,ArrayList<Forum> contains) {
		 //super();         
		 mContext = context;
		 this.contains = contains;
	 }
	 
	 
	public void setSize(float mTextSize){
		this.mTextSize1 = mContext.getResources().getDimension(R.dimen.textSize1)*mTextSize;
		this.mTextSize2 = mContext.getResources().getDimension(R.dimen.textSize2)*mTextSize;
        log(""+this.mTextSize1);
	}
    private void log(String ss){
        Log.d("nna",ss);
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contains.size(); 
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	static class ViewHolder
	{
        TextView txtView1;  
        TextView txtView2;
        RelativeLayout layout;
        public ViewHolder(View convertView){
            layout = (RelativeLayout) convertView.findViewById(R.id.listitem1);
            txtView1 = (TextView) convertView.findViewById(R.id.text1);
            txtView2 = (TextView) convertView.findViewById(R.id.text2);
            convertView.setTag(R.id.id_holder);
        }
	}  

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int pos = position;
        if(convertView==null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag(R.id.id_holder);
        if (holder == null) {
            holder = new ViewHolder(convertView);
            convertView.setTag(R.id.id_holder, holder);

        }

        holder.txtView1.setText(contains.get(position).Forum());
        holder.txtView1.setTextSize(mTextSize1);
        holder.txtView2.setTextSize(mTextSize2);

        Global.setBackgroundItemThread(holder.layout);

        if(contains.get(position).Viewing() == null){
        	holder.txtView1.setText(contains.get(position).Forum());
            Global.setTextColor2(holder.txtView1);
        	holder.txtView2.setVisibility(View.GONE);

        }else{
        	holder.txtView2.setVisibility(View.VISIBLE);
        	Global.setTextColor1(holder.txtView1);
        	Global.setTextColor2(holder.txtView2);
           	holder.txtView1.setText(contains.get(position).Forum());
            if(contains.get(position).Viewing().contains("Viewing")){
            	holder.txtView2.setText(contains.get(position).Viewing());
            }else
            	holder.txtView2.setText("(0 Viewing)");
        }
        holder.layout.setClickable(false);
        holder.layout.setFocusable(false);
        holder.layout.setFocusableInTouchMode(false);
        return convertView;
        
	}

}
