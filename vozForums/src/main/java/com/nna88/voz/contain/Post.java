package com.nna88.voz.contain;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;

import com.nna88.voz.main.Global;
import com.nna88.voz.util.spanable;

import org.jsoup.nodes.Element;

import java.util.ArrayList;

public class Post {
    public int mCount;
	private String tContain;
	private String mUser;
    private String mUserTitle;
	private String mJd="";
	private String mPosts="0";
	private String mUrlAvatar;
	private String m_id;
	public String m_UserId="";
	private String m_postId;
	private Bitmap mAvatar;
	private String mTitle;
	public spanable quote;
	public spanable border;
	public spanable img;
	public spanable web;
	public spanable size;
	public bitmapSpan image;
	public fontSpan font;
	public typeSpan type;
	public spanable typeU;
	private ArrayList<Bitmap> bitmaps;
	public int style;
    public boolean isOnline = false;
	private boolean isMultiQuote;
    public String mTime="";
    public String mIndex="";
    public String html;
    public String htmlSign;

    public void parserTime(Element eleTime){
        try {
            String time = eleTime.text();
            if (time != null) {
                if (time.split(" ").length > 3) {
                    String timeSplit[] = time.split(" ");
                    mTime = timeSplit[2] + " " + timeSplit[3];
                    mIndex = timeSplit[0];
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
	public Post(){
		tContain = new String();
		img = new spanable();
		web = new spanable();
		bitmaps = new ArrayList<Bitmap>();
		image = new bitmapSpan();
		font = new fontSpan();
		type = new typeSpan();
		typeU = new spanable();
		quote = new spanable();
		isMultiQuote = false;
	}
	public void setTitle(String value){
		mTitle = value;
	}
	public String title(){
		return mTitle;
	}
	public void setMultiQuote(boolean value){
		isMultiQuote = value;
	}
	public boolean isMultiQuote(){
		return isMultiQuote;
	}
	public boolean isEmpty(){
		for(int i=0;i<tContain.length();i++){
			if(tContain.codePointAt(i) != 32) return false;
		}
		if((bitmaps.size()==0)) return true;
		else return false;
	}
	public void set(String text,ArrayList<Bitmap> bitmap){
		tContain = text;
		bitmaps = bitmap;
	}
	public void addText(String text){
		tContain += text;
	}
	public String getText(){
		return tContain;
	}
	public void Info(String mUser,String mUsertitle,String mTime, Bitmap mAvatar,String mUrlAvatar){
		this.mUser = mUser;
        this.mUserTitle = mUsertitle;
		this.mTime = mTime;
		this.mAvatar = mAvatar;
		this.mUrlAvatar = mUrlAvatar;

	}
	public void setId(String m_id){
		this.m_id = m_id;
	}
	public String Id(){
		return m_id;
	}
	public String User(){
		return mUser;
	}
    public String UserTitle(){
        return mUserTitle;
    }
	public String Time(){
		return mTime;
	}
	public String JD(){
		return mJd;
	}
	public void setJD(String ss){
		mJd = ss;
	}
    public String Posts(){
		return mPosts;
	}
	public void setPosts(String ss){
		mPosts = ss;
	}
	public Bitmap Avatar(){
		return mAvatar;
	}
	public void setAvatar(Bitmap bitmap){
		this.mAvatar = bitmap;
	}
	public String UrlAvatar(){
		return mUrlAvatar;
	}
	public class typeSpan extends spanable{
		private int type;
		public void add(String str, int start, int end,int type){
			super.add(str, start, end);
			this.type = type;
		}
		public int type(){
			return type;
		}
	}
	public class fontSpan extends spanable{
		private ArrayList<String> color = new ArrayList<String>();
		private ArrayList<Integer> size = new ArrayList<Integer>();
		@Override
		public void add(String str, int start, int end) {
			// TODO Auto-generated method stub
			super.add(str, start, end);
		}
		public void add(String str, int start, int end,String mcolor,int msize){
			super.add(str, start, end);
			color.add(mcolor);
			size.add(msize);
		}
		public String color(int index){
			return color.get(index);
		}
		public int size(int index){
			return size.get(index);
		}
	}
	
	public class bitmapSpan extends spanable{
		ArrayList<Bitmap> bitmaps;
		public bitmapSpan(){
			bitmaps = new ArrayList<Bitmap>();
		}
		public void add(String str, int start, int end,Bitmap bitmap) {
			// TODO Auto-generated method stub
			super.add(str, start, end);
			this.bitmaps.add(bitmap);
		}
		public void addBitmap(Bitmap bitmap){
			bitmaps.add(bitmap);
		}
		public ArrayList<Bitmap> getBitmap(){
			return bitmaps;
		}
		public Bitmap getBitmap(int index){
			return bitmaps.get(index);
		}
		public int sizeBitmap(){
			return bitmaps.size();
		}
		public void SetBitmap(int i,Bitmap bitmap){
			bitmaps.set(i, bitmap);
		}
		
	}
}
