package com.nna88.voz.contain;

import android.graphics.Bitmap;

public class Thread {

	private String mThread;
	private String mLastPost;
	private String mReply;
	private String mView;
	private Bitmap bitmap;
	private String mUrlThread;
	private String mUrlLasrPost;
	private String mUrlLastLast=null;
	private boolean isSticky;
	private boolean isStar;
	public boolean isBookmark = false;
	public String mIdThread = null;
    public String mPrefixLink=null;
	public Thread( String mThread, 
				     String mLastPost,
				     String mReply,
				     String mView,
				     Bitmap bitmap,
				     String mUrlThread,
				     String mUrlLastPost,
				     Boolean isStar){
		this.mReply = mReply;
		this.mLastPost = mLastPost;
		this.mView = mView;
		this.mThread = mThread;
		this.bitmap = bitmap;
		this.mUrlThread = mUrlThread;
		this.mUrlLasrPost = mUrlLastPost;
		isSticky = false;
		this.isStar = isStar;
	}
	public Thread( String mThread, 
					     String mLastPost,
					     String mReply,
					     String mView,
					     Bitmap bitmap,
					     String mUrlThread,
					     String mUrlLastPost){
		this.mReply = mReply;
		this.mLastPost = mLastPost;
		this.mView = mView;
		this.mThread = mThread;
		this.bitmap = bitmap;
		this.mUrlThread = mUrlThread;
		this.mUrlLasrPost = mUrlLastPost;
		isSticky = false;
		isStar = false;
	}
	public void setUrlLastPost(String url){
		mUrlLastLast = url;
	}
	public String getUrlLastLast(){
		return mUrlLastLast;
	}
	public void setStart(boolean value){
		isStar = value;
	}
	public boolean isStart(){
		return isStar;
	}
	public void setSticky(boolean value){
		isSticky = value;
	}
	public boolean isSticky(){
		return isSticky;
	}
	public String UrlLastPosst(){
		return mUrlLasrPost;
	}
	public int isUrl(){
		if(mUrlThread==null) return 2;
		if (mUrlThread.contains("forumdisplay.php?")){
			return 0;
		}else if(mUrlThread.contains("showthread.php?")){
			return 1;
		}else
			return 2;
	}
	public String UrlThread(){
		return mUrlThread;
	}
	public String Thread(){
		return mThread;
	}
	public String LastPost(){
		return mLastPost;
	}
	public String Reply(){
		return mReply;
	}
	public String View(){
		return mView;
	}
	public Bitmap Image(){
		return bitmap;
	}

}
